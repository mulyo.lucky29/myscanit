# My ScanIT
##
Scan IT adalah aplikasi mobile yang menghandle proses stock opname tahunan terhadap asset yang tersebar di beberapa site yang dilakukan oleh property. karena begitu banyaknya dan tersebar, maka opname dilakukan secara digital: yaitu hanya menscan barcode tag yang di tempel di asset dengan android handheld yang dibawa pada saat proses opname;  device akan secara otomatis menampilkan indo detail dari asset barcode tsb. handheld sebelumnya sudah diisi dengan listing asset-asset yang terdaftar di system, sehingga memungkinkan untuk di jalankan secara offline (tanpa jaringan sekalipun).
 
## Screen Example 
login <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myscanit/myscanit_login.png?raw=true)

download listing dari system <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myscanit/myscanit_download.png?raw=true)

saat scanning <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myscanit/myscanit_scan.png?raw=true)

info detail asset yang muncul <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myscanit/myscanit_scan2.png?raw=true)

hasil listing item yang di opname <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myscanit/myscanit_home.png?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.