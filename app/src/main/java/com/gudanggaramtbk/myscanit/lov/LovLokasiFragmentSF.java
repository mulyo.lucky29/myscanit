package com.gudanggaramtbk.myscanit.lov;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

import java.util.List;

/**
 * Created by luckym on 3/20/2019.
 */

public class LovLokasiFragmentSF  extends DialogFragment {
    Button btn;
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    List<String> lst_data;
    TextView Txt_selected;
    DownloadAssetActivity PActivity;

    public void LovLokasiFragmentSF(){}
    public void SetListOfValue(List<String> lst){
        this.lst_data = lst;
    }
    public void setParentActivity(DownloadAssetActivity oParent){
        this.PActivity = oParent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_lov_lokasi, null);

        //SET TITLE DIALOG TITLE
        getDialog().setTitle("List Value");

        lv  = (ListView) rootView.findViewById(R.id.listView1_loc);
        sv  = (SearchView) rootView.findViewById(R.id.searchView1_loc);
        btn = (Button) rootView.findViewById(R.id.cmd_lov_close_loc);

        PActivity = (DownloadAssetActivity) getActivity();
        adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,lst_data);
        lv.setAdapter(adapter);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Object o = lv.getItemAtPosition(position);
                PActivity.setTxt_spf_location(o.toString());
                dismiss();
            }
        });

        // search filter
        sv.setQueryHint("Search Here..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) { return false; }
            @Override
            public boolean onQueryTextChange(String txt) {
                adapter.getFilter().filter(txt);
                return true;
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        return rootView;
    }
}
