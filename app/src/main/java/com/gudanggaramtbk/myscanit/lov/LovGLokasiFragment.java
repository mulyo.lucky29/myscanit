package com.gudanggaramtbk.myscanit.lov;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.activity.ScanAssetActivity;
import com.gudanggaramtbk.myscanit.adapter.CustomGLocLovAdapter;
import com.gudanggaramtbk.myscanit.model.CLocation;

import java.util.List;


public class LovGLokasiFragment extends DialogFragment {
    Button                            btn;
    ListView                          lv;
    SearchView                        sv;
    private CustomGLocLovAdapter adapter;
    List<CLocation>                   lst_data;
    TextView                          Txt_selected;
    ScanAssetActivity activity;
    Dialog                            d;
    Context                           context;

    public void LovGLokasiFragment(){
        Log.d("[GudangGaram]", "LovGLokasiFragment");
    }
    public void setContext(Context ctx){
        this.context = ctx;
    }
    public void SetListOfValue(List<CLocation> lst){
        this.lst_data = lst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_lov_glokasi, null);

        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);

        d = getDialog();
        d.setTitle("List Value");
        d.getWindow().setLayout(width, height);


        lv  = (ListView) rootView.findViewById(R.id.listView1);
        sv  = (SearchView) rootView.findViewById(R.id.searchView1);
        btn = (Button) rootView.findViewById(R.id.cmd_lov_close);

        activity = (ScanAssetActivity) getActivity();
        //String[] players={"A","B","C","D","E"};
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        //adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,lst_data);
        adapter = new CustomGLocLovAdapter(activity, lst_data);
        //adapter.notifyDataSetChanged();

        lv.setAdapter(adapter);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                CLocation o = adapter.getItemAtPosition(position);
                activity.setTxt_ass_loc_id(o.getLocationID().toString());
                activity.setTxt_ass_loc_code(o.getLocationName().toString());
                activity.setTxt_ass_loc_provinsi(o.getSegment2().toString());
                activity.setTxt_ass_loc_kota(o.getSegment3().toString());
                activity.setTxt_ass_loc_lokasi(o.getSegment4().toString());
                activity.setTxt_ass_loc_lantai(o.getSegment5().toString());

                dismiss();
            }
        });

        // search filter
        sv.setQueryHint("Search Here..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) { return false; }
            @Override
            public boolean onQueryTextChange(String txt) {
                Log.d("[GudangGaram]", "LovGLokasiFragment :: onQueryTextChange : " + txt);
                adapter.getFilter().filter(txt);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        return rootView;
    }
}
