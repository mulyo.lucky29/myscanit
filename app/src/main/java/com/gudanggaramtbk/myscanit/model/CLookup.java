package com.gudanggaramtbk.myscanit.model;

/**
 * Created by LuckyM on 2/8/2019.
 */

public class CLookup {
    private String LContext;
    private String LValue;
    private String LMeaning;

    // ================= getter =======================
    public String getLContext() {
        return LContext;
    }
    public String getLValue() {
        return LValue;
    }
    public String getLMeaning() {
        return LMeaning;
    }

    // ================= setter ========================
    public void setLContext(String LContext) {
        this.LContext = LContext;
    }
    public void setLValue(String LValue) {
        this.LValue = LValue;
    }
    public void setLMeaning(String LMeaning) {
        this.LMeaning = LMeaning;
    }
}
