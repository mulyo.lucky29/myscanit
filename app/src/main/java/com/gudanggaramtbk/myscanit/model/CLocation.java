package com.gudanggaramtbk.myscanit.model;

/**
 * Created by luckym on 2/8/2019.
 */

public class CLocation {

    private Integer LocationID;
    private String LocationName;
    private String Segment1;
    private String Segment2;
    private String Segment3;
    private String Segment4;
    private String Segment5;


    // =========================== getter  ==============================
    public Integer getLocationID() {
        return LocationID;
    }
    public String getLocationName() {
        return LocationName;
    }
    public String getSegment1() {
        return Segment1;
    }
    public String getSegment2() {
        return Segment2;
    }
    public String getSegment3() {
        return Segment3;
    }
    public String getSegment4() {
        return Segment4;
    }
    public String getSegment5() {
        return Segment5;
    }

    // =========================== setter ==============================
    public void setSegment5(String segment5) {
        Segment5 = segment5;
    }
    public void setSegment4(String segment4) {
        Segment4 = segment4;
    }
    public void setSegment3(String segment3) {
        Segment3 = segment3;
    }
    public void setSegment2(String segment2) {
        Segment2 = segment2;
    }
    public void setSegment1(String segment1) {
        Segment1 = segment1;
    }
    public void setLocationName(String locationName) {
        LocationName = locationName;
    }
    public void setLocationID(Integer locationID) {
        LocationID = locationID;
    }

}
