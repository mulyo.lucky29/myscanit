package com.gudanggaramtbk.myscanit.model;

import android.widget.EditText;

/**
 * Created by LuckyM on 2/14/2019.
 */

public class CAttribINV {
    private String nama_barang;
    private String tipe_barang;
    private String ukuran_barang;
    private String merk_barang;
    private String tag_number;
    private String note;

    // ================= getter ==========================
    public String getNama_barang() {
        return nama_barang;
    }
    public String getTipe_barang() {
        return tipe_barang;
    }
    public String getUkuran_barang() {
        return ukuran_barang;
    }
    public String getMerk_barang() {
        return merk_barang;
    }
    public String getTag_number() {
        return tag_number;
    }
    public String getNote() {
        return note;
    }

    // ===================== setter ==========================
    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }
    public void setTipe_barang(String tipe_barang) {
        this.tipe_barang = tipe_barang;
    }
    public void setUkuran_barang(String ukuran_barang) {
        this.ukuran_barang = ukuran_barang;
    }
    public void setMerk_barang(String merk_barang) {
        this.merk_barang = merk_barang;
    }
    public void setTag_number(String tag_number) {
        this.tag_number = tag_number;
    }
    public void setNote(String note) {
        this.note = note;
    }
}
