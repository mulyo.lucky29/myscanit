package com.gudanggaramtbk.myscanit.model;

import android.widget.EditText;

/**
 * Created by LuckyM on 2/14/2019.
 */

public class CAttribBGN {
    private String no_bangunan;
    private String unit;
    private String kecamatan;
    private String kota;
    private String pbb;
    private String tag_number;
    private String note;

    // ===================== getter ================================
    public String getNo_bangunan() {
        return no_bangunan;
    }
    public String getUnit() {
        return unit;
    }
    public String getKecamatan() {
        return kecamatan;
    }
    public String getKota() {
        return kota;
    }
    public String getPbb() {
        return pbb;
    }
    public String getTag_number() {
        return tag_number;
    }
    public String getNote() {
        return note;
    }


    // ====================== setter =================================
    public void setNo_bangunan(String no_bangunan) {
        this.no_bangunan = no_bangunan;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    public void setKota(String kota) {
        this.kota = kota;
    }
    public void setPbb(String pbb) {
        this.pbb = pbb;
    }
    public void setTag_number(String tag_number) {
        this.tag_number = tag_number;
    }
    public void setNote(String note) {
        this.note = note;
    }
}
