package com.gudanggaramtbk.myscanit.model;

/**
 * Created by luckym on 3/21/2019.
 */

public class CFilter {
    private String  QueryCondition;
    private String  FAssetBook;
    private String  FKota;
    private String  FLocation;
    private Boolean FData;

    // ------------- getter -------------------
    public String getQueryCondition() {
        return QueryCondition;
    }
    public String getFAssetBook() {
        return FAssetBook;
    }
    public String getFKota() {
        return FKota;
    }
    public String getFLocation() {
        return FLocation;
    }
    public Boolean getFData() {
        return FData;
    }
    // -------------- setter ------------------
    public void setQueryCondition(String queryCondition) {
        QueryCondition = queryCondition;
    }
    public void setAllFilter(String FAssetBook, String FKota, String FLocation, Boolean FData, String pKondisi){
        this.FAssetBook     = FAssetBook;
        this.FKota          = FKota;
        this.FLocation      = FLocation;
        this.FData          = FData;
        this.QueryCondition = pKondisi;
    }

}
