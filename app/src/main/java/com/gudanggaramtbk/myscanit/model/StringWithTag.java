package com.gudanggaramtbk.myscanit.model;

/**
 * Created by luckym on 1/31/2019.
 */

public class StringWithTag {
    public String string;
    public Object tag;

    public StringWithTag(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}
