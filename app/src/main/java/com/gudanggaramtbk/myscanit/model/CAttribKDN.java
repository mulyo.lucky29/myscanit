package com.gudanggaramtbk.myscanit.model;

import android.widget.EditText;

/**
 * Created by LuckyM on 2/14/2019.
 */

public class CAttribKDN {
    private String nopol_lama;
    private String nopol_baru;
    private String nama_stnk;
    private String warna;
    private String jenis;
    private String tahun;
    private String no_rangka;
    private String no_mesin;
    private String no_faktur;
    private String tgl_faktur;
    private String nama_pemakai;
    private String tag_number;
    private String note;


    // ===================== getter =================================
    public String getNopol_lama() {
        return nopol_lama;
    }
    public String getNopol_baru() {
        return nopol_baru;
    }
    public String getNama_stnk() {
        return nama_stnk;
    }
    public String getWarna() {
        return warna;
    }
    public String getJenis() {
        return jenis;
    }
    public String getTahun() {
        return tahun;
    }
    public String getNo_rangka() {
        return no_rangka;
    }
    public String getNo_mesin() {
        return no_mesin;
    }
    public String getNo_faktur() {
        return no_faktur;
    }
    public String getTgl_faktur() {
        return tgl_faktur;
    }
    public String getNama_pemakai() {
        return nama_pemakai;
    }
    public String getTag_number() {
        return tag_number;
    }
    public String getNote() {
        return note;
    }

    // =========================== setter =================================
    public void setNopol_baru(String nopol_baru) {
        this.nopol_baru = nopol_baru;
    }
    public void setNopol_lama(String nopol_lama) {
        this.nopol_lama = nopol_lama;
    }
    public void setNama_stnk(String nama_stnk) {
        this.nama_stnk = nama_stnk;
    }
    public void setWarna(String warna) {
        this.warna = warna;
    }
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
    public void setNo_rangka(String no_rangka) {
        this.no_rangka = no_rangka;
    }
    public void setNo_mesin(String no_mesin) {
        this.no_mesin = no_mesin;
    }
    public void setNo_faktur(String no_faktur) {
        this.no_faktur = no_faktur;
    }
    public void setTgl_faktur(String tgl_faktur) {
        this.tgl_faktur = tgl_faktur;
    }
    public void setNama_pemakai(String nama_pemakai) {
        this.nama_pemakai = nama_pemakai;
    }
    public void setTag_number(String tag_number) {
        this.tag_number = tag_number;
    }
    public void setNote(String note) {
        this.note = note;
    }

}
