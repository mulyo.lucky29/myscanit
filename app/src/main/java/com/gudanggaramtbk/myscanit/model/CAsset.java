package com.gudanggaramtbk.myscanit.model;

/**
 * Created by luckym on 2/7/2019.
 */

public class CAsset {
    private String  BatchID;
    private String  Asset_ID;
    private String  Asset_Book;
    private String  Asset_BrCode;
    private String  Asset_Desc;
    private Integer Asset_Cat_ID;
    private String  Asset_Cat;
    private Integer Asset_Loc_ID_af;
    private Integer Asset_Loc_ID_bf;
    private Integer Asset_Assign_ID;
    private String  Asset_Assign_Name;
    private String  Asset_Assign_NIK_af;
    private String  Asset_Assign_NIK_bf;
    private Integer Asset_Qty_af;
    private Integer Asset_Qty_bf;
    private String  A1_af;
    private String  A1_bf;
    private String  A2_af;
    private String  A2_bf;
    private String  A3_af;
    private String  A3_bf;
    private String  A4_af;
    private String  A4_bf;
    private String  A5_af;
    private String  A5_bf;
    private String  A6_af;
    private String  A6_bf;
    private String  A7_af;
    private String  A7_bf;
    private String  A8_af;
    private String  A8_bf;
    private String  A9_af;
    private String  A9_bf;
    private String  A10_af;
    private String  A10_bf;
    private String  A11_af;
    private String  A11_bf;
    private String  A12_af;
    private String  A12_bf;
    private String  A13_af;
    private String  A13_bf;
    private String  A14_af;
    private String  A14_bf;
    private String  A15_af;
    private String  A15_bf;
    private String  A16_af;
    private String  A16_bf;
    private String  A17_af;
    private String  A17_bf;
    private String  A18_af;
    private String  A18_bf;
    private String  A19_af;
    private String  A19_bf;
    private String  A20_af;
    private String  A20_bf;

    private String Loc_Seg2;
    private String Loc_Seg3;
    private String Loc_Seg4;
    private String Loc_Seg5;

    private String DOWNLOAD_DATE;
    private String SCAN_DATE;
    private String SCAN_BY;
    private Boolean Checked;

    // ================================ getter ===========================================
    public String getBatchID() {
        return BatchID;
    }
    public String  getAsset_ID() {
        return Asset_ID;
    }
    public String  getAsset_Book() {
        return Asset_Book;
    }
    public String  getAsset_BrCode() {
        return Asset_BrCode;
    }
    public String  getAsset_Desc() {
        return Asset_Desc;
    }
    public Integer getAsset_Cat_ID() {
        return Asset_Cat_ID;
    }
    public String  getAsset_Cat() {
        return Asset_Cat;
    }
    public Integer getAsset_Loc_ID_af() {
        return Asset_Loc_ID_af;
    }
    public Integer getAsset_Loc_ID_bf() {
        return Asset_Loc_ID_bf;
    }
    public Integer getAsset_Assign_ID() {
        return Asset_Assign_ID;
    }
    public String  getAsset_Assign_Name() {
        return Asset_Assign_Name;
    }
    public String  getAsset_Assign_NIK_af() {
        return Asset_Assign_NIK_af;
    }
    public String  getAsset_Assign_NIK_bf() {
        return Asset_Assign_NIK_bf;
    }
    public Integer getAsset_Qty_af() {
        return Asset_Qty_af;
    }
    public Integer getAsset_Qty_bf() {
        return Asset_Qty_bf;
    }
    public String getA1_af() {
        return A1_af;
    }
    public String getA1_bf() {
        return A1_bf;
    }
    public String getA2_af() {
        return A2_af;
    }
    public String getA2_bf() {
        return A2_bf;
    }
    public String getA3_af() {
        return A3_af;
    }
    public String getA3_bf() {
        return A3_bf;
    }
    public String getA4_af() {
        return A4_af;
    }
    public String getA4_bf() {
        return A4_bf;
    }
    public String getA5_af() {
        return A5_af;
    }
    public String getA5_bf() {
        return A5_bf;
    }
    public String getA6_af() {
        return A6_af;
    }
    public String getA6_bf() {
        return A6_bf;
    }
    public String getA7_bf() {
        return A7_bf;
    }
    public String getA7_af() {
        return A7_af;
    }
    public String getA8_bf() {
        return A8_bf;
    }
    public String getA8_af() {
        return A8_af;
    }
    public String getA9_bf() {
        return A9_bf;
    }
    public String getA9_af() {
        return A9_af;
    }
    public String getA10_bf() {
        return A10_bf;
    }
    public String getA10_af() {
        return A10_af;
    }
    public String getA11_bf() {
        return A11_bf;
    }
    public String getA11_af() {
        return A11_af;
    }
    public String getA12_bf() {
        return A12_bf;
    }
    public String getA12_af() {
        return A12_af;
    }
    public String getA13_bf() {
        return A13_bf;
    }
    public String getA13_af() {
        return A13_af;
    }
    public String getA14_bf() {
        return A14_bf;
    }
    public String getA14_af() {
        return A14_af;
    }
    public String getA15_bf() {
        return A15_bf;
    }
    public String getA15_af() {
        return A15_af;
    }
    public String getA16_bf() {
        return A16_bf;
    }
    public String getA16_af() {
        return A16_af;
    }
    public String getA17_bf() {
        return A17_bf;
    }
    public String getA17_af() {
        return A17_af;
    }
    public String getA18_bf() {
        return A18_bf;
    }
    public String getA18_af() {
        return A18_af;
    }
    public String getA19_bf() {
        return A19_bf;
    }
    public String getA19_af() {
        return A19_af;
    }
    public String getA20_bf() {
        return A20_bf;
    }
    public String getA20_af() {
        return A20_af;
    }
    public String getLoc_Seg2() {
        return Loc_Seg2;
    }
    public String getLoc_Seg3() {
        return Loc_Seg3;
    }
    public String getLoc_Seg4() {
        return Loc_Seg4;
    }
    public String getLoc_Seg5() {
        return Loc_Seg5;
    }
    public String getDOWNLOAD_DATE() {
        return DOWNLOAD_DATE;
    }
    public String getSCAN_DATE() {
        return SCAN_DATE;
    }
    public String getSCAN_BY() {
        return SCAN_BY;
    }
    public Boolean getChecked() {
        return Checked;
    }

    // ================================= setter ==========================================
    public void setBatchID(String batchID) {
        BatchID = batchID;
    }
    public void setAsset_ID(String asset_ID) {
        Asset_ID = asset_ID;
    }
    public void setAsset_Book(String asset_Book) {
        Asset_Book = asset_Book;
    }
    public void setAsset_BrCode(String asset_BrCode) {
        Asset_BrCode = asset_BrCode;
    }
    public void setAsset_Desc(String asset_Desc) {
        Asset_Desc = asset_Desc;
    }
    public void setAsset_Cat_ID(Integer asset_Cat_ID) {
        Asset_Cat_ID = asset_Cat_ID;
    }
    public void setAsset_Cat(String asset_Cat) {
        Asset_Cat = asset_Cat;
    }
    public void setAsset_Loc_ID_af(Integer asset_Loc_ID_af) {
        Asset_Loc_ID_af = asset_Loc_ID_af;
    }
    public void setAsset_Loc_ID_bf(Integer asset_Loc_ID_bf) {
        Asset_Loc_ID_bf = asset_Loc_ID_bf;
    }
    public void setAsset_Assign_ID(Integer asset_Assign_ID) {
        Asset_Assign_ID = asset_Assign_ID;
    }
    public void setAsset_Assign_Name(String asset_Assign_Name) { Asset_Assign_Name = asset_Assign_Name; }
    public void setAsset_Assign_NIK_af(String asset_Assign_NIK_af) { Asset_Assign_NIK_af = asset_Assign_NIK_af; }
    public void setAsset_Assign_NIK_bf(String asset_Assign_NIK_bf) { Asset_Assign_NIK_bf = asset_Assign_NIK_bf; }
    public void setAsset_Qty_af(Integer asset_Qty_af) {
        Asset_Qty_af = asset_Qty_af;
    }
    public void setAsset_Qty_bf(Integer asset_Qty_bf) {
        Asset_Qty_bf = asset_Qty_bf;
    }
    public void setA1_af(String a1_af) {
        A1_af = a1_af;
    }
    public void setA1_bf(String a1_bf) {
        A1_bf = a1_bf;
    }
    public void setA2_af(String a2_af) {
        A2_af = a2_af;
    }
    public void setA2_bf(String a2_bf) {
        A2_bf = a2_bf;
    }
    public void setA3_af(String a3_af) {
        A3_af = a3_af;
    }
    public void setA3_bf(String a3_bf) {
        A3_bf = a3_bf;
    }
    public void setA4_af(String a4_af) {
        A4_af = a4_af;
    }
    public void setA4_bf(String a4_bf) {
        A4_bf = a4_bf;
    }
    public void setA5_af(String a5_af) {
        A5_af = a5_af;
    }
    public void setA5_bf(String a5_bf) {
        A5_bf = a5_bf;
    }
    public void setA6_af(String a6_af) {
        A6_af = a6_af;
    }
    public void setA6_bf(String a6_bf) {
        A6_bf = a6_bf;
    }
    public void setA7_af(String a7_af) {
        A7_af = a7_af;
    }
    public void setA7_bf(String a7_bf) {
        A7_bf = a7_bf;
    }
    public void setA8_af(String a8_af) {
        A8_af = a8_af;
    }
    public void setA8_bf(String a8_bf) {
        A8_bf = a8_bf;
    }
    public void setA9_af(String a9_af) {
        A9_af = a9_af;
    }
    public void setA9_bf(String a9_bf) {
        A9_bf = a9_bf;
    }
    public void setA10_af(String a10_af) {
        A10_af = a10_af;
    }
    public void setA10_bf(String a10_bf) {
        A10_bf = a10_bf;
    }
    public void setA11_af(String a11_af) {
        A11_af = a11_af;
    }
    public void setA11_bf(String a11_bf) {
        A11_bf = a11_bf;
    }
    public void setA12_af(String a12_af) {
        A12_af = a12_af;
    }
    public void setA12_bf(String a12_bf) {
        A12_bf = a12_bf;
    }
    public void setA13_af(String a13_af) {
        A13_af = a13_af;
    }
    public void setA13_bf(String a13_bf) {
        A13_bf = a13_bf;
    }
    public void setA14_af(String a14_af) {
        A14_af = a14_af;
    }
    public void setA14_bf(String a14_bf) {
        A14_bf = a14_bf;
    }

    public void setA15_af(String a15_af) {
        A15_af = a15_af;
    }
    public void setA15_bf(String a15_bf) {
        A15_bf = a15_bf;
    }
    public void setA16_af(String a16_af) {
        A16_af = a16_af;
    }
    public void setA16_bf(String a16_bf) {
        A16_bf = a16_bf;
    }
    public void setA17_af(String a17_af) {
        A17_af = a17_af;
    }
    public void setA17_bf(String a17_bf) {
        A17_bf = a17_bf;
    }
    public void setA18_af(String a18_af) {
        A18_af = a18_af;
    }
    public void setA18_bf(String a18_bf) {
        A18_bf = a18_bf;
    }
    public void setA19_af(String a19_af) {
        A19_af = a19_af;
    }
    public void setA19_bf(String a19_bf) {
        A19_bf = a19_bf;
    }
    public void setA20_af(String a20_af) {
        A20_af = a20_af;
    }
    public void setA20_bf(String a20_bf) {
        A20_bf = a20_bf;
    }
    public void setLoc_Seg2(String loc_Seg2) {
        Loc_Seg2 = loc_Seg2;
    }
    public void setLoc_Seg3(String loc_Seg3) {
        Loc_Seg3 = loc_Seg3;
    }
    public void setLoc_Seg4(String loc_Seg4) {
        Loc_Seg4 = loc_Seg4;
    }
    public void setLoc_Seg5(String loc_Seg5) {
        Loc_Seg5 = loc_Seg5;
    }

    public void setDOWNLOAD_DATE(String DOWNLOAD_DATE) {
        this.DOWNLOAD_DATE = DOWNLOAD_DATE;
    }
    public void setSCAN_DATE(String SCAN_DATE) {
        this.SCAN_DATE = SCAN_DATE;
    }
    public void setSCAN_BY(String SCAN_BY) {
        this.SCAN_BY = SCAN_BY;
    }
    public void setChecked(Boolean checked) {
        Checked = checked;
    }

}
