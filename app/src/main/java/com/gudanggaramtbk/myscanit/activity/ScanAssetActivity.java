package com.gudanggaramtbk.myscanit.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.lov.LovGLokasiFragment;
import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.model.CAsset;
import com.gudanggaramtbk.myscanit.model.CLocation;

import java.util.List;

public class ScanAssetActivity extends AppCompatActivity {
    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   StrAssetID;
    private String                   StrAssetCat;
    private String                   StrSessionGroup;
    private MySQLiteHelper dbHelper;
    private SharedPreferences        config;
    TabHost                          hostList;
    private List<CLocation>          lst_Location;
    Context                          context;
    ImageButton                      Cmd_sa_close;
    ImageButton                      Cmd_sa_save;
    ImageButton                      Cmd_ass_loc_lov;

    TextView                         Txt_saa_asset_id;
    TextView                         Txt_saa_asset_tag;
    TextView                         Txt_saa_asset_description;
    TextView                         Txt_saa_asset_category;
    EditText                         Txt_saa_qty;

    // -------- intialize widget in tab ASS --------------
    TextView                         Txt_ass_loc_id;
    EditText                         Txt_ass_loc_code;
    TextView                         Txt_ass_loc_provinsi;
    TextView                         Txt_ass_loc_kota;
    TextView                         Txt_ass_loc_lokasi;
    TextView                         Txt_ass_loc_lantai;
    EditText                         Txt_ass_nik;
    TextView                         Txt_ass_name;
    // -------- intialize widget in tab INV --------------
    EditText                         Txt_inv_nama_barang;
    EditText                         Txt_inv_tipe_barang;
    EditText                         Txt_inv_ukuran_barang;
    EditText                         Txt_inv_merk_barang;
    EditText                         Txt_inv_tag_number;
    EditText                         Txt_inv_note;
    // -------- intialize widget in tab BGN --------------
    EditText                         Txt_bgn_no_bangunan;
    EditText                         Txt_bgn_unit;
    EditText                         Txt_bgn_kecamatan;
    EditText                         Txt_bgn_kota;
    EditText                         Txt_bgn_pbb;
    EditText                         Txt_bgn_tag_number;
    EditText                         Txt_bgn_note;
    // -------- intialize widget in tab KDN --------------
    EditText                         Txt_kdn_nopol_lama;
    EditText                         Txt_kdn_nopol_baru;
    EditText                         Txt_kdn_nama_stnk;
    EditText                         Txt_kdn_warna;
    EditText                         Txt_kdn_jenis;
    EditText                         Txt_kdn_tahun;
    EditText                         Txt_kdn_no_rangka;
    EditText                         Txt_kdn_no_mesin;
    EditText                         Txt_kdn_no_faktur;
    EditText                         Txt_kdn_tgl_faktur;
    EditText                         Txt_kdn_nama_pemakai;
    EditText                         Txt_kdn_tag_number;
    EditText                         Txt_kdn_note;
    // -------- intialize widget in tab OTH --------------
    EditText                         Txt_oth_attribute1;
    EditText                         Txt_oth_attribute2;
    EditText                         Txt_oth_attribute3;
    EditText                         Txt_oth_attribute4;
    EditText                         Txt_oth_attribute5;
    EditText                         Txt_oth_attribute6;
    EditText                         Txt_oth_attribute7;
    EditText                         Txt_oth_attribute8;
    EditText                         Txt_oth_attribute9;
    EditText                         Txt_oth_attribute10;
    EditText                         Txt_oth_attribute11;
    EditText                         Txt_oth_attribute12;
    EditText                         Txt_oth_attribute13;
    EditText                         Txt_oth_attribute14;

    // ============== setter for location lov =================
    public void setTxt_ass_loc_id(String p_txt_ass_loc_id) {
        Txt_ass_loc_id.setText(p_txt_ass_loc_id);
    }
    public void setTxt_ass_loc_code(String p_txt_ass_loc_code) {
        Txt_ass_loc_code.setText(p_txt_ass_loc_code);
    }
    public void setTxt_ass_loc_provinsi(String p_txt_ass_loc_provinsi) {
        Txt_ass_loc_provinsi.setText(p_txt_ass_loc_provinsi);
    }
    public void setTxt_ass_loc_kota(String p_txt_ass_loc_kota) {
        Txt_ass_loc_kota.setText(p_txt_ass_loc_kota);
    }
    public void setTxt_ass_loc_lokasi(String p_txt_ass_loc_lokasi) {
        Txt_ass_loc_lokasi.setText(p_txt_ass_loc_lokasi);
    }
    public void setTxt_ass_loc_lantai(String p_txt_ass_loc_lantai) {
        Txt_ass_loc_lantai.setText(p_txt_ass_loc_lantai);
    }

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",   StrSessionNIK);
                    upIntent.putExtra("PIC_NAME",  StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_asset);

        context = this;
        try{
            hostList = (TabHost)findViewById(R.id.TabAsset);
            hostList.setup();

            //Tab ASS
            TabHost.TabSpec spec = hostList.newTabSpec("Asset List");
            spec.setContent(R.id.tab_ASS);
            spec.setIndicator("Assign");
            hostList.addTab(spec);
            //Tab INV
            spec = hostList.newTabSpec("Asset List");
            spec.setContent(R.id.tab_INV);
            spec.setIndicator("Barang");
            hostList.addTab(spec);
            //Tab BGN
            spec = hostList.newTabSpec("Asset List");
            spec.setContent(R.id.tab_BGN);
            spec.setIndicator("Bangunan");
            hostList.addTab(spec);
            //Tab KDN
            spec = hostList.newTabSpec("Asset List");
            spec.setContent(R.id.tab_KDN);
            spec.setIndicator("Kendaraan");
            hostList.addTab(spec);
            //Tab OTH
            spec = hostList.newTabSpec("Asset List");
            spec.setContent(R.id.tab_OTH);
            spec.setIndicator("Lainnya");
            hostList.addTab(spec);
            // focus on tab 1
            hostList.setCurrentTab(0);
        }
        catch(Exception e){
        }

        Txt_saa_asset_id          = (TextView) findViewById(R.id.txt_saa_asset_id);
        Txt_saa_asset_tag         = (TextView) findViewById(R.id.txt_saa_asset_tag);
        Txt_saa_asset_description = (TextView) findViewById(R.id.txt_saa_asset_description);
        Txt_saa_asset_category    = (TextView) findViewById(R.id.txt_saa_asset_category);
        Txt_saa_qty               = (EditText) findViewById(R.id.txt_saa_qty);

        Txt_ass_loc_id            = (TextView) findViewById(R.id.txt_ass_loc_id);
        Txt_ass_loc_code          = (EditText) findViewById(R.id.txt_ass_loc_code);

        Cmd_ass_loc_lov           = (ImageButton) findViewById(R.id.cmd_ass_loc_lov);

        Txt_ass_loc_provinsi      = (TextView) findViewById(R.id.txt_ass_loc_provinsi);
        Txt_ass_loc_kota          = (TextView) findViewById(R.id.txt_ass_loc_kota);
        Txt_ass_loc_lokasi        = (TextView) findViewById(R.id.txt_ass_loc_lokasi);
        Txt_ass_loc_lantai        = (TextView) findViewById(R.id.txt_ass_loc_lantai);

        Txt_ass_nik               = (EditText) findViewById(R.id.txt_ass_nik);
        Txt_ass_name              = (TextView) findViewById(R.id.txt_ass_name);


        Txt_inv_nama_barang       = (EditText) findViewById(R.id.txt_inv_nama_barang);
        Txt_inv_tipe_barang       = (EditText) findViewById(R.id.txt_inv_tipe_barang);
        Txt_inv_ukuran_barang     = (EditText) findViewById(R.id.txt_inv_ukuran_barang);
        Txt_inv_merk_barang       = (EditText) findViewById(R.id.txt_inv_merk_barang);
        Txt_inv_tag_number        = (EditText) findViewById(R.id.txt_inv_tag_number);
        Txt_inv_note              = (EditText) findViewById(R.id.txt_inv_note);

        Txt_bgn_no_bangunan       = (EditText) findViewById(R.id.txt_bgn_no_bangunan);
        Txt_bgn_unit              = (EditText) findViewById(R.id.txt_bgn_unit);
        Txt_bgn_kecamatan         = (EditText) findViewById(R.id.txt_bgn_kecamatan);
        Txt_bgn_kota              = (EditText) findViewById(R.id.txt_bgn_kota);
        Txt_bgn_pbb               = (EditText) findViewById(R.id.txt_bgn_pbb);
        Txt_bgn_tag_number        = (EditText) findViewById(R.id.txt_bgn_tag_number);
        Txt_bgn_note              = (EditText) findViewById(R.id.txt_bgn_note);

        Txt_kdn_nopol_lama        = (EditText) findViewById(R.id.txt_kdn_nopol_lama);
        Txt_kdn_nopol_baru        = (EditText) findViewById(R.id.txt_kdn_nopol_baru);
        Txt_kdn_nama_stnk         = (EditText) findViewById(R.id.txt_kdn_nama_stnk);
        Txt_kdn_warna             = (EditText) findViewById(R.id.txt_kdn_warna);
        Txt_kdn_jenis             = (EditText) findViewById(R.id.txt_kdn_jenis);
        Txt_kdn_tahun             = (EditText) findViewById(R.id.txt_kdn_tahun);
        Txt_kdn_no_rangka         = (EditText) findViewById(R.id.txt_kdn_no_rangka);
        Txt_kdn_no_mesin          = (EditText) findViewById(R.id.txt_kdn_no_mesin);
        Txt_kdn_no_faktur         = (EditText) findViewById(R.id.txt_kdn_no_faktur);
        Txt_kdn_tgl_faktur        = (EditText) findViewById(R.id.txt_kdn_tgl_faktur);
        Txt_kdn_nama_pemakai      = (EditText) findViewById(R.id.txt_kdn_nama_pemakai);
        Txt_kdn_tag_number        = (EditText) findViewById(R.id.txt_kdn_tag_number);
        Txt_kdn_note              = (EditText) findViewById(R.id.txt_kdn_note);

        Txt_oth_attribute1        = (EditText) findViewById(R.id.txt_oth_attribute1);
        Txt_oth_attribute2        = (EditText) findViewById(R.id.txt_oth_attribute2);
        Txt_oth_attribute3        = (EditText) findViewById(R.id.txt_oth_attribute3);
        Txt_oth_attribute4        = (EditText) findViewById(R.id.txt_oth_attribute4);
        Txt_oth_attribute5        = (EditText) findViewById(R.id.txt_oth_attribute5);
        Txt_oth_attribute6        = (EditText) findViewById(R.id.txt_oth_attribute6);
        Txt_oth_attribute7        = (EditText) findViewById(R.id.txt_oth_attribute7);
        Txt_oth_attribute8        = (EditText) findViewById(R.id.txt_oth_attribute8);
        Txt_oth_attribute9        = (EditText) findViewById(R.id.txt_oth_attribute9);
        Txt_oth_attribute10       = (EditText) findViewById(R.id.txt_oth_attribute10);
        Txt_oth_attribute11       = (EditText) findViewById(R.id.txt_oth_attribute11);
        Txt_oth_attribute12       = (EditText) findViewById(R.id.txt_oth_attribute12);
        Txt_oth_attribute13       = (EditText) findViewById(R.id.txt_oth_attribute13);
        Txt_oth_attribute14       = (EditText) findViewById(R.id.txt_oth_attribute14);

        Cmd_sa_close              = (ImageButton)findViewById(R.id.cmd_sa_close);
        Cmd_sa_save               = (ImageButton)findViewById(R.id.cmd_sa_save);
        Cmd_ass_loc_lov           = (ImageButton) findViewById(R.id.cmd_ass_loc_lov);


        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();


        Cmd_sa_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               EH_CMD_CLOSE();
            }
        });
        Cmd_sa_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Save Changes Data Confirm")
                        .setMessage("Confirm Save Changes Asset Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SAVE();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        Cmd_ass_loc_lov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_LOV_GLOCATION();
            }
        });

        // get information intent
        try{
            StrSessionName  = getIntent().getStringExtra("PIC_NAME");
            StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
            StrAssetID      = getIntent().getStringExtra("ASSET_ID");

            Log.d("[GudangGaram]", "ScanAssetActivity :: StrSessionName  : " + StrSessionName);
            Log.d("[GudangGaram]", "ScanAssetActivity :: StrSessionNIK   : " + StrSessionNIK);
            Log.d("[GudangGaram]", "ScanAssetActivity :: StrAssetID      : " + StrAssetID);

            if(StrAssetID.length() > 0){
                try{
                    if(Integer.parseInt(StrAssetID) > 0){
                        EH_CMD_LOAD_ASSET_INFO(StrAssetID);
                    }
                } catch (Exception e){}
            }
        }
        catch( NullPointerException e){ }

        Log.d("[GudangGaram]", "ScanAssetActivity :: StrAssetID : " + StrAssetID);
    }

    public void EH_CMD_LOAD_ASSET_INFO(String p_asset_id) {
        String StrCat;
        Log.d("[GudangGaram]", "ScanAssetActivity :: EH_CMD_LOAD_ASSET_INFO > " + p_asset_id);
        List<CAsset> oca = dbHelper.loadScanAsset(p_asset_id);

        if(oca.size() > 0) {
            for (CAsset a : oca) {
                Log.d("[GudangGaram]", "ScanAssetActivity :: Barcode> " + a.getAsset_BrCode().toString());

                Txt_saa_asset_id.setText(a.getAsset_ID().toString());
                Txt_saa_asset_tag.setText(a.getAsset_BrCode().toString());
                Txt_saa_asset_description.setText(a.getAsset_Desc().toString());
                Txt_saa_asset_category.setText(a.getAsset_Cat().toString());
                Txt_ass_loc_code.setText(a.getLoc_Seg2().toString() + "|" + a.getLoc_Seg3().toString() + "|" + a.getLoc_Seg4().toString() + "|" + a.getLoc_Seg5().toString());
                Txt_ass_loc_provinsi.setText(a.getLoc_Seg2().toString());
                Txt_ass_loc_kota.setText(a.getLoc_Seg3().toString());
                Txt_ass_loc_id.setText(a.getAsset_Loc_ID_af().toString());
                Txt_ass_loc_lokasi.setText(a.getLoc_Seg4().toString());
                Txt_ass_loc_lantai.setText(a.getLoc_Seg5().toString());
                Txt_saa_qty.setText(a.getAsset_Qty_af().toString());
                Txt_ass_nik.setText(a.getAsset_Assign_NIK_af().toString());
                Txt_ass_name.setText(a.getAsset_Assign_Name().toString());

                StrCat = a.getAsset_Cat().toString();
                if (StrCat.equals("INV")) {
                    Log.d("[GudangGaram]", "ScanAssetActivity :: INV ");
                    // --------- access tab ------------
                    hostList.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);    // assign
                    hostList.getTabWidget().getChildAt(1).setVisibility(View.VISIBLE);    // INV
                    hostList.getTabWidget().getChildAt(2).setVisibility(View.GONE);       // BGN
                    hostList.getTabWidget().getChildAt(3).setVisibility(View.GONE);       // KDN
                    hostList.getTabWidget().getChildAt(4).setVisibility(View.GONE);       // OTH
                    hostList.setCurrentTab(0);

                    try {
                        Txt_inv_nama_barang.setText(a.getA1_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                    try {
                        Txt_inv_tipe_barang.setText(a.getA2_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                    try {
                        Txt_inv_ukuran_barang.setText(a.getA3_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                    try {
                        Txt_inv_merk_barang.setText(a.getA4_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                    try {
                        Txt_inv_tag_number.setText(a.getA8_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                    try {
                        Txt_inv_note.setText(a.getA10_af().toString());
                    } catch (java.lang.NullPointerException exception) {
                    }
                }
                else if (StrCat.equals("BGN")) {
                    // --------- access tab ------------
                    hostList.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);    // assign
                    hostList.getTabWidget().getChildAt(1).setVisibility(View.GONE);       // INV
                    hostList.getTabWidget().getChildAt(2).setVisibility(View.VISIBLE);    // BGN
                    hostList.getTabWidget().getChildAt(3).setVisibility(View.GONE);       // KDN
                    hostList.getTabWidget().getChildAt(4).setVisibility(View.GONE);       // OTH
                    hostList.setCurrentTab(0);

                    Txt_bgn_no_bangunan.setText(a.getA1_af().toString());
                    Txt_bgn_unit.setText(a.getA2_af().toString());
                    Txt_bgn_kecamatan.setText(a.getA3_af().toString());
                    Txt_bgn_kota.setText(a.getA4_af().toString());
                    Txt_bgn_pbb.setText(a.getA5_af().toString());
                    Txt_bgn_tag_number.setText(a.getA6_af().toString());
                    //Txt_bgn_notes.setText(oca.get(0).getA8_af().toString());
                }
                else if (StrCat.equals("KDN")) {
                    // --------- access tab ------------
                    hostList.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);    // assign
                    hostList.getTabWidget().getChildAt(1).setVisibility(View.GONE);       // INV
                    hostList.getTabWidget().getChildAt(2).setVisibility(View.GONE);       // BGN
                    hostList.getTabWidget().getChildAt(3).setVisibility(View.VISIBLE);    // KDN
                    hostList.getTabWidget().getChildAt(4).setVisibility(View.GONE);       // OTH
                    hostList.setCurrentTab(0);

                    Txt_kdn_nopol_lama.setText(a.getA1_af().toString());
                    Txt_kdn_nopol_baru.setText(a.getA2_af().toString());
                    Txt_kdn_nama_stnk.setText(a.getA3_af().toString());
                    Txt_kdn_warna.setText(a.getA4_af().toString());
                    Txt_kdn_jenis.setText(a.getA5_af().toString());
                    Txt_kdn_tahun.setText(a.getA6_af().toString());
                    Txt_kdn_no_rangka.setText(a.getA7_af().toString());
                    Txt_kdn_no_mesin.setText(a.getA8_af().toString());
                    Txt_kdn_no_faktur.setText(a.getA9_af().toString());
                    Txt_kdn_tgl_faktur.setText(a.getA10_af().toString());
                    Txt_kdn_nama_pemakai.setText(a.getA11_af().toString());
                    Txt_kdn_tag_number.setText(a.getA12_af().toString());
                    Txt_kdn_note.setText(a.getA14_af().toString());
                }
                else if(StrCat.equals("OTH")){
                    // --------- access tab ------------
                    hostList.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);    // assign
                    hostList.getTabWidget().getChildAt(1).setVisibility(View.GONE);       // INV
                    hostList.getTabWidget().getChildAt(2).setVisibility(View.GONE);       // BGN
                    hostList.getTabWidget().getChildAt(3).setVisibility(View.GONE);       // KDN
                    hostList.getTabWidget().getChildAt(4).setVisibility(View.VISIBLE);    // OTH
                    hostList.setCurrentTab(0);

                    Txt_oth_attribute1.setText(a.getA1_af().toString());
                    Txt_oth_attribute2.setText(a.getA2_af().toString());
                    Txt_oth_attribute3.setText(a.getA3_af().toString());
                    Txt_oth_attribute4.setText(a.getA4_af().toString());
                    Txt_oth_attribute5.setText(a.getA5_af().toString());
                    Txt_oth_attribute6.setText(a.getA6_af().toString());
                    Txt_oth_attribute7.setText(a.getA7_af().toString());
                    Txt_oth_attribute8.setText(a.getA8_af().toString());
                    Txt_oth_attribute9.setText(a.getA9_af().toString());
                    Txt_oth_attribute10.setText(a.getA10_af().toString());
                    Txt_oth_attribute11.setText(a.getA11_af().toString());
                    Txt_oth_attribute12.setText(a.getA12_af().toString());
                    Txt_oth_attribute13.setText(a.getA13_af().toString());
                    Txt_oth_attribute14.setText(a.getA14_af().toString());
                }
                else{
                    // --------- access tab ------------
                    hostList.getTabWidget().getChildAt(0).setVisibility(View.VISIBLE);    // assign
                    hostList.getTabWidget().getChildAt(1).setVisibility(View.GONE);       // INV
                    hostList.getTabWidget().getChildAt(2).setVisibility(View.GONE);       // BGN
                    hostList.getTabWidget().getChildAt(3).setVisibility(View.GONE);       // KDN
                    hostList.getTabWidget().getChildAt(4).setVisibility(View.GONE);    // OTH
                    hostList.setCurrentTab(0);
                }
            }
        }
    }

    public boolean validate_input(){
        Integer nerr = 0;
        boolean is_valid = false;

        if(Txt_saa_qty.getText().length() > 0){
            if(Integer.parseInt(Txt_saa_qty.getText().toString()) == 0){
                nerr +=1;
                Toast.makeText(getApplicationContext(),"Qty Cannot Be Zero", Toast.LENGTH_LONG).show();
            }
        }
        else{
            nerr +=1;
            Toast.makeText(getApplicationContext(),"Qty Cannot Be Blank", Toast.LENGTH_LONG).show();
        }

        if(nerr == 0){
            is_valid = true;
        }
        else{
            is_valid = false;
        }

        return is_valid;
    }


    // ============= Event Handler ===============================

    public void EH_CMD_SAVE(){
        String StrCat;
        Log.d("[GudangGaram]", "ScanAssetActivity :: EH_CMD_SAVE");

        StrCat = Txt_saa_asset_category.getText().toString();
        try {
            if (StrCat.length() > 0) {

                if(validate_input()){
                        CAsset ca = new CAsset();
                        ca.setAsset_ID(Txt_saa_asset_id.getText().toString());
                        ca.setAsset_Cat(Txt_saa_asset_category.getText().toString());
                        ca.setAsset_Qty_af(Integer.parseInt(Txt_saa_qty.getText().toString()));
                        ca.setAsset_Loc_ID_af(Integer.parseInt(Txt_ass_loc_id.getText().toString()));
                        ca.setAsset_Assign_NIK_af(Txt_ass_nik.getText().toString());
                        //ca.setAsset_Assign_Name("");

                        // ---------- get info attribute ------------
                        if (StrCat.equals("INV")) {
                            ca.setA1_af(Txt_inv_nama_barang.getText().toString());
                            ca.setA2_af(Txt_inv_tipe_barang.getText().toString());
                            ca.setA3_af(Txt_inv_ukuran_barang.getText().toString());
                            ca.setA4_af(Txt_inv_merk_barang.getText().toString());
                            ca.setA8_af(Txt_inv_tag_number.getText().toString());
                            ca.setA10_af(Txt_inv_note.getText().toString());
                        }
                        else if (StrCat.equals("BGN")) {
                            ca.setA1_af(Txt_bgn_no_bangunan.getText().toString());
                            ca.setA2_af(Txt_bgn_unit.getText().toString());
                            ca.setA3_af(Txt_bgn_kecamatan.getText().toString());
                            ca.setA4_af(Txt_bgn_kota.getText().toString());
                            ca.setA5_af(Txt_bgn_pbb.getText().toString());
                            ca.setA6_af(Txt_bgn_tag_number.getText().toString());
                        }
                        else if (StrCat.equals("KDN")) {
                            ca.setA1_af(Txt_kdn_nopol_lama.getText().toString());
                            ca.setA2_af(Txt_kdn_nopol_baru.getText().toString());
                            ca.setA3_af(Txt_kdn_nama_stnk.getText().toString());
                            ca.setA4_af(Txt_kdn_warna.getText().toString());
                            ca.setA5_af(Txt_kdn_jenis.getText().toString());
                            ca.setA6_af(Txt_kdn_tahun.getText().toString());
                            ca.setA7_af(Txt_kdn_no_rangka.getText().toString());
                            ca.setA8_af(Txt_kdn_no_mesin.getText().toString());
                            ca.setA9_af(Txt_kdn_no_faktur.getText().toString());
                            ca.setA10_af(Txt_kdn_tgl_faktur.getText().toString());
                            ca.setA11_af(Txt_kdn_nama_pemakai.getText().toString());
                            ca.setA12_af(Txt_kdn_tag_number.getText().toString());
                            ca.setA14_af(Txt_kdn_note.getText().toString());
                        }
                        else{
                            ca.setA1_af(Txt_oth_attribute1.getText().toString());
                            ca.setA2_af(Txt_oth_attribute2.getText().toString());
                            ca.setA3_af(Txt_oth_attribute3.getText().toString());
                            ca.setA4_af(Txt_oth_attribute4.getText().toString());
                            ca.setA5_af(Txt_oth_attribute5.getText().toString());
                            ca.setA6_af(Txt_oth_attribute6.getText().toString());
                            ca.setA7_af(Txt_oth_attribute7.getText().toString());
                            ca.setA8_af(Txt_oth_attribute8.getText().toString());
                            ca.setA9_af(Txt_oth_attribute9.getText().toString());
                            ca.setA10_af(Txt_oth_attribute10.getText().toString());
                            ca.setA11_af(Txt_oth_attribute11.getText().toString());
                            ca.setA12_af(Txt_oth_attribute12.getText().toString());
                            ca.setA13_af(Txt_oth_attribute13.getText().toString());
                            ca.setA14_af(Txt_oth_attribute14.getText().toString());
                        }
                        // put info scanned by
                        ca.setSCAN_BY(StrSessionNIK);

                        // save change to database
                        dbHelper.SaveEditAsset(ca);
                        EH_CMD_CLOSE();
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ScanAssetActivity :: EH_CMD_SAVE Exception : " + e.getMessage().toString());
        }

    }
    public void EH_CMD_CLOSE(){
        Log.d("[GudangGaram]", "ScanAssetActivity :: EH_CMD_CLOSE");
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        // passing intent login information before back
        upIntent.putExtra("PIC_NIK",  StrSessionNIK);
        upIntent.putExtra("PIC_NAME", StrSessionName);
        NavUtils.navigateUpTo(this, upIntent);
    }

    public void EH_CMD_LOV_GLOCATION(){
        Log.d("[GudangGaram]", "ScanAssetActivity :: EH_CMD_LOV_GLOCATION");
        // --- move focus cursor
        Txt_ass_loc_code.requestFocus();
        // --- summon lov
        lst_Location = dbHelper.LoadCboGLokasi();
        FragmentManager ListOfValue = getFragmentManager();
        LovGLokasiFragment lov_location = new LovGLokasiFragment();
        lov_location.setContext(context);
        lov_location.SetListOfValue(lst_Location);
        lov_location.show(ListOfValue,"LOV Location");

}

}
