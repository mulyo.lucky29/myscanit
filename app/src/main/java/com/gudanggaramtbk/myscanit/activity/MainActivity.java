package com.gudanggaramtbk.myscanit.activity;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.design.widget.FloatingActionButton;

import com.gudanggaramtbk.myscanit.adapter.CustomListAdapterMain;
import com.gudanggaramtbk.myscanit.util.Message;
import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.service.SoapSendAsset;
import com.gudanggaramtbk.myscanit.viewholder.ViewHolderMain;
import com.gudanggaramtbk.myscanit.model.CAsset;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private MySQLiteHelper dbHelper;
    private SharedPreferences            config;
    private String                       StrSessionNIK;
    private String                       StrSessionName;
    private String                       StrDeviceID;
    private List<CAsset>                 lvi;
    private ListView                     list1;
    private CustomListAdapterMain adapter;
    private Integer                      LPosition;
    private Message msg;
    private SoapSendAsset SendAsset;
    private MainActivity                 PActivity;
    private Boolean                      isFABOpen;
    private TextView                     rec_num;
    private FloatingActionButton         fab;
    private FloatingActionButton         fab_da;
    private FloatingActionButton         fab_sa;
    private FloatingActionButton         fab_s;
    private LinearLayout                 fab_lay_ca;
    private LinearLayout                 fab_lay_sa;
    private LinearLayout                 fab_lay_s;
    private View                         fabBGLayout;
    private Boolean                      isSelectAll;

    public void setRec_num(String Strrec_num) {
        this.rec_num.setText(Strrec_num);
    }
    private void setIconFAB(FloatingActionButton ofab, Boolean pisSelectAll){
        if(pisSelectAll == true){
            ofab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fabclearall));
        }
        else{
            ofab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fabcheckall));
        }
    }
    private void showFABMenu(){
        isFABOpen=true;
        fab_sa.setVisibility(View.VISIBLE);
        fab_da.setVisibility(View.VISIBLE);
        fab_s.setVisibility(View.VISIBLE);

        fab_sa.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fab_da.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fab_s.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu(){
        isFABOpen=false;

        fab_sa.animate().translationY(0);
        fab_da.animate().translationY(0);
        fab_s.animate().translationY(0);
        fab_s.animate().translationY(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) { }
            @Override
            public void onAnimationEnd(Animator animator) {
                if(!isFABOpen){
                    fab_sa.setVisibility(View.GONE);
                    fab_da.setVisibility(View.GONE);
                    fab_s.setVisibility(View.GONE);
                }
            }
            @Override
            public void onAnimationCancel(Animator animator) { }
            @Override
            public void onAnimationRepeat(Animator animator) { }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        isFABOpen   = false;
        isSelectAll = false;

        // --------------------------- record number --------------------------------------
        rec_num    = (TextView) findViewById(R.id.lbl_rc_main);
        // --------------------------- fab main -------------------------------------------
        fab        = (FloatingActionButton) findViewById(R.id.fab);
        fab_sa     = (FloatingActionButton) findViewById(R.id.fab_select_all);
        fab_da     = (FloatingActionButton) findViewById(R.id.fab_delete_all);
        fab_s      = (FloatingActionButton) findViewById(R.id.fab_send);

        // -------------------------- EH fab main -----------------------------------------
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }
                else{
                    closeFABMenu();
                }
            }
        });
        // --------------------------- EH fab delete all --------------------------------------
        fab_da.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list1.getCount() > 0){
                    EH_cmd_delete_all();
                }
            }
        });
        // --------------------------- EH fab select all --------------------------------------
        fab_sa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list1.getCount() > 0){
                    if(isSelectAll == true){
                        isSelectAll = false;
                        EH_cmd_clear_all();
                    }
                    else{
                        isSelectAll = true;
                        EH_cmd_select_all();
                    }
                }
                setIconFAB(fab_sa,isSelectAll);
            }
        });
        // --------------------------- EH fab send --------------------------------------------
        fab_s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list1.getCount() > 0){
                    EH_cmd_send_item();
                }
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        PActivity = this;
        msg = new Message(this);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        list1 = (ListView) findViewById(R.id.lvi_main);
        list1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LPosition = position;
                list1.setSelection(position);
            }
        });
        list1.setTextFilterEnabled(true);
        list1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String selected_asset_id  = lvi.get(position).getAsset_ID().toString();
                try{
                    EH_cmd_asset_info(selected_asset_id);
                }
                catch(Exception e){
                }
                return true;
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_device_id = (TextView) hView.findViewById(R.id.txt_xdevice_id);
        TextView nav_pic_nik   = (TextView) hView.findViewById(R.id.txt_nav_pic_nik);
        TextView nav_pic_name  = (TextView) hView.findViewById(R.id.txt_nav_pic_name);
        // --------------- set display menu info --------------------
        nav_pic_nik.setText(StrSessionNIK);
        nav_pic_name.setText(StrSessionName);
        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();

        SendAsset = new SoapSendAsset(config, this, dbHelper);
        SendAsset.setParentActivity(this);

        if(StrSessionNIK.equals("999999999")){
            fab.setVisibility(View.GONE);
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_download_asset).setVisible(false);
            menu.findItem(R.id.nav_sync).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);
        }
        else {
            fab.setVisibility(View.VISIBLE);
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_download_asset).setVisible(true);
            menu.findItem(R.id.nav_sync).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

            // -------- refresh --------
            EH_cmd_Refresh();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            if(isFABOpen) {
                closeFABMenu();
            }
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem.OnActionExpandListener onActionExpandListener = new MenuItem.OnActionExpandListener(){
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                Toast.makeText(MainActivity.this, "Action View Expanded ..",Toast.LENGTH_SHORT);
                return false;
            }
            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                Toast.makeText(MainActivity.this, "Action View Collapsed ..",Toast.LENGTH_SHORT);
                return false;
            }
        };

        MenuItem searchmenu = menu.findItem(R.id.action_search);
        final MenuItem addmenu    = menu.findItem(R.id.action_add);
        MenuItem flushmenu  = menu.findItem(R.id.action_flush);

        // -------- hide menu add if default user login --------
        if(StrSessionNIK.equals("999999999")){
            addmenu.setVisible(false);
            searchmenu.setVisible(false);
            flushmenu.setVisible(true);
        }
        else {
            addmenu.setVisible(true);
            searchmenu.setVisible(true);
            flushmenu.setVisible(false);
        }

        final SearchView searchView = (SearchView) searchmenu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d("[GudangGaram]", "MainActivity :: onCreateOptionsMenu :: onQueryTextSubmit " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Integer count = 0;
                list1.clearChoices();

                if(newText.length() > 0){
                    Log.d("[GudangGaram]", "MainActivity :: onQueryTextChange : " + newText);
                    adapter.getFilter().filter(newText);
                }
                else{
                    Log.d("[GudangGaram]", "MainActivity :: onQueryTextChange Blank ");
                    list1.clearTextFilter();
                    EH_cmd_Refresh();
                }
                count = adapter.getCount();
                adapter.notifyDataSetChanged();
                rec_num.setText(Long.toString(count) + " Records");

                return true;
            }
        });

        // -------------- Toolbar Menu Event Handler ----------------
        addmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d("[GudangGaram]", "MainActivity :: addMenu : Click ");
                EH_cmd_next_scan();
                return true;
            }
        });

        flushmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d("[GudangGaram]", "MainActivity :: flushMenu : Click ");
                EH_cmd_flush();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_search){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_setting){
            EH_cmd_setting();
        }
        else if(id == R.id.nav_download_asset){
            EH_cmd_download_asset();
        }
        else if(id == R.id.nav_sync){
            EH_cmd_sync();
        }
        else if(id == R.id.nav_about){
            EH_cmd_about();
        }
        else if(id == R.id.nav_exit){
            EH_cmd_exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // ========================= EVENT HANDLER FAB NAVIGATION ==============================

    public void EH_cmd_clear_all(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_clear_all Begin");
        adapter.EH_CMD_CLEAR_ALL();
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_clear_all End");
    }

    public void EH_cmd_select_all(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_select_all Begin");
        adapter.EH_CMD_SELECT_ALL();
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_select_all End");

    }

    public void EH_cmd_send_item(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_send_item");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Send Data Confirm ")
                .setMessage("Confirm Send Data ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_cmd_Process_send();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_cmd_delete_all(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_delete_all Begin");
        if(do_check_select_valid_error() == 0) {
            adapter.EH_CMD_DELETE_ALL(StrSessionNIK);
        }

        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_delete_all End");
    }


    // ========================= EVENT HANDLER NAVIGATION ==============================

    public void EH_cmd_Refresh(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_refresh");
        try
        {
            lvi = dbHelper.getListScannedAsset(StrSessionNIK);
            adapter = new CustomListAdapterMain(this,lvi,dbHelper, this,list1, StrSessionNIK );
            adapter.notifyDataSetChanged();
            list1.setAdapter(adapter);
            rec_num.setText(list1.getCount() + " Records");
            Log.d("[GudangGaram]", "EH_cmd_Refresh count : " +  list1.getCount());
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_cmd_next_scan(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_next_scan");
        Intent I_ScanBarcode;
        I_ScanBarcode = new Intent(this, ScanBarcodeActivity.class);
        I_ScanBarcode.putExtra("PIC_NIK",  StrSessionNIK);
        I_ScanBarcode.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_ScanBarcode);
    }

    public void EH_cmd_sync(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_sync");
        Intent I_syncPicture;
        I_syncPicture = new Intent(this, SyncAction.class);
        I_syncPicture.putExtra("PIC_NIK",  StrSessionNIK);
        I_syncPicture.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_syncPicture);
    }

    public void EH_cmd_setting(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_setting");
        Intent I_Setting;
        I_Setting = new Intent(this, SettingActivity.class);
        I_Setting.putExtra("PIC_NIK",  StrSessionNIK);
        I_Setting.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_Setting);
    }

    public void EH_cmd_about(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_about");
        Intent I_About;
        I_About = new Intent(this, AboutActivity.class);
        I_About.putExtra("PIC_NIK",  StrSessionNIK);
        I_About.putExtra("PIC_NAME", StrSessionName);
        startActivity(I_About);
    }

    public void EH_cmd_asset_info(String p_asset_id){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_asset");
        Intent I_ScanAssetInfo;
        I_ScanAssetInfo = new Intent(this, ScanAssetActivity.class);
        I_ScanAssetInfo.putExtra("PIC_NIK",   StrSessionNIK);
        I_ScanAssetInfo.putExtra("PIC_NAME",  StrSessionName);
        I_ScanAssetInfo.putExtra("ASSET_ID",  p_asset_id);
        startActivity(I_ScanAssetInfo);
    }

    public void EH_cmd_download_asset(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_donwnload_Asset");
        Intent I_DownloadAsset;
        I_DownloadAsset = new Intent(this, DownloadAssetActivity.class);
        I_DownloadAsset.putExtra("PIC_NIK",   StrSessionNIK);
        I_DownloadAsset.putExtra("PIC_NAME",  StrSessionName);
        startActivity(I_DownloadAsset);
    }

    public void EH_cmd_exit(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_exit");
        Intent objsignOut = new Intent(getBaseContext(),Login.class);
        objsignOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(objsignOut);
    }

    private Integer do_check_select_valid_error(){
        Integer err     = 0;
        Integer checked = 0;

        if(list1.getCount() == 0){
            err +=1;
            msg.Show("Item Not Available", "Item Scanned Not Available, Please Scan and Save It First Before Send");
        }
        else {
            for (int idx = 0; idx < list1.getCount(); idx++) {
                if (list1.isItemChecked(idx) == true) {
                    checked = checked + 1;
                }
            }
            if (checked == 0) {
                err += 1;
                msg.Show("None Item Selected","Please Select At Least One Item To Be Sent");
            }
        }
        return err;
    }

    public void EH_cmd_Process_send(){
        Integer rown = 0;
        List<CAsset> listselected;

        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send");
        rown = list1.getCount();

        listselected = new ArrayList<CAsset>();
        if(rown > 0){
            try{
                for (Integer idx = 0; idx < rown; idx++) {
                    Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send :: checked idx = " + idx);
                    View HoldView = list1.getAdapter().getView(idx, null, null);
                    ViewHolderMain holder = (ViewHolderMain) HoldView.getTag();

                    CAsset oa = new CAsset();
                    oa.setAsset_ID(holder.Txt_asset_id.getText().toString());
                    listselected.add(oa);
                }

                Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send > rown :" + rown);
                SendAsset.Send(listselected, StrSessionNIK, StrDeviceID);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send Exception : " + e.getMessage().toString());
            }
        }
        else{
            msg.Show("Item Not Available", "Item Scanned Not Available, Please Scan and Save It First Before Send");
        }


        /*
        if(do_check_select_valid_error() == 0) {
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send > do_check_select");
            listselected = new ArrayList<CAsset>();
            for (Integer idx = 0; idx < list1.getCount(); idx++) {
                if (list1.isItemChecked(idx) == true) {
                    Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send :: checked idx = " + idx);
                    View HoldView = list1.getAdapter().getView(idx, null, null);
                    ViewHolderMain holder = (ViewHolderMain) HoldView.getTag();

                    CAsset oa = new CAsset();
                    oa.setAsset_ID(holder.Txt_asset_id.getText().toString());
                    listselected.add(oa);
                    rown = rown + 1;
                }
            }
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send > rown :" + rown);
            if(rown > 0){
                try{
                    SendAsset.Send(listselected, StrSessionNIK, StrDeviceID);
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send Exception : " + e.getMessage().toString());
                }
            }
        } // end if
        */
    }

    public void EH_cmd_refresh_MainList(Context ctx, MainActivity pActivity){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_refresh_MainList");
        try{
            // refresh list
            try
            {
                lvi = dbHelper.getListScannedAsset(StrSessionNIK);
                adapter = new CustomListAdapterMain(ctx,lvi,dbHelper, pActivity,list1, StrSessionNIK );
                adapter.notifyDataSetChanged();
                list1.setAdapter(adapter);
                rec_num.setText(list1.getCount() + " Records");
                Log.d("[GudangGaram]", "EH_cmd_Refresh count : " +  list1.getCount());
            }
            catch (Exception e) {
                Log.d("[GudangGaram]", e.getMessage().toString());
                e.printStackTrace();
            }
            //msg.Show("Data Sent","Send Data To Oracle Complete");
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_refresh_MainList : Exception : " + e.getMessage().toString());
        }
    }

    public void EH_cmd_flush(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_flush");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Flush Data Confirm")
                .setMessage("Warning You Want To Flush All Data, This Action Is Irreversible, are you still ahead to Flush Data ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dbHelper.flush_all();
                                msg.Show("Flush Data","Data Flushed");
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
