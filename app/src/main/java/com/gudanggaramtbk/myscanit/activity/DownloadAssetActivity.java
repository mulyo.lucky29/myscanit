package com.gudanggaramtbk.myscanit.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.gudanggaramtbk.myscanit.adapter.CustomFilterAdapter;
import com.gudanggaramtbk.myscanit.adapter.CustomListAdapterDownloadAsset;
import com.gudanggaramtbk.myscanit.lov.LovKotaFragment;
import com.gudanggaramtbk.myscanit.lov.LovKotaFragmentSF;
import com.gudanggaramtbk.myscanit.lov.LovLokasiFragment;
import com.gudanggaramtbk.myscanit.lov.LovLokasiFragmentSF;
import com.gudanggaramtbk.myscanit.util.Message;
import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.service.SoapRetrieveAsset;
import com.gudanggaramtbk.myscanit.service.SoapRetrieveAssetInfo;
import com.gudanggaramtbk.myscanit.service.SoapRetrieveCountAsset;
import com.gudanggaramtbk.myscanit.model.CAsset;
import com.gudanggaramtbk.myscanit.model.CFilter;

import java.util.List;

public class DownloadAssetActivity extends AppCompatActivity {
    private MySQLiteHelper dbHelper;
    private SharedPreferences                   config;
    private String                              StrSessionNIK;
    private String                              StrSessionName;
    private String                              StrDeviceID;
    private Boolean                             ScanState;
    private DownloadAssetActivity               PActivity;
    private Message msg;
    private SoapRetrieveAsset downloadAsset;
    private SoapRetrieveCountAsset countAsset;
    private Context                             context;

    private ImageButton                         Cmd_sp_kota_lov;
    private ImageButton                         Cmd_sp_location_lov;
    private ImageButton                         Cmd_DMASSET;
    private ImageButton                         Cmd_PLASSET;
    private ImageButton                         Cmd_DLASSET;
    private ImageButton                         Cmd_DLFILTER;
    private ImageButton                         Cmd_DLSCAN;

    private TextView                            Lbl_down_count;
    private TextView                            Lbl_down_record;
    private EditText                            Txt_sp_kota;
    private EditText                            Txt_sp_location;
    private List<String>                        lst_Lokasi;
    private List<String>                        lst_Kota;
    private List<CAsset>                        lvi;
    private ListView                            lv;
    private ListView                            lvl;
    private CustomListAdapterDownloadAsset adapter;
    private SearchView                          sv;
    private TabHost                             thost;
    private Integer                             TotalPage;
    //private FloatingActionButton                Fab_filter;
    private Integer                             CurrentPage;
    private CustomFilterAdapter cua;
    private List<String>                        lst_spf_Lokasi;
    private List<String>                        lst_spf_Kota;
    private AlertDialog                         alertDialogFilter;
    private CFilter cfd;

    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_CAMERA = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_camera_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(DownloadAssetActivity.this, Manifest.permission.CAMERA);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    DownloadAssetActivity.this,
                    PERMISSIONS_CAMERA,
                    REQUEST_PERMISSION
            );
        }
    }

    public void setFilterParameters(CFilter oCf){
        cfd.setAllFilter(oCf.getFAssetBook().toString(),oCf.getFKota().toString(),oCf.getFLocation().toString(),oCf.getFData(),oCf.getQueryCondition());
    }

    // ---------------- setter ----------------------------
    public void set_Record_Count(String RecordCount){
        Lbl_down_count.setText(RecordCount);
    }

    public void MessageBox(String Title, String Message){
        msg.Show(Title,Message);
    }
    public void setTxt_sp_kota(String p_value) {
        Txt_sp_kota.setText(p_value);
    }
    public void setTxt_sp_location(String p_value) {
        Txt_sp_location.setText(p_value);
    }
    public void setTxt_spf_kota(String p_value) {
        cua.setTxt_spf_kota(p_value);
    }
    public void setTxt_spf_location(String p_value) {
        cua.setTxt_spf_location(p_value);
    }

    public void ApplyFilter() {
        try{
            EH_cmd_Refresh(cfd.getQueryCondition());
        }
        catch(Exception e){
        }
    }

    public void ConstructLOVFragmentKota(){
        // ------- summon fragment lov --------
        lst_spf_Kota = dbHelper.LoadCboKota();
        FragmentManager ListOfValue = getFragmentManager();
        LovKotaFragmentSF lov_spf_kota = new LovKotaFragmentSF();
        lov_spf_kota.SetListOfValue(lst_spf_Kota);
        lov_spf_kota.show(ListOfValue,"LOV Kota");
    }

    public void ConstructLOVFragmentLokasi(){
        lst_spf_Lokasi = dbHelper.LoadCboLokasi();
        FragmentManager ListOfValue = getFragmentManager();
        LovLokasiFragmentSF lov_spf_location = new LovLokasiFragmentSF();
        lov_spf_location.SetListOfValue(lst_spf_Lokasi);
        lov_spf_location.show(ListOfValue,"LOV Lokasi");
    }

    // --------- page retrieve operation -----------

    // ---------------------- getter --------------------
    public Integer getTotalPage() {
        return TotalPage;
    }
    public Integer getCurrentPage() {
        return CurrentPage;
    }

    // --------------------- setter ---------------------
    public void setLbl_down_record(String Strlbl_down_record) {
        Lbl_down_record.setText(Strlbl_down_record);
    }

    public void setCurrentPage(Integer currentPage) {
        CurrentPage = currentPage;
    }
    public void setTotalPage(Integer totalPage) {
        TotalPage = totalPage;
    }
    public void setIncrementCurrentPage() {
        CurrentPage = CurrentPage + 1;
    }

    public void RequestPageAssetDetail(Integer tp, Integer cp, String p_asset_book, String p_segment3, String p_segment4){
        try{
            if(tp > 0){
                if(cp <= tp){
                    SoapRetrieveAssetInfo ai = new SoapRetrieveAssetInfo(config);
                    ai.setContext(context);
                    ai.setParentActivity(PActivity);
                    ai.setDBHelper(dbHelper);
                    ai.Retrieve(tp, cp, p_asset_book, p_segment3, p_segment4);
                }
            }
        }
        catch(Exception e){
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_asset);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        context = DownloadAssetActivity.this;
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        // ================= initialized filter ================
        cfd = new CFilter();
        cfd.setAllFilter("GGPWJ","","",false,"");

        TotalPage       = 1;
        CurrentPage     = 1;

        PActivity     = this;

        msg = new Message(this);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        ScanState       = config.getBoolean("ScanMode",false);
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //Fab_filter          = (FloatingActionButton) findViewById(R.id.fab_filter);
        Cmd_sp_kota_lov     = (ImageButton)          findViewById(R.id.cmd_sp_kota_lov);
        Cmd_sp_location_lov = (ImageButton)          findViewById(R.id.cmd_sp_location_lov);
        Cmd_DMASSET         = (ImageButton)          findViewById(R.id.cmd_down_asset);
        Cmd_PLASSET         = (ImageButton)          findViewById(R.id.cmd_down_refresh);
        Cmd_DLASSET         = (ImageButton)          findViewById(R.id.cmd_dll_refresh);
        Cmd_DLFILTER        = (ImageButton)          findViewById(R.id.cmd_dll_filter);
        Cmd_DLSCAN          = (ImageButton)          findViewById(R.id.cmd_dll_scan);

        Txt_sp_kota         = (EditText)             findViewById(R.id.txt_sp_kota);
        Txt_sp_location     = (EditText)             findViewById(R.id.txt_sp_location);
        Lbl_down_record     = (TextView)             findViewById(R.id.lbl_down_record);
        Lbl_down_count      = (TextView)             findViewById(R.id.lbl_down_count);
        lv                  = (ListView)             findViewById(R.id.lvi_dlasset_list);
        sv                  = (SearchView)           findViewById(R.id.txt_dl_search);

        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv.setClickable(true);
        //lv.setTextFilterEnabled(true);

        // search filter
        sv.setQueryHint("Search Asset Downloded Here..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String txt) {
                Integer count = 0;
                try{
                    if(txt.length() > 0){
                        Log.d("[GudangGaram]", "DownloadAssetActivity :: onQueryTextChange : " + txt);
                        adapter.getFilter().filter(txt);
                    }
                    else{
                        Log.d("[GudangGaram]", "DownloadAssetActivity :: onQueryTextChange Blank ");
                        EH_cmd_Refresh(cfd.getQueryCondition());
                    }
                    count = adapter.getCount();
                    adapter.notifyDataSetChanged();

                    Lbl_down_record.setText(Long.toString(count) + " Records");

                    Log.d("[GudangGaram]", "DownloadAssetActivity :: onQueryTextChange : " + txt + " >> " + count);
                }
                catch(Exception e){
                }
                return true;
            }
        });


        thost = (TabHost)findViewById(R.id.TabDownAsset);
        thost.setup();
        TabHost.TabSpec spec = thost.newTabSpec("Download Wizard");
        try{
            //Tab 1
            spec.setContent(R.id.MTab_DownWiz);
            spec.setIndicator("Download Wizard");
            thost.addTab(spec);

            //Tab 2
            spec = thost.newTabSpec("Downloaded Asset");
            spec.setContent(R.id.MTab_DownList);
            spec.setIndicator("Downloaded Asset");
            thost.addTab(spec);
        }
        catch(Exception e){
        }

        // service to download asset list
        downloadAsset = new SoapRetrieveAsset(config);
        downloadAsset.setContext(context);
        downloadAsset.setParentActivity(PActivity);
        downloadAsset.setDBHelper(dbHelper);

        // service to count asset list
        countAsset = new SoapRetrieveCountAsset(config);
        countAsset.setContext(context);
        countAsset.setParetActivity(PActivity);
        countAsset.setDBHelper(dbHelper);

        // class to render dialog view
        cua = new CustomFilterAdapter(config);
        cua.setContext(context);
        cua.setParentAtivity(PActivity);
        cua.setDBHelper(dbHelper);

        if(ScanState == Boolean.TRUE){
            check_camera_permission();
            Cmd_DLSCAN.setVisibility(View.VISIBLE);
        }
        else{
            Cmd_DLSCAN.setVisibility(View.INVISIBLE);
        }

        // ----------- list of value popup -------------
        Cmd_sp_kota_lov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ------- move focus on textfield --------
                Txt_sp_kota.requestFocus();
                // ------- summon fragment lov --------
                lst_Kota = dbHelper.LoadCboKota();
                FragmentManager ListOfValue = getFragmentManager();;
                LovKotaFragment lov_kota = new LovKotaFragment();
                lov_kota.SetListOfValue(lst_Kota);
                lov_kota.show(ListOfValue,"LOV Kota");
            }
        });

        Cmd_sp_location_lov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ------- move focus on textfield --------
                Txt_sp_location.requestFocus();
                // ------- summon fragment lov ---------
                lst_Lokasi = dbHelper.LoadCboLokasi();
                FragmentManager ListOfValue = getFragmentManager();;
                LovLokasiFragment lov_location = new LovLokasiFragment();
                lov_location.SetListOfValue(lst_Lokasi);
                lov_location.show(ListOfValue,"LOV Lokasi");
            }
        });

        // --------- action button download  -----------
        Cmd_DMASSET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Download Asset Data Confirm")
                        .setMessage("Confirm Download Asset Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SYNC_ASSET();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        // ----------- action button load ---------------
        Cmd_PLASSET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_GetCountWS();
            }
        });
        // ----------- action button refresh offline loaded asset  -----------
        Cmd_DLASSET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_Refresh(cfd.getQueryCondition());
            }
        });
        // ---------- action button scan --------------
        Cmd_DLSCAN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SCAN();
            }
        });

        // -------- FAB button for prefilter dialog popup --------
        Cmd_DLFILTER.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_show_prefilter();
            }
        });

    }

    public void EH_cmd_show_prefilter(){
        View promptsView;
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_show_filter : Begin");

        try{
            promptsView = cua.getView(cfd);
            alertDialogFilter = new AlertDialog.Builder(context).create();
            alertDialogFilter.setTitle("Filter Asset Data");
            alertDialogFilter.setView(promptsView);
            alertDialogFilter.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialogFilter.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_show_filter : Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_show_filter : End");
        }
    }

    public void EH_cmd_close_filter(){
        try{
            alertDialogFilter.dismiss();
        }
        catch(Exception e){
        }
    }

    public void EH_cmd_Refresh(String pCondition){
        long recordcount  = 0 ;
        String xp_kota, xp_lokasi;
        Boolean xp_not_scan;
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_refresh");

        try
        {
            xp_kota   = Txt_sp_kota.getText().toString();
            xp_lokasi = Txt_sp_location.getText().toString();

            lvi = dbHelper.getListDownloadAsset(pCondition);
            recordcount = lvi.size();
            if(recordcount > 0){
                adapter = new CustomListAdapterDownloadAsset(this, lvi);
                adapter.setParentActivity(this);

                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
            }
            else{
                lv.setAdapter(null);
                msg.Show("Populate","Reload Return Zero Result");
            }
            Lbl_down_record.setText(Long.toString(recordcount) + " Records");
            Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_Refresh count : " +  lv.getCount());
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_Refresh Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {

        }
    }

    public void EH_cmd_GetCountWS(){
        String p_asset_book;
        String p_kota;
        String p_lokasi;
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_GetCountWS :: Begin");

        p_asset_book = "GGPWJ";
        p_kota       = Txt_sp_kota.getText().toString();
        p_lokasi     = Txt_sp_location.getText().toString();

        if(p_kota.length() > 0 && p_lokasi.length() > 0){
            try{
                countAsset.Count(p_asset_book, p_kota, p_lokasi);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_GetCountWS :: Exception : " + e.getMessage().toString());
            }
        }
        else{
            msg.Show("Please Check Parameter","Please Check Input Parameter !");
        }
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_cmd_GetCountWS :: End");
    }

    public void EH_CMD_SYNC_ASSET() {
        String p_asset_book;
        String p_kota;
        String p_lokasi;

        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_CMD_SYNC_ASSET :: Begin");
        p_asset_book = "GGPWJ";
        p_kota       = Txt_sp_kota.getText().toString();
        p_lokasi     = Txt_sp_location.getText().toString();

        // reset current page
        CurrentPage = 1;
        if(p_kota.length() > 0 && p_lokasi.length() > 0){
            try{
                downloadAsset.Retrieve(p_asset_book, p_kota, p_lokasi);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_CMD_SYNC_ASSET :: Exception : " + e.getMessage().toString());
            }
        }
        else{
            msg.Show("Please Check Parameter","Please Check Input Parameter !");
        }
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_CMD_SYNC_ASSET :: End");
    }

    public void EH_CMD_SCAN(){
        Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_CMD_SCAN");
        IntentIntegrator SI_Scan_asset;
        sv.setFocusable(true);
        sv.requestFocus();

        try {
            SI_Scan_asset = new IntentIntegrator(this);
            SI_Scan_asset.setCaptureActivity(CaptureCustomeActivity.class);
            SI_Scan_asset.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            SI_Scan_asset.setCameraId(0);
            SI_Scan_asset.setBeepEnabled(true);
            SI_Scan_asset.setBarcodeImageEnabled(true);
            SI_Scan_asset.initiateScan();
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_CMD_SCAN Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "DownloadAssetActivity :: upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",   StrSessionNIK);
                    upIntent.putExtra("PIC_NAME",  StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult SR_Barcode;
        String StrScanBarcode;

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                }
                else
                {
                    SR_Barcode = IntentIntegrator.parseActivityResult(requestCode,resultCode,intent);
                    if(SR_Barcode != null){
                        try{
                            StrScanBarcode = SR_Barcode.getContents();
                            sv.setQuery(StrScanBarcode, true);
                        }
                        catch(Exception e){
                        }
                    }
                }
                break;
            }
        }
    }

}
