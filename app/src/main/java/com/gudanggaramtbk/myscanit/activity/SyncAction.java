package com.gudanggaramtbk.myscanit.activity;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.util.Message;
import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.service.SoapRetrieveLoc;
import com.gudanggaramtbk.myscanit.service.SoapRetrieveLookup;
import com.gudanggaramtbk.myscanit.service.SoapRetrievePIC;

public class SyncAction extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private SoapRetrievePIC downloadPIC;
    private SoapRetrieveLoc downloadLoc;
    private SoapRetrieveLookup downloadLOOK;

    private String StrSessionNIK;
    private String StrSessionName;
    private String DeviceID;

    private String StrSelectedKota;
    private String StrSelectedLokasi;

    private TextView Lbl_rec_DMLookup;
    private TextView Lbl_rec_DMLoc;
    private TextView Lbl_rec_DMPIC;

    private ImageButton Cmd_DMPIC;
    private ImageButton Cmd_DMLOC;
    private ImageButton Cmd_DMLOOK;
    private ImageButton Cmd_del_DMPIC;
    private ImageButton Cmd_del_DMLoc;
    private ImageButton Cmd_del_DMLookup;
    private Button      Cmd_Backup_DB;

    private SearchView Srch_sp_location;
    private ListView lv_location;
    private SyncAction pActivity;
    private Message msg;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Sync Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",  StrSessionNIK);
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("[GudangGaram]", "SyncAction :: OnCreate Begin");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_action);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context = SyncAction.this;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        DeviceID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        // get information intent
        StrSessionName = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK = getIntent().getStringExtra("PIC_NIK");

        msg = new Message(context);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        pActivity = this;

        // service to download master location
        downloadLoc = new SoapRetrieveLoc(config);
        downloadLoc.setContext(context);
        downloadLoc.setParentActivity(pActivity);
        downloadLoc.setDBHelper(dbHelper);

        // service to download master pic
        downloadPIC = new SoapRetrievePIC(config);
        downloadPIC.setContext(context);
        downloadPIC.setParentActivity(pActivity);
        downloadPIC.setDBHelper(dbHelper);

        // service to download master pic
        downloadLOOK = new SoapRetrieveLookup(config);
        downloadLOOK.setContext(context);
        downloadLOOK.setParentActivity(pActivity);
        downloadLOOK.setDBHelper(dbHelper);

        Cmd_DMPIC        = (ImageButton) findViewById(R.id.cmd_down_DMPIC);
        Cmd_DMLOC        = (ImageButton) findViewById(R.id.cmd_down_DMLoc);
        Cmd_DMLOOK       = (ImageButton) findViewById(R.id.cmd_down_DMLookup);
        Cmd_del_DMPIC    = (ImageButton) findViewById(R.id.cmd_del_DMPIC);
        Cmd_del_DMLoc    = (ImageButton) findViewById(R.id.cmd_del_DMLoc);
        Cmd_del_DMLookup = (ImageButton) findViewById(R.id.cmd_del_DMLookup);
        Lbl_rec_DMLookup = (TextView) findViewById(R.id.lbl_rec_DMLookup);
        Lbl_rec_DMLoc    = (TextView) findViewById(R.id.lbl_rec_DMLoc);
        Lbl_rec_DMPIC    = (TextView) findViewById(R.id.lbl_rec_DMPIC);


        Cmd_DMPIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_PIC");
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Download PIC Data Confirm")
                        .setMessage("Confirm Download PIC Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SYNC_M_PIC();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        Cmd_DMLOC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Download Location Data Confirm")
                        .setMessage("Confirm Download Location Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SYNC_M_LOC();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });
        Cmd_DMLOOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Download Lookup Data Confirm")
                        .setMessage("Confirm Download Lookup Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SYNC_M_LOOK();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_del_DMPIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Delete PIC Data Confirm")
                        .setMessage("Confirm Delete PIC Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DEL_PIC();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_del_DMLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Delete Location Data Confirm")
                        .setMessage("Confirm Delete Location Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DEL_LOC();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_del_DMLookup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Delete Lookup Data Confirm")
                        .setMessage("Confirm Delete Lookup Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DEL_LOOK();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        EH_Reload_Record_Count("all");

        Log.d("[GudangGaram]", "SyncAction :: OnCreate End");
    }

    public void EH_Reload_Record_Count(String Type){
        Log.d("[GudangGaram]", "SyncAction :: EH_Reload_Record_Count :: Begin");
        try{
            if(Type.equals("look")){
                Lbl_rec_DMLookup.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMLookup())) + " Records");
            }
            else if(Type.equals("loc")){
                Lbl_rec_DMLoc.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMLocation())) + " Records");
            }
            else if(Type.equals("pic")){
                Lbl_rec_DMPIC.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMPerson())) + " Records");
            }
            else{
                Lbl_rec_DMLookup.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMLookup())) + " Records");
                Lbl_rec_DMLoc.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMLocation())) + " Records");
                Lbl_rec_DMPIC.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMPerson())) + " Records");
            }
       }
       catch(Exception e){
           Log.d("[GudangGaram]", "SyncAction :: EH_Reload_Record_Count Exception : " + e.getMessage().toString());
       }
       finally {
            Log.d("[GudangGaram]", "SyncAction :: EH_Reload_Record_Count :: End");
        }
    }

    public void EH_CMD_DEL_PIC(){
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_PIC :: Begin");
        try{
            dbHelper.flushTable(dbHelper.getTableMPerson());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_PIC :: Exception : " + e.getMessage().toString());
        }
        finally {
            EH_Reload_Record_Count("pic");
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_PIC :: End");
        }
    }

    public void EH_CMD_DEL_LOC(){
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOC :: Begin");
        try{
            dbHelper.flushTable(dbHelper.getTableMLocation());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOC :: Exception : " + e.getMessage().toString());
        }
        finally {
            EH_Reload_Record_Count("loc");
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOC :: End");
        }
    }

    public void EH_CMD_DEL_LOOK(){
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOOK :: Begin");
        try{
            dbHelper.flushTable(dbHelper.getTableMLookup());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOOK :: Exception : " + e.getMessage().toString());
        }
        finally {
            EH_Reload_Record_Count("look");
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_DEL_LOOK :: End");
        }
    }

    public void EH_CMD_SYNC_M_PIC() {
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_PIC :: Begin");
        try{
            downloadPIC.Retrieve();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_PIC :: Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_PIC :: End");
        }
    }

    public void EH_CMD_SYNC_M_LOC() {
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOC :: Begin");
        try{
            downloadLoc.Retrieve();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOC :: Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOC :: End");
        }
    }

    public void EH_CMD_SYNC_M_LOOK() {
        Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOOK :: Begin");
        try{
            downloadLOOK.Retrieve();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOOK :: Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SyncAction :: EH_CMD_SYNC_M_LOOK :: End");
        }
    }

}
