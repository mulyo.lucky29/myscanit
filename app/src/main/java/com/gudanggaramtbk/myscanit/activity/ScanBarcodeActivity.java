package com.gudanggaramtbk.myscanit.activity;

import android.Manifest;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import java.util.Timer;
import java.util.TimerTask;

public class ScanBarcodeActivity extends AppCompatActivity {
    private MySQLiteHelper dbHelper;
    private SharedPreferences        config;
    private String                   StrSessionNIK;
    private String                   StrSessionName;
    private String                   StrAssetID;
    private Boolean                  ScanState;
    private ImageView                Imv_main;
    private ImageButton              Cmd_asc_scan;
    private Button                   Cmd_asc_next;
    private EditText                 Txt_asc_barcode_tag;
    private TextView                 Txt_error_message;
    private Timer                    timer1,timer2;
    private FloatingActionButton     Fab_filter;
    private Camera                   mCamera;
    public Camera.Parameters         params;

    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_CAMERA = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_camera_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(ScanBarcodeActivity.this, Manifest.permission.CAMERA);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    ScanBarcodeActivity.this,
                    PERMISSIONS_CAMERA,
                    REQUEST_PERMISSION
            );
        }
    }

    public void display_error(final String errm){
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
                Txt_error_message.setText(errm);

            }
            public void onFinish() {
                Txt_error_message.setText("");
                Txt_asc_barcode_tag.setText("");
                Txt_asc_barcode_tag.setFocusable(true);
            }
        }.start();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrSessionNIK   = getIntent().getStringExtra("PIC_NIK");
        ScanState       = config.getBoolean("ScanMode",false);
        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        Imv_main                  = (ImageView)            findViewById(R.id.imv_main);
        Cmd_asc_scan              = (ImageButton)          findViewById(R.id.cmd_asc_scan);
        Cmd_asc_next              = (Button)               findViewById(R.id.cmd_asc_next);
        Txt_asc_barcode_tag       = (EditText)             findViewById(R.id.txt_asc_barcode_tag);
        Txt_error_message         = (TextView)             findViewById(R.id.txt_error_message);

        Txt_asc_barcode_tag.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0) {
                    timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    EH_CMD_NEXT();
                                }
                            });
                        }

                    }, 600);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 if(count > 0) {
                     // user is typing: reset already started timer (if existing)
                     if (timer2 != null)
                         timer2.cancel();
                 }
            }
        });

        if(ScanState == Boolean.TRUE){
            check_camera_permission();
            Cmd_asc_scan.setVisibility(View.VISIBLE);
        }
        else{
            Cmd_asc_scan.setVisibility(View.INVISIBLE);
        }
        Cmd_asc_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SCAN();
            }
        });
        Cmd_asc_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_NEXT();
            }
        });
    }

    // --------------------------------------- event handler ----------------------------------------
    public void EH_CMD_SCAN(){
        Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_SCAN");
        IntentIntegrator SI_Scan_asset;
        Txt_asc_barcode_tag.setFocusable(true);
        Txt_asc_barcode_tag.requestFocus();

        try {
            SI_Scan_asset = new IntentIntegrator(this);
            SI_Scan_asset.setCaptureActivity(CaptureCustomeActivity.class);
            SI_Scan_asset.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            SI_Scan_asset.setCameraId(0);
            SI_Scan_asset.setBeepEnabled(true);
            SI_Scan_asset.setBarcodeImageEnabled(true);
            SI_Scan_asset.initiateScan();
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_SCAN Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void EH_CMD_NEXT(){
        Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_NEXT");

        Integer already_scanned;
        Integer asset_id;
        String asset_tag;
        String errm;
        String p_asset_id = "";
        Intent I_ScanAssetInfo;
        Integer errc;

        already_scanned = 0;
        errc = 0;
        errm = "";

        try{
            asset_tag = Txt_asc_barcode_tag.getText().toString();
            asset_id  = dbHelper.getAssetIDFromBarcode(asset_tag);
            Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_NEXT : Asset ID : " + asset_id);

            if(asset_id > 0){
                already_scanned = dbHelper.getCountScannedBarcode(asset_tag);
                Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_NEXT : already_scanned : " + already_scanned);

                if(already_scanned > 0){
                    errc +=1;
                    errm = "This Barcode Already Scanned, Please Try Other !";
                }
                else{
                    Log.d("[GudangGaram]", "ScanBarcodeActivity :: Asset Info Page Called");
                    p_asset_id = Integer.toString(asset_id);
                    I_ScanAssetInfo = new Intent(this, ScanAssetActivity.class);
                    I_ScanAssetInfo.putExtra("PIC_NIK",   StrSessionNIK);
                    I_ScanAssetInfo.putExtra("PIC_NAME",  StrSessionName);
                    I_ScanAssetInfo.putExtra("ASSET_ID",  p_asset_id);
                    startActivity(I_ScanAssetInfo);
                }
            }
            else{
                errc +=1;
                errm = "Invalid Asset Tag Barcode, Cannot Translated To Asset ID";
                Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_NEXT :: Asset_ID < 0");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ScanBarcodeActivity :: EH_CMD_NEXT : Exception " + e.getMessage().toString());
            errc +=1;
            errm = "Scan Exception : " + e.getMessage().toString();
        }
        finally {
            if(errc > 0){
                display_error(errm);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult SR_Asset;
        String StrScanAsset;

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                }
                else
                {
                    SR_Asset = IntentIntegrator.parseActivityResult(requestCode,resultCode,intent);
                    if(SR_Asset != null){
                        try{
                            StrScanAsset = SR_Asset.getContents();
                            Txt_asc_barcode_tag.setText(StrScanAsset);
                        }
                        catch(Exception e){
                        }
                        EH_CMD_NEXT();
                    }
                }
                break;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NIK",   StrSessionNIK);
                    upIntent.putExtra("PIC_NAME",  StrSessionName);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
