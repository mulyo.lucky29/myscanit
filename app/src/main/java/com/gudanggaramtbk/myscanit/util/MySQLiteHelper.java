package com.gudanggaramtbk.myscanit.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.gudanggaramtbk.myscanit.model.CAsset;
import com.gudanggaramtbk.myscanit.model.CLocation;
import com.gudanggaramtbk.myscanit.model.CLookup;
import com.gudanggaramtbk.myscanit.model.CPic;
import com.gudanggaramtbk.myscanit.model.StringWithTag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by luckym on 1/31/2019.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyScanITDB.db";
    private static Cursor XMICursor;

    // ====================== table define ============================
    private static final String TABLE_M_PERSON   = "GGGG_SCANIT_M_PERSON";
    private static final String TABLE_M_LOOKUP   = "GGGG_SCANIT_M_LOOKUP";
    private static final String TABLE_M_LOCATION = "GGGG_SCANIT_M_LOCATION";
    private static final String TABLE_M_ASSET    = "GGGG_SCANIT_M_ASSET";

    // ====================== view define =============================
    private static final String VIEW_LOGIN = "GGGG_SCANIT_LOGIN_V";

    // =========== Master Person ======================================
    private static final String KEY_M_PERSON_ID        = "Person_ID";
    private static final String KEY_M_PERSON_NIK       = "Person_NIK";
    private static final String KEY_M_PERSON_NAME      = "Person_Name";

    // =========== Master Lookup ================================
    private static final String KEY_M_LOOKUP_CONTEXT   = "LContext";
    private static final String KEY_M_LOOKUP_VALUE     = "LValue";
    private static final String KEY_M_LOOKUP_MEANNG    = "LMeaning";

    // =========== Master Location ================================
    private static final String KEY_M_LOCATION_ID      = "Location_ID";
    private static final String KEY_M_LOCATION_CODE    = "Location_Code";
    private static final String KEY_M_LOCATION_SEG1    = "Segment1";
    private static final String KEY_M_LOCATION_SEG2    = "Segment2";
    private static final String KEY_M_LOCATION_SEG3    = "Segment3";
    private static final String KEY_M_LOCATION_SEG4    = "Segment4";
    private static final String KEY_M_LOCATION_SEG5    = "Segment5";

    // =========== Master Asset ================================
    private static final String KEY_M_ASSET_TRX_ID          = "Asset_Trx_ID";
    private static final String KEY_M_ASSET_ID              = "Asset_ID";
    private static final String KEY_M_ASSET_BOOK            = "Asset_Book";
    private static final String KEY_M_ASSET_BRCODE          = "Asset_BrCode";
    private static final String KEY_M_ASSET_DESC            = "Asset_Desc";
    private static final String KEY_M_ASSET_CAT_ID          = "Asset_Cat_ID";
    private static final String KEY_M_ASSET_CAT             = "Asset_Cat";
    private static final String KEY_M_ASSET_LOC_ID_AF       = "Asset_Loc_ID_af";
    private static final String KEY_M_ASSET_LOC_ID_BF       = "Asset_Loc_ID_bf";
    private static final String KEY_M_ASSET_ASSIGN_ID       = "Asset_Assign_ID";
    private static final String KEY_M_ASSET_ASSIGN_NAME     = "Asset_Assign_Name";
    private static final String KEY_M_ASSET_ASSIGN_NIK_AF   = "Asset_Assign_NIK_af";
    private static final String KEY_M_ASSET_ASSIGN_NIK_BF   = "Asset_Assign_NIK_bf";
    private static final String KEY_M_ASSET_QTY_AF          = "Asset_Qty_Af";
    private static final String KEY_M_ASSET_QTY_BF          = "Asset_Qty_Bf";
    private static final String KEY_M_ASSET_A1_AF           = "Asset_A1_af";
    private static final String KEY_M_ASSET_A2_AF           = "Asset_A2_af";
    private static final String KEY_M_ASSET_A3_AF           = "Asset_A3_af";
    private static final String KEY_M_ASSET_A4_AF           = "Asset_A4_af";
    private static final String KEY_M_ASSET_A5_AF           = "Asset_A5_af";
    private static final String KEY_M_ASSET_A6_AF           = "Asset_A6_af";
    private static final String KEY_M_ASSET_A7_AF           = "Asset_A7_af";
    private static final String KEY_M_ASSET_A8_AF           = "Asset_A8_af";
    private static final String KEY_M_ASSET_A9_AF           = "Asset_A9_af";
    private static final String KEY_M_ASSET_A10_AF          = "Asset_A10_af";
    private static final String KEY_M_ASSET_A11_AF          = "Asset_A11_af";
    private static final String KEY_M_ASSET_A12_AF          = "Asset_A12_af";
    private static final String KEY_M_ASSET_A13_AF          = "Asset_A13_af";
    private static final String KEY_M_ASSET_A14_AF          = "Asset_A14_af";
    private static final String KEY_M_ASSET_A15_AF          = "Asset_A15_af";
    private static final String KEY_M_ASSET_A16_AF          = "Asset_A16_af";
    private static final String KEY_M_ASSET_A17_AF          = "Asset_A17_af";
    private static final String KEY_M_ASSET_A18_AF          = "Asset_A18_af";
    private static final String KEY_M_ASSET_A19_AF          = "Asset_A19_af";
    private static final String KEY_M_ASSET_A20_AF          = "Asset_A20_af";
    private static final String KEY_M_ASSET_A1_BF           = "Asset_A1_bf";
    private static final String KEY_M_ASSET_A2_BF           = "Asset_A2_bf";
    private static final String KEY_M_ASSET_A3_BF           = "Asset_A3_bf";
    private static final String KEY_M_ASSET_A4_BF           = "Asset_A4_bf";
    private static final String KEY_M_ASSET_A5_BF           = "Asset_A5_bf";
    private static final String KEY_M_ASSET_A6_BF           = "Asset_A6_bf";
    private static final String KEY_M_ASSET_A7_BF           = "Asset_A7_bf";
    private static final String KEY_M_ASSET_A8_BF           = "Asset_A8_bf";
    private static final String KEY_M_ASSET_A9_BF           = "Asset_A9_bf";
    private static final String KEY_M_ASSET_A10_BF          = "Asset_A10_bf";
    private static final String KEY_M_ASSET_A11_BF          = "Asset_A11_bf";
    private static final String KEY_M_ASSET_A12_BF          = "Asset_A12_bf";
    private static final String KEY_M_ASSET_A13_BF          = "Asset_A13_bf";
    private static final String KEY_M_ASSET_A14_BF          = "Asset_A14_bf";
    private static final String KEY_M_ASSET_A15_BF          = "Asset_A15_bf";
    private static final String KEY_M_ASSET_A16_BF          = "Asset_A16_bf";
    private static final String KEY_M_ASSET_A17_BF          = "Asset_A17_bf";
    private static final String KEY_M_ASSET_A18_BF          = "Asset_A18_bf";
    private static final String KEY_M_ASSET_A19_BF          = "Asset_A19_bf";
    private static final String KEY_M_ASSET_A20_BF          = "Asset_A20_bf";
    private static final String KEY_M_ASSET_DOWNLOAD_DATE   = "Download_Date";
    private static final String KEY_M_ASSET_SCAN_DATE       = "Scan_Date";
    private static final String KEY_M_ASSET_SCAN_BY         = "Scan_By";


    public static final String CREATE_PERSON_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_PERSON + " ( " +
            KEY_M_PERSON_ID + " INTEGER PRIMARY KEY, " +
            KEY_M_PERSON_NIK + " TEXT, " +
            KEY_M_PERSON_NAME + " TEXT); ";



    public static final String CREATE_INDEX_ASSET = "CREATE INDEX IF NOT EXISTS idxAssetBrC ON " + TABLE_M_ASSET + " (" + KEY_M_ASSET_BRCODE + ");";

    // TEXT PRIMARY KEY,
    public static final String CREATE_ASSET_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_ASSET + " ( " +
            KEY_M_ASSET_ID + " INTEGER PRIMARY KEY, " +
            KEY_M_ASSET_BOOK + " TEXT, " +
            KEY_M_ASSET_BRCODE + " TEXT, " +
            KEY_M_ASSET_DESC + " TEXT, " +
            KEY_M_ASSET_CAT_ID + " INTEGER, " +
            KEY_M_ASSET_CAT + " TEXT, " +
            KEY_M_ASSET_LOC_ID_AF + " INTEGER, " +
            KEY_M_ASSET_LOC_ID_BF + " INTEGER, " +
            KEY_M_ASSET_ASSIGN_ID + " INTEGER, " +
            KEY_M_ASSET_ASSIGN_NAME + " TEXT, " +
            KEY_M_ASSET_ASSIGN_NIK_AF + " TEXT, " +
            KEY_M_ASSET_ASSIGN_NIK_BF + " TEXT, " +
            KEY_M_ASSET_QTY_AF + " INTEGER, " +
            KEY_M_ASSET_QTY_BF + " INTEGER, " +
            KEY_M_ASSET_A1_BF + " TEXT, " +
            KEY_M_ASSET_A1_AF + " TEXT, " +
            KEY_M_ASSET_A2_BF + " TEXT, " +
            KEY_M_ASSET_A2_AF + " TEXT, " +
            KEY_M_ASSET_A3_BF + " TEXT, " +
            KEY_M_ASSET_A3_AF + " TEXT, " +
            KEY_M_ASSET_A4_BF + " TEXT, " +
            KEY_M_ASSET_A4_AF + " TEXT, " +
            KEY_M_ASSET_A5_BF + " TEXT, " +
            KEY_M_ASSET_A5_AF + " TEXT, " +
            KEY_M_ASSET_A6_BF + " TEXT, " +
            KEY_M_ASSET_A6_AF + " TEXT, " +
            KEY_M_ASSET_A7_BF + " TEXT, " +
            KEY_M_ASSET_A7_AF + " TEXT, " +
            KEY_M_ASSET_A8_BF + " TEXT, " +
            KEY_M_ASSET_A8_AF + " TEXT, " +
            KEY_M_ASSET_A9_BF + " TEXT, " +
            KEY_M_ASSET_A9_AF + " TEXT, " +
            KEY_M_ASSET_A10_BF + " TEXT, " +
            KEY_M_ASSET_A10_AF + " TEXT, " +
            KEY_M_ASSET_A11_BF + " TEXT, " +
            KEY_M_ASSET_A11_AF + " TEXT, " +
            KEY_M_ASSET_A12_BF + " TEXT, " +
            KEY_M_ASSET_A12_AF + " TEXT, " +
            KEY_M_ASSET_A13_BF + " TEXT, " +
            KEY_M_ASSET_A13_AF + " TEXT, " +
            KEY_M_ASSET_A14_BF + " TEXT, " +
            KEY_M_ASSET_A14_AF + " TEXT, " +
            KEY_M_ASSET_A15_BF + " TEXT, " +
            KEY_M_ASSET_A15_AF + " TEXT, " +
            KEY_M_ASSET_A16_BF + " TEXT, " +
            KEY_M_ASSET_A16_AF + " TEXT, " +
            KEY_M_ASSET_A17_BF + " TEXT, " +
            KEY_M_ASSET_A17_AF + " TEXT, " +
            KEY_M_ASSET_A18_BF + " TEXT, " +
            KEY_M_ASSET_A18_AF + " TEXT, " +
            KEY_M_ASSET_A19_BF + " TEXT, " +
            KEY_M_ASSET_A19_AF + " TEXT, " +
            KEY_M_ASSET_A20_BF + " TEXT, " +
            KEY_M_ASSET_A20_AF + " TEXT, " +
            KEY_M_ASSET_DOWNLOAD_DATE + " TEXT, " +
            KEY_M_ASSET_SCAN_DATE + " TEXT, " +
            KEY_M_ASSET_SCAN_BY + " TEXT);";

    public static final String CREATE_LOCATION_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_LOCATION + " ( " +
            KEY_M_LOCATION_ID + " INTEGER PRIMARY KEY, " +
            KEY_M_LOCATION_CODE + " TEXT, " +
            KEY_M_LOCATION_SEG1 + " TEXT, " +
            KEY_M_LOCATION_SEG2 + " TEXT, " +
            KEY_M_LOCATION_SEG3 + " TEXT, " +
            KEY_M_LOCATION_SEG4 + " TEXT, " +
            KEY_M_LOCATION_SEG5 + " TEXT); ";

    private static final String CREATE_LOOKUP_TABLE = " CREATE TABLE IF NOT EXISTS " + TABLE_M_LOOKUP + " ( " +
            KEY_M_LOOKUP_CONTEXT + " TEXT, " +
            KEY_M_LOOKUP_VALUE + " TEXT, " +
            KEY_M_LOOKUP_MEANNG + " TEXT, " +
            "PRIMARY KEY(" + KEY_M_LOOKUP_CONTEXT + "," + KEY_M_LOOKUP_VALUE + ")" + "); ";

    private static final String CREATE_LOGIN_VW = " CREATE VIEW IF NOT EXISTS " + VIEW_LOGIN + " " +
            "  AS " +
            "  SELECT Person_ID, Person_NIK, Person_Name " +
            "  FROM ( " +
            "  SELECT " + KEY_M_PERSON_ID + " as Person_ID," +
            " " + KEY_M_PERSON_NIK + " as Person_NIK," +
            " " + KEY_M_PERSON_NAME + " as Person_Name " +
            " FROM " + TABLE_M_PERSON + " UNION ALL " +
            " SELECT -1 as Person_ID, '999999999' as Person_NIK, 'Default' as Person_Name); ";

    private static Cursor XCursor;


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public String Get_DatabaseName(){
        return DATABASE_NAME;
    }
    // ============================== getter table =================================================
    public static String getTableMAsset() {
        return TABLE_M_ASSET;
    }
    public static String getTableMLocation() {
        return TABLE_M_LOCATION;
    }
    public static String getTableMLookup() {
        return TABLE_M_LOOKUP;
    }
    public static String getTableMPerson() {
        return TABLE_M_PERSON;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("[GudangGaram]", "MySQLiteHelper :: onCreate");
        db.execSQL(CREATE_PERSON_TABLE);
        db.execSQL(CREATE_LOOKUP_TABLE);
        db.execSQL(CREATE_LOCATION_TABLE);
        db.execSQL(CREATE_ASSET_TABLE);
        db.execSQL(CREATE_LOGIN_VW);
        db.execSQL(CREATE_INDEX_ASSET);
    }

    public long countRecordTable(String pTableName){
        long result;
        result = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        String kueri = "SELECT count(*) FROM " + pTableName + ";";
        Log.d("[GudangGaram]", "MySQLiteHelper :: countRecordTable :: Query : " + kueri);

        XMICursor = db.rawQuery(kueri, null);
        try {
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    result = XMICursor.getInt(0);
                    XMICursor.moveToNext();
                }
            }
        }
        catch (SQLiteException e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: getCountScannedBarcode :: Exception " + e.getMessage().toString());
            e.printStackTrace();
            result = 0;
        }

        return result;
    }

    // ================================= db operation util =========================================
    public void flushTable(String pTableName){
        Log.d("[GudangGaram]", "MySQLiteHelper :: flushTable");
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(pTableName, null, null);
        db.close();
    }

    public void flush_all(){
        Log.d("[GudangGaram]", "MySQLiteHelper :: flush_all");
        flushTable(TABLE_M_ASSET);
        /*
        TABLE_M_PERSON   = "GGGG_SCANIT_M_PERSON";
        TABLE_M_LOOKUP   = "GGGG_SCANIT_M_LOOKUP";
        TABLE_M_LOCATION = "GGGG_SCANIT_M_LOCATION";
        TABLE_M_ASSET    = "GGGG_SCANIT_M_ASSET";
        */
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("[GudangGaram]", "MySQLiteHelper :: onUpgrade");
        // Drop older books table if existed
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_PERSON);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_ASSET);
        //onCreate(db);
    }

    public Cursor GetCursor() {
        return XCursor;
    }

    public long AddLocation(CLocation oCLocation){
        long savestatus;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddLocation");

        try{ values.put(KEY_M_LOCATION_ID, oCLocation.getLocationID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOCATION_CODE, oCLocation.getLocationName().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOCATION_SEG2, oCLocation.getSegment2().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOCATION_SEG3, oCLocation.getSegment3().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOCATION_SEG4, oCLocation.getSegment4().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOCATION_SEG5, oCLocation.getSegment5().toString()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_LOCATION, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public long AddLookup(CLookup oCLookup){
        long savestatus;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddLookup");

        try{ values.put(KEY_M_LOOKUP_CONTEXT, oCLookup.getLContext().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOOKUP_VALUE, oCLookup.getLValue().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOOKUP_MEANNG, oCLookup.getLMeaning().toString()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_LOOKUP, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public long AddPIC(CPic oCPic){
        long savestatus;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddPIC");

        try{ values.put(KEY_M_PERSON_ID, oCPic.getPersonID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_PERSON_NIK, oCPic.getPersonNIK().toString()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_PERSON_NAME, oCPic.getPersonName().toString()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_PERSON, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public int getAssetIDFromBarcode(String AssetTag){
        Integer AssetID ;
        SQLiteDatabase db = this.getWritableDatabase();
        String kueri = "SELECT Asset_ID FROM " + TABLE_M_ASSET +  " WHERE Asset_BrCode = '" +  AssetTag + "'";

        XMICursor = db.rawQuery(kueri, null);

        AssetID = 0;
        try {
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    AssetID = XMICursor.getInt(0);

                    XMICursor.moveToNext();
                }
            }
        }
        catch (SQLiteException e) {
            e.printStackTrace();
            AssetID = -1;
        }

        return AssetID;
    }

    public int getCountScannedBarcode(String p_assetTag) {
        Integer result;

        result = 0;
            SQLiteDatabase db = this.getWritableDatabase();
            String kueri = "SELECT count(*) " +
                           "FROM " + TABLE_M_ASSET + " " +
                           "WHERE SCAN_DATE IS NOT NULL " +
                            " AND Asset_BrCode = '" + p_assetTag + "'; ";

            Log.d("[GudangGaram]", "MySQLiteHelper :: getCountScannedBarcode :: Query : " + kueri);

            XMICursor = db.rawQuery(kueri, null);
            try {
                if(XMICursor!=null){
                    XMICursor.moveToFirst();
                    while (!XMICursor.isAfterLast())
                    {
                        result = XMICursor.getInt(0);
                        XMICursor.moveToNext();
                    }
                }
            }
            catch (SQLiteException e) {
                Log.d("[GudangGaram]", "MySQLiteHelper :: getCountScannedBarcode :: Exception " + e.getMessage().toString());

                e.printStackTrace();
                result = 0;
            }

        return result;
    }

    public List<CAsset> loadScanAsset(String p_asset_id)
    {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset(" + p_asset_id + ")");

        String kueri = " SELECT ma.Asset_ID, " +
                              " ma.Asset_Book, " +
                              " ma.Asset_BrCode, " +
                              " ma.Asset_Desc, " +
                              " ma.Asset_Cat_ID, " +
                              " ma.Asset_Cat, " +
                              " coalesce(ma.Asset_A1_af,'')  as Asset_A1_af, " +
                              " coalesce(ma.Asset_A2_af,'')  as Asset_A2_af," +
                              " coalesce(ma.Asset_A3_af,'')  as Asset_A3_af," +
                              " coalesce(ma.Asset_A4_af,'')  as Asset_A4_af," +
                              " coalesce(ma.Asset_A5_af,'')  as Asset_A5_af," +
                              " coalesce(ma.Asset_A6_af,'')  as Asset_A6_af," +
                              " coalesce(ma.Asset_A7_af,'')  as Asset_A7_af," +
                              " coalesce(ma.Asset_A8_af,'')  as Asset_A8_af," +
                              " coalesce(ma.Asset_A9_af,'')  as Asset_A9_af," +
                              " coalesce(ma.Asset_A10_af,'') as Asset_A10_af," +
                              " coalesce(ma.Asset_A11_af,'') as Asset_A11_af," +
                              " coalesce(ma.Asset_A12_af,'') as Asset_A12_af, " +
                              " coalesce(ma.Asset_A13_af,'') as Asset_A13_af," +
                              " coalesce(ma.Asset_A14_af,'') as Asset_A14_af, " +
                              " coalesce(ma.Asset_A15_af,'') as Asset_A15_af, " +
                              " coalesce(ma.Asset_A16_af,'') as Asset_A16_af, " +
                              " coalesce(ma.Asset_A17_af,'') as Asset_A17_af, " +
                              " coalesce(ma.Asset_A18_af,'') as Asset_A18_af, " +
                              " coalesce(ma.Asset_A19_af,'') as Asset_A19_af, " +
                              " coalesce(ma.Asset_A20_af,'') as Asset_A20_af, " +
                              " coalesce(ma.Asset_A1_bf,'')  as Asset_A1_bf, " +
                              " coalesce(ma.Asset_A2_bf,'')  as Asset_A2_bf," +
                              " coalesce(ma.Asset_A3_bf,'')  as Asset_A3_bf," +
                              " coalesce(ma.Asset_A4_bf,'')  as Asset_A4_bf," +
                              " coalesce(ma.Asset_A5_bf,'')  as Asset_A5_bf," +
                              " coalesce(ma.Asset_A6_bf,'')  as Asset_A6_bf," +
                              " coalesce(ma.Asset_A7_bf,'')  as Asset_A7_bf," +
                              " coalesce(ma.Asset_A8_bf,'')  as Asset_A8_bf," +
                              " coalesce(ma.Asset_A9_bf,'')  as Asset_A9_bf," +
                              " coalesce(ma.Asset_A10_bf,'') as Asset_A10_bf," +
                              " coalesce(ma.Asset_A11_bf,'') as Asset_A11_bf," +
                              " coalesce(ma.Asset_A12_bf,'') as Asset_A12_bf, " +
                              " coalesce(ma.Asset_A13_bf,'') as Asset_A13_bf," +
                              " coalesce(ma.Asset_A14_bf,'') as Asset_A14_bf, " +
                              " coalesce(ma.Asset_A15_bf,'') as Asset_A15_bf, " +
                              " coalesce(ma.Asset_A16_bf,'') as Asset_A16_bf, " +
                              " coalesce(ma.Asset_A17_bf,'') as Asset_A17_bf, " +
                              " coalesce(ma.Asset_A18_bf,'') as Asset_A18_bf, " +
                              " coalesce(ma.Asset_A19_bf,'') as Asset_A19_bf, " +
                              " coalesce(ma.Asset_A20_bf,'') as Asset_A20_bf, " +
                              " ma.Asset_Loc_ID_af," +
                              " ma.Asset_Loc_ID_bf," +
                              " lo.Segment2," +
                              " lo.Segment3," +
                              " lo.Segment4," +
                              " lo.Segment5, " +
                              " ma.Asset_Qty_af, " +
                              " ma.Asset_Qty_bf, " +
                              " ma.Asset_Assign_NIK_af, " +
                              " ma.Asset_Assign_NIK_bf, " +
                              " ma.Asset_Assign_Name, " +
                              " coalesce(ma.Download_Date,'') Download_Date, " +
                              " coalesce(ma.Scan_Date,'') Scan_Date, " +
                              " coalesce(ma.Scan_By,'') Scan_Date " +
                " FROM GGGG_SCANIT_M_ASSET ma left join " +
                " GGGG_SCANIT_M_LOCATION lo ON (ma.Asset_Loc_ID_af = lo.Location_ID) " +
                " WHERE 1 = 1 " +
                " AND ma.Asset_ID = " + p_asset_id + " " +
                " ORDER BY ma.Asset_ID; ";

        Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset Query : " +  kueri);

        List<CAsset> lvi  = new ArrayList<CAsset>();
        SQLiteDatabase db =  this.getReadableDatabase();

        ctr = 1;
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset Query count : " +  XMICursor.getCount());
            lvi.clear();
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset : XMICursor [" +  ctr + "]");

                    CAsset o = new CAsset();
                    o.setAsset_ID(Integer.toString(XMICursor.getInt(0)));
                    o.setAsset_Book(XMICursor.getString(1));
                    o.setAsset_BrCode(XMICursor.getString(2));
                    o.setAsset_Desc(XMICursor.getString(3));
                    o.setAsset_Cat_ID(XMICursor.getInt(4));
                    o.setAsset_Cat(XMICursor.getString(5));
                    o.setA1_af(XMICursor.getString(6));
                    o.setA2_af(XMICursor.getString(7));
                    o.setA3_af(XMICursor.getString(8));
                    o.setA4_af(XMICursor.getString(9));
                    o.setA5_af(XMICursor.getString(10));
                    o.setA6_af(XMICursor.getString(11));
                    o.setA7_af(XMICursor.getString(12));
                    o.setA8_af(XMICursor.getString(13));
                    o.setA9_af(XMICursor.getString(14));
                    o.setA10_af(XMICursor.getString(15));
                    o.setA11_af(XMICursor.getString(16));
                    o.setA12_af(XMICursor.getString(17));
                    o.setA13_af(XMICursor.getString(18));
                    o.setA14_af(XMICursor.getString(19));
                    o.setA15_af(XMICursor.getString(20));
                    o.setA16_af(XMICursor.getString(21));
                    o.setA17_af(XMICursor.getString(22));
                    o.setA18_af(XMICursor.getString(23));
                    o.setA19_af(XMICursor.getString(24));
                    o.setA20_af(XMICursor.getString(25));
                    o.setA1_bf(XMICursor.getString(26));
                    o.setA2_bf(XMICursor.getString(27));
                    o.setA3_bf(XMICursor.getString(28));
                    o.setA4_bf(XMICursor.getString(29));
                    o.setA5_bf(XMICursor.getString(30));
                    o.setA6_bf(XMICursor.getString(31));
                    o.setA7_bf(XMICursor.getString(32));
                    o.setA8_bf(XMICursor.getString(33));
                    o.setA9_bf(XMICursor.getString(34));
                    o.setA10_bf(XMICursor.getString(35));
                    o.setA11_bf(XMICursor.getString(36));
                    o.setA12_bf(XMICursor.getString(37));
                    o.setA13_bf(XMICursor.getString(38));
                    o.setA14_bf(XMICursor.getString(39));
                    o.setA15_bf(XMICursor.getString(40));
                    o.setA16_bf(XMICursor.getString(41));
                    o.setA17_bf(XMICursor.getString(42));
                    o.setA18_bf(XMICursor.getString(43));
                    o.setA19_bf(XMICursor.getString(44));
                    o.setA20_bf(XMICursor.getString(45));
                    o.setAsset_Loc_ID_af(XMICursor.getInt(46));
                    o.setAsset_Loc_ID_bf(XMICursor.getInt(47));
                    o.setLoc_Seg2(XMICursor.getString(48));
                    o.setLoc_Seg3(XMICursor.getString(49));
                    o.setLoc_Seg4(XMICursor.getString(50));
                    o.setLoc_Seg5(XMICursor.getString(51));
                    o.setAsset_Qty_af(XMICursor.getInt(52));
                    o.setAsset_Qty_bf(XMICursor.getInt(53));
                    o.setAsset_Assign_NIK_af(XMICursor.getString(54));
                    o.setAsset_Assign_NIK_bf(XMICursor.getString(55));
                    o.setAsset_Assign_Name(XMICursor.getString(56));
                    o.setDOWNLOAD_DATE(XMICursor.getString(57));
                    o.setSCAN_DATE(XMICursor.getString(58));
                    o.setSCAN_BY(XMICursor.getString(59));

                    lvi.add(o);
                    XMICursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset Exception : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
            //db.close();
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadScanAsset : lvi size = " +  lvi.size());
        }

        return lvi;
    }


    public List<CAsset> getListScannedAsset(String p_nik)
    {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getListScannedAsset");
        List<CAsset> lvi = new ArrayList<CAsset>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = "";
        String Base_Query = " SELECT ma.Asset_ID," +
                              " ma.Asset_Book, " +
                              " ma.Asset_BrCode, " +
                              " ma.Asset_Desc, " +
                              " ma.Asset_Cat, " +
                              " ma.Asset_Qty_Bf, " +
                              " ma.Asset_Qty_Af, " +
                              " coalesce(lo.Segment2, '') Segment2, " +
                              " coalesce(lo.Segment3, '') Segment3, " +
                              " coalesce(lo.Segment4, '') Segment4, " +
                              " coalesce(lo.Segment5, '') Segment5, " +
                              " ma.Download_Date, " +
                              " ma.Scan_Date " +
                       " FROM GGGG_SCANIT_M_ASSET ma left join " +
                       " GGGG_SCANIT_M_LOCATION lo on (ma.Asset_Loc_ID_af = lo.Location_ID) ";

            kueri = Base_Query + " WHERE 1 = 1 ";
            kueri = kueri + "   AND ma.Scan_Date IS NOT NULL ";
            kueri = kueri + "   AND ma.Scan_By IS NOT NULL " +
                            "   AND ma.Scan_By = '" +  p_nik + "' " +
                            " ORDER BY ma.Scan_Date DESC; ";

        Log.d("[GudangGaram]", "getListScannedAsset Query : " +  kueri);

        ctr = 0;
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListScannedAsset count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getListScannedAsset : " +  ctr);

                    CAsset o = new CAsset();
                    o.setAsset_ID(XMICursor.getString(0));
                    o.setAsset_Book(XMICursor.getString(1));
                    o.setAsset_BrCode(XMICursor.getString(2));
                    o.setAsset_Desc(XMICursor.getString(3));
                    o.setAsset_Cat(XMICursor.getString(4));
                    o.setAsset_Qty_bf(XMICursor.getInt(5));
                    o.setAsset_Qty_af(XMICursor.getInt(6));
                    o.setAsset_Assign_NIK_af("");
                    o.setLoc_Seg2(XMICursor.getString(7));
                    o.setLoc_Seg3(XMICursor.getString(8));
                    o.setLoc_Seg4(XMICursor.getString(9));
                    o.setLoc_Seg5(XMICursor.getString(10));
                    o.setDOWNLOAD_DATE(XMICursor.getString(11));
                    o.setSCAN_DATE(XMICursor.getString(12));
                    lvi.add(o);
                    XMICursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListScannedAsset : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }

    public List<CAsset> getListDownloadAsset(String pStrFilter)
    {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getListDownloadAsset");
        List<CAsset> lvi = new ArrayList<CAsset>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = "";
        String Base_Query = " SELECT ma.Asset_ID," +
                " ma.Asset_Book, " +
                " ma.Asset_BrCode, " +
                " ma.Asset_Desc, " +
                " ma.Asset_Cat, " +
                " ma.Asset_Qty_Bf, " +
                " ma.Asset_Qty_Af, " +
                " coalesce(lo.Segment2, '') Segment2, " +
                " coalesce(lo.Segment3, '') Segment3, " +
                " coalesce(lo.Segment4, '') Segment4, " +
                " coalesce(lo.Segment5, '') Segment5, " +
                " ma.Download_Date, " +
                " ma.Scan_Date " +
                " FROM GGGG_SCANIT_M_ASSET ma left join " +
                " GGGG_SCANIT_M_LOCATION lo on (ma.Asset_Loc_ID_af = lo.Location_ID) ";

        kueri = Base_Query + " WHERE 1 = 1 ";
        kueri = kueri + " AND ma.Download_Date IS NOT NULL ";

        if(pStrFilter.length() > 0){
            kueri = kueri + pStrFilter + " ";
        }
        kueri = kueri + " ORDER BY ma.Asset_ID ";

        Log.d("[GudangGaram]", "getListDownloadAsset Query : " +  kueri);

        ctr = 0;
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListDownloadAsset count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getListDownloadAsset : " +  ctr);

                    CAsset o = new CAsset();
                    o.setAsset_ID(XMICursor.getString(0));
                    o.setAsset_Book(XMICursor.getString(1));
                    o.setAsset_BrCode(XMICursor.getString(2));
                    o.setAsset_Desc(XMICursor.getString(3));
                    o.setAsset_Cat(XMICursor.getString(4));
                    o.setAsset_Qty_bf(XMICursor.getInt(5));
                    o.setAsset_Qty_af(XMICursor.getInt(6));
                    o.setAsset_Assign_NIK_af("");
                    o.setLoc_Seg2(XMICursor.getString(7));
                    o.setLoc_Seg3(XMICursor.getString(8));
                    o.setLoc_Seg4(XMICursor.getString(9));
                    o.setLoc_Seg5(XMICursor.getString(10));
                    o.setDOWNLOAD_DATE(XMICursor.getString(11));
                    o.setSCAN_DATE(XMICursor.getString(12));
                    lvi.add(o);
                    XMICursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListDownloadAsset : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }

    public long SaveEditAsset(CAsset oCAsset){
        long    savestatus;
        Integer pc;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: AssetID = " + oCAsset.getAsset_ID().toString());

        savestatus = 0;
        pc = 0;

        if(oCAsset.getAsset_ID().toString().length() > 0){
            String pWehere = KEY_M_ASSET_ID + " = " + oCAsset.getAsset_ID().toString() + " ";
            String StrCat = oCAsset.getAsset_Cat().toString();

            // ------ parameter input : qty ---------------------------------
            if(oCAsset.getAsset_Qty_af().toString().length() > 0){
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_QTY_AF + " : " + oCAsset.getAsset_Qty_af().toString());
                args.put(KEY_M_ASSET_QTY_AF, oCAsset.getAsset_Qty_af().toString());
                pc +=1;
            }

            // ------ parameter input : location_id ------------------------
            if(oCAsset.getAsset_Loc_ID_af().toString().length() > 0){
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_LOC_ID_AF + " : " + oCAsset.getAsset_Loc_ID_af().toString());
                args.put(KEY_M_ASSET_LOC_ID_AF, oCAsset.getAsset_Loc_ID_af().toString());
                pc +=1;
            }

            // ------ parameter input : assign nik ------------------------
            if(oCAsset.getAsset_Assign_NIK_af().toString().length() >= 0){
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_ASSIGN_NIK_AF + " : " + oCAsset.getAsset_Assign_NIK_af().toString());
                args.put(KEY_M_ASSET_ASSIGN_NIK_AF, oCAsset.getAsset_Assign_NIK_af().toString());
                //args.put(KEY_M_ASSET_ASSIGN_NAME, );
                pc +=1;
            }

            // ---------------------------------------- attribute ----------------------------------------
            if (StrCat.equals("INV")) {
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: INV");
                if(oCAsset.getA1_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A1_AF + " : " + oCAsset.getA1_af().toString());
                    args.put(KEY_M_ASSET_A1_AF, oCAsset.getA1_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA2_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A2_AF + " : " + oCAsset.getA2_af().toString());
                    args.put(KEY_M_ASSET_A2_AF, oCAsset.getA2_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA3_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A3_AF + " : " + oCAsset.getA3_af().toString());
                    args.put(KEY_M_ASSET_A3_AF, oCAsset.getA3_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA4_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A4_AF + " : " + oCAsset.getA4_af().toString());
                    args.put(KEY_M_ASSET_A4_AF, oCAsset.getA4_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA8_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A8_AF + " : " + oCAsset.getA8_af().toString());
                    args.put(KEY_M_ASSET_A8_AF, oCAsset.getA8_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA10_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A10_AF + " : " + oCAsset.getA10_af().toString());
                    args.put(KEY_M_ASSET_A10_AF, oCAsset.getA10_af().toString());
                    pc +=1;
                }
            }
            else if (StrCat.equals("BGN")) {
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: BGN");
                if(oCAsset.getA1_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A1_AF + " : " + oCAsset.getA1_af().toString());
                    args.put(KEY_M_ASSET_A1_AF, oCAsset.getA1_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA2_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A2_AF + " : " + oCAsset.getA2_af().toString());
                    args.put(KEY_M_ASSET_A2_AF, oCAsset.getA2_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA3_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A3_AF + " : " + oCAsset.getA3_af().toString());
                    args.put(KEY_M_ASSET_A3_AF, oCAsset.getA3_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA4_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A4_AF + " : " + oCAsset.getA4_af().toString());
                    args.put(KEY_M_ASSET_A4_AF, oCAsset.getA4_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA5_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A5_AF + " : " + oCAsset.getA5_af().toString());
                    args.put(KEY_M_ASSET_A5_AF, oCAsset.getA5_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA6_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A6_AF + " : " + oCAsset.getA6_af().toString());
                    args.put(KEY_M_ASSET_A6_AF, oCAsset.getA6_af().toString());
                    pc +=1;
                }
            }
            else if (StrCat.equals("KDN")) {
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: KDN");
                if(oCAsset.getA1_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A1_AF + " : " + oCAsset.getA1_af().toString());
                    args.put(KEY_M_ASSET_A1_AF, oCAsset.getA1_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA2_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A2_AF + " : " + oCAsset.getA2_af().toString());
                    args.put(KEY_M_ASSET_A2_AF, oCAsset.getA2_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA3_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A3_AF + " : " + oCAsset.getA3_af().toString());
                    args.put(KEY_M_ASSET_A3_AF, oCAsset.getA3_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA4_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A4_AF + " : " + oCAsset.getA4_af().toString());
                    args.put(KEY_M_ASSET_A4_AF, oCAsset.getA4_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA5_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A5_AF + " : " + oCAsset.getA5_af().toString());
                    args.put(KEY_M_ASSET_A5_AF, oCAsset.getA5_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA6_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A6_AF + " : " + oCAsset.getA6_af().toString());
                    args.put(KEY_M_ASSET_A6_AF, oCAsset.getA6_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA7_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A7_AF + " : " + oCAsset.getA7_af().toString());
                    args.put(KEY_M_ASSET_A7_AF, oCAsset.getA7_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA8_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A8_AF + " : " + oCAsset.getA8_af().toString());
                    args.put(KEY_M_ASSET_A8_AF, oCAsset.getA8_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA9_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A9_AF + " : " + oCAsset.getA9_af().toString());
                    args.put(KEY_M_ASSET_A9_AF, oCAsset.getA9_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA10_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A10_AF + " : " + oCAsset.getA10_af().toString());
                    args.put(KEY_M_ASSET_A10_AF, oCAsset.getA10_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA11_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A11_AF + " : " + oCAsset.getA11_af().toString());
                    args.put(KEY_M_ASSET_A11_AF, oCAsset.getA11_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA12_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A12_AF + " : " + oCAsset.getA12_af().toString());
                    args.put(KEY_M_ASSET_A12_AF, oCAsset.getA12_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA14_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A14_AF + " : " + oCAsset.getA14_af().toString());
                    args.put(KEY_M_ASSET_A14_AF, oCAsset.getA14_af().toString());
                    pc +=1;
                }
            }
            else{
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: OTH");
                if(oCAsset.getA1_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A1_AF + " : " + oCAsset.getA1_af().toString());
                    args.put(KEY_M_ASSET_A1_AF, oCAsset.getA1_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA2_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A2_AF + " : " + oCAsset.getA2_af().toString());
                    args.put(KEY_M_ASSET_A2_AF, oCAsset.getA2_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA3_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A3_AF + " : " + oCAsset.getA3_af().toString());
                    args.put(KEY_M_ASSET_A3_AF, oCAsset.getA3_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA4_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A4_AF + " : " + oCAsset.getA4_af().toString());
                    args.put(KEY_M_ASSET_A4_AF, oCAsset.getA4_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA5_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A5_AF + " : " + oCAsset.getA5_af().toString());
                    args.put(KEY_M_ASSET_A5_AF, oCAsset.getA5_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA6_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A6_AF + " : " + oCAsset.getA6_af().toString());
                    args.put(KEY_M_ASSET_A6_AF, oCAsset.getA6_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA7_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A7_AF + " : " + oCAsset.getA7_af().toString());
                    args.put(KEY_M_ASSET_A7_AF, oCAsset.getA7_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA8_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A8_AF + " : " + oCAsset.getA8_af().toString());
                    args.put(KEY_M_ASSET_A8_AF, oCAsset.getA8_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA9_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A9_AF + " : " + oCAsset.getA9_af().toString());
                    args.put(KEY_M_ASSET_A9_AF, oCAsset.getA9_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA10_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A10_AF + " : " + oCAsset.getA10_af().toString());
                    args.put(KEY_M_ASSET_A10_AF, oCAsset.getA10_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA11_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A11_AF + " : " + oCAsset.getA11_af().toString());
                    args.put(KEY_M_ASSET_A11_AF, oCAsset.getA11_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA12_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A12_AF + " : " + oCAsset.getA12_af().toString());
                    args.put(KEY_M_ASSET_A12_AF, oCAsset.getA12_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA13_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A13_AF + " : " + oCAsset.getA13_af().toString());
                    args.put(KEY_M_ASSET_A13_AF, oCAsset.getA13_af().toString());
                    pc +=1;
                }
                if(oCAsset.getA14_af().toString().length() > 0){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: " + KEY_M_ASSET_A14_AF + " : " + oCAsset.getA14_af().toString());
                    args.put(KEY_M_ASSET_A14_AF, oCAsset.getA14_af().toString());
                    pc +=1;
                }
            }

            // ---- fill scan info  --------
            if(pc > 0){
                args.put(KEY_M_ASSET_SCAN_DATE, sysdate);
                args.put(KEY_M_ASSET_SCAN_BY, oCAsset.getSCAN_BY().toString());
            }

            // ----- update to database table -------------
            try{
                db.update(TABLE_M_ASSET,args,pWehere, null);
                savestatus = 1;
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "MySQLiteHelper :: SaveEditAsset :: Add Exception :" + e.getMessage().toString());
                savestatus = 0;
            }
        }

        return savestatus;
    }

    public void ResetAssetAll(String p_nik){
        Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAssetAll");

        String kueri;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        //String pWehere = KEY_M_ASSET_SCAN_BY + " = '" + p_nik + "' ";
        /*
        // set null to asset scan date and scan by to null
        try{
            args.putNull(KEY_M_ASSET_SCAN_DATE);
            args.putNull(KEY_M_ASSET_SCAN_BY);
            db.update(TABLE_M_ASSET,args,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAssetAll Exception :" + e.getMessage().toString());
        }
        */

        kueri = "UPDATE GGGG_SCANIT_M_ASSET " +
                "SET Scan_Date           = null," +
                    "Scan_By             = null," +
                    "Asset_Loc_ID_af     = Asset_Loc_ID_bf," +
                    "Asset_Assign_NIK_af = Asset_Assign_NIK_bf," +
                    "Asset_Qty_Af        = Asset_Qty_Bf," +
                    "Asset_A1_af         = Asset_A1_bf," +
                    "Asset_A2_af         = Asset_A2_bf," +
                    "Asset_A3_af         = Asset_A3_bf," +
                    "Asset_A4_af         = Asset_A4_bf," +
                    "Asset_A5_af         = Asset_A5_bf," +
                    "Asset_A6_af         = Asset_A6_bf," +
                    "Asset_A7_af         = Asset_A7_bf," +
                    "Asset_A8_af         = Asset_A8_bf," +
                    "Asset_A9_af         = Asset_A9_bf," +
                    "Asset_A10_af        = Asset_A10_bf," +
                    "Asset_A11_af        = Asset_A11_bf," +
                    "Asset_A12_af        = Asset_A12_bf," +
                    "Asset_A13_af        = Asset_A13_bf," +
                    "Asset_A14_af        = Asset_A14_bf," +
                    "Asset_A15_af        = Asset_A15_bf," +
                    "Asset_A16_af        = Asset_A16_bf," +
                    "Asset_A17_af        = Asset_A17_bf," +
                    "Asset_A18_af        = Asset_A18_bf," +
                    "Asset_A19_af        = Asset_A19_bf," +
                    "Asset_A20_af        = Asset_A20_bf " +
                "WHERE " + KEY_M_ASSET_SCAN_BY + " = '" + p_nik + "' ";

        try{
            db.execSQL(kueri);
        }
        catch(Exception e){
             Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAssetAll Exception :" + e.getMessage().toString());
        }
    }

    public void ResetAsset(String p_asset_id){
        Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAsset");
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        String kueri;
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        /*
        String pWehere = KEY_M_ASSET_ID + " = " + p_asset_id + " ";
        // set null to asset scan date and scan by to null
        try{
            args.putNull(KEY_M_ASSET_SCAN_DATE);
            args.putNull(KEY_M_ASSET_SCAN_BY);
            db.update(TABLE_M_ASSET,args,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAsset Exception :" + e.getMessage().toString());
        }
        */

        kueri = "UPDATE GGGG_SCANIT_M_ASSET " +
                "SET Scan_Date       = null," +
                "Scan_By             = null," +
                "Asset_Loc_ID_af     = Asset_Loc_ID_bf," +
                "Asset_Assign_NIK_af = Asset_Assign_NIK_bf," +
                "Asset_Qty_Af        = Asset_Qty_Bf," +
                "Asset_A1_af         = Asset_A1_bf," +
                "Asset_A2_af         = Asset_A2_bf," +
                "Asset_A3_af         = Asset_A3_bf," +
                "Asset_A4_af         = Asset_A4_bf," +
                "Asset_A5_af         = Asset_A5_bf," +
                "Asset_A6_af         = Asset_A6_bf," +
                "Asset_A7_af         = Asset_A7_bf," +
                "Asset_A8_af         = Asset_A8_bf," +
                "Asset_A9_af         = Asset_A9_bf," +
                "Asset_A10_af        = Asset_A10_bf," +
                "Asset_A11_af        = Asset_A11_bf," +
                "Asset_A12_af        = Asset_A12_bf," +
                "Asset_A13_af        = Asset_A13_bf," +
                "Asset_A14_af        = Asset_A14_bf," +
                "Asset_A15_af        = Asset_A15_bf," +
                "Asset_A16_af        = Asset_A16_bf," +
                "Asset_A17_af        = Asset_A17_bf," +
                "Asset_A18_af        = Asset_A18_bf," +
                "Asset_A19_af        = Asset_A19_bf," +
                "Asset_A20_af        = Asset_A20_bf " +
               " WHERE " + KEY_M_ASSET_ID + " = " + p_asset_id + " ";

        try{
            db.execSQL(kueri);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: ResetAsset Exception :" + e.getMessage().toString());
        }
    }

    public long AddBulkAsset(List<CAsset> OLCAsset){
        long savestatus;

        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String sql = "INSERT INTO "+ TABLE_M_ASSET + "(" +
                    KEY_M_ASSET_ID + "," +
                    KEY_M_ASSET_BOOK + "," +
                    KEY_M_ASSET_BRCODE + "," +
                    KEY_M_ASSET_DESC + "," +
                    KEY_M_ASSET_CAT_ID + "," +
                    KEY_M_ASSET_CAT + "," +
                    KEY_M_ASSET_LOC_ID_AF + "," +
                    KEY_M_ASSET_LOC_ID_BF + "," +
                    KEY_M_ASSET_ASSIGN_ID + "," +
                    KEY_M_ASSET_ASSIGN_NAME + "," +
                    KEY_M_ASSET_ASSIGN_NIK_AF + "," +
                    KEY_M_ASSET_ASSIGN_NIK_BF + "," +
                    KEY_M_ASSET_QTY_AF + "," +
                    KEY_M_ASSET_QTY_BF + "," +
                    KEY_M_ASSET_DOWNLOAD_DATE + "," +
                    KEY_M_ASSET_A1_AF  + "," +
                    KEY_M_ASSET_A1_BF  + "," +
                    KEY_M_ASSET_A2_AF  + "," +
                    KEY_M_ASSET_A2_BF  + "," +
                    KEY_M_ASSET_A3_AF  + "," +
                    KEY_M_ASSET_A3_BF  + "," +
                    KEY_M_ASSET_A4_AF  + "," +
                    KEY_M_ASSET_A4_BF  + "," +
                    KEY_M_ASSET_A5_AF  + "," +
                    KEY_M_ASSET_A5_BF  + "," +
                    KEY_M_ASSET_A6_AF  + "," +
                    KEY_M_ASSET_A6_BF  + "," +
                    KEY_M_ASSET_A7_AF  + "," +
                    KEY_M_ASSET_A7_BF  + "," +
                    KEY_M_ASSET_A8_AF  + "," +
                    KEY_M_ASSET_A8_BF  + "," +
                    KEY_M_ASSET_A9_AF  + "," +
                    KEY_M_ASSET_A9_BF  + "," +
                    KEY_M_ASSET_A10_AF + "," +
                    KEY_M_ASSET_A10_BF + "," +
                    KEY_M_ASSET_A11_AF + "," +
                    KEY_M_ASSET_A11_BF + "," +
                    KEY_M_ASSET_A12_AF + "," +
                    KEY_M_ASSET_A12_BF + "," +
                    KEY_M_ASSET_A13_AF + "," +
                    KEY_M_ASSET_A13_BF + "," +
                    KEY_M_ASSET_A14_AF + "," +
                    KEY_M_ASSET_A14_BF + "," +
                    KEY_M_ASSET_A20_AF + "," +
                    KEY_M_ASSET_A20_BF + "" +
                ")" +
                " VALUES (" +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?," +
                    "?" +
                ");";


        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkAsset " + OLCAsset.size());

        SQLiteStatement ist = db.compileStatement(sql);
        db.beginTransaction();
        try{
            if(OLCAsset.size() > 0){
                for (int i = 0; i< OLCAsset.size(); i++) {
                    ist.clearBindings();

                    ist.bindString(1,OLCAsset.get(i).getAsset_ID());
                    ist.bindString(2,OLCAsset.get(i).getAsset_Book().toString());
                    ist.bindString(3,OLCAsset.get(i).getAsset_BrCode().toString());
                    ist.bindString(4,OLCAsset.get(i).getAsset_Desc().toString());
                    ist.bindLong(5,OLCAsset.get(i).getAsset_Cat_ID());
                    ist.bindString(6,OLCAsset.get(i).getAsset_Cat().toString());
                    ist.bindLong(7,OLCAsset.get(i).getAsset_Loc_ID_af());
                    ist.bindLong(8,OLCAsset.get(i).getAsset_Loc_ID_bf());
                    ist.bindLong(9,OLCAsset.get(i).getAsset_Assign_ID());
                    ist.bindString(10,OLCAsset.get(i).getAsset_Assign_Name());
                    ist.bindString(11,OLCAsset.get(i).getAsset_Assign_NIK_af());
                    ist.bindString(12,OLCAsset.get(i).getAsset_Assign_NIK_bf());
                    ist.bindDouble(13,OLCAsset.get(i).getAsset_Qty_af());
                    ist.bindDouble(14,OLCAsset.get(i).getAsset_Qty_bf());
                    ist.bindString(15,sysdate);
                    try{ ist.bindString(16,OLCAsset.get(i).getA1_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(16,null); }
                    try{ ist.bindString(17,OLCAsset.get(i).getA1_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(17,null); }
                    try{ ist.bindString(18,OLCAsset.get(i).getA2_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(18,null); }
                    try{ ist.bindString(19,OLCAsset.get(i).getA2_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(19,null); }
                    try{ ist.bindString(20,OLCAsset.get(i).getA3_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(20,null); }
                    try{ ist.bindString(21,OLCAsset.get(i).getA3_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(21,null); }
                    try{ ist.bindString(22,OLCAsset.get(i).getA4_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(22,null); }
                    try{ ist.bindString(23,OLCAsset.get(i).getA4_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(23,null); }
                    try{ ist.bindString(24,OLCAsset.get(i).getA5_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(24,null); }
                    try{ ist.bindString(25,OLCAsset.get(i).getA5_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(25,null); }
                    try{ ist.bindString(26,OLCAsset.get(i).getA6_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(26,null); }
                    try{ ist.bindString(27,OLCAsset.get(i).getA6_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(27,null); }
                    try{ ist.bindString(28,OLCAsset.get(i).getA7_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(28,null); }
                    try{ ist.bindString(29,OLCAsset.get(i).getA7_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(29,null); }
                    try{ ist.bindString(30,OLCAsset.get(i).getA8_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(30,null); }
                    try{ ist.bindString(31,OLCAsset.get(i).getA8_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(31,null); }
                    try{ ist.bindString(32,OLCAsset.get(i).getA9_af());  }  catch(java.lang.NullPointerException exception){  ist.bindString(32,null); }
                    try{ ist.bindString(33,OLCAsset.get(i).getA9_bf());  }  catch(java.lang.NullPointerException exception){  ist.bindString(33,null); }
                    try{ ist.bindString(34,OLCAsset.get(i).getA10_af()); }  catch(java.lang.NullPointerException exception){  ist.bindString(34,null); }
                    try{ ist.bindString(35,OLCAsset.get(i).getA10_bf()); }  catch(java.lang.NullPointerException exception){  ist.bindString(35,null); }
                    try{ ist.bindString(36,OLCAsset.get(i).getA11_af()); }  catch(java.lang.NullPointerException exception){  ist.bindString(36,null); }
                    try{ ist.bindString(37,OLCAsset.get(i).getA11_bf()); }  catch(java.lang.NullPointerException exception){  ist.bindString(37,null); }
                    try{ ist.bindString(38,OLCAsset.get(i).getA12_af()); }  catch(java.lang.NullPointerException exception){  ist.bindString(38,null); }
                    try{ ist.bindString(39,OLCAsset.get(i).getA12_bf()); }  catch(java.lang.NullPointerException exception){  ist.bindString(39,null); }
                    try{ ist.bindString(40,OLCAsset.get(i).getA13_af()); }  catch(java.lang.NullPointerException exception){  ist.bindString(40,null); }
                    try{ ist.bindString(41,OLCAsset.get(i).getA13_bf()); }  catch(java.lang.NullPointerException exception){  ist.bindString(41,null); }
                    try{ ist.bindString(42,OLCAsset.get(i).getA14_af()); }  catch(java.lang.NullPointerException exception){  ist.bindString(42,null); }
                    try{ ist.bindString(43,OLCAsset.get(i).getA14_bf()); }  catch(java.lang.NullPointerException exception){  ist.bindString(43,null); }

                    ist.execute();
                }
            }
            savestatus = 1;
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "Add Exception :" + e.getMessage().toString());
            savestatus = 0;
        }
        db.setTransactionSuccessful();
        db.endTransaction();


        return savestatus;
    }

    public long AddAsset(CAsset oCAsset){
        long savestatus;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddAsset");

        try{
            try{ values.put(KEY_M_ASSET_ID, oCAsset.getAsset_ID().toString());}
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_BOOK, oCAsset.getAsset_Book().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_BRCODE, oCAsset.getAsset_BrCode().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_DESC, oCAsset.getAsset_Desc().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_CAT_ID, oCAsset.getAsset_Cat_ID()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_CAT, oCAsset.getAsset_Cat().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_LOC_ID_AF, oCAsset.getAsset_Loc_ID_af()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_LOC_ID_BF, oCAsset.getAsset_Loc_ID_bf()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_ASSIGN_ID, oCAsset.getAsset_Assign_ID()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_ASSIGN_NAME, oCAsset.getAsset_Assign_Name().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_ASSIGN_NIK_AF, oCAsset.getAsset_Assign_NIK_af().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_ASSIGN_NIK_BF, oCAsset.getAsset_Assign_NIK_bf().toString()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_QTY_AF, oCAsset.getAsset_Qty_af()); }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_QTY_BF, oCAsset.getAsset_Qty_bf()); }
            catch(java.lang.NullPointerException exception){ }
            try {
                if(oCAsset.getA1_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A1_AF, oCAsset.getA1_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA1_bf().toString().contains("|") == false) {
                    values.put(KEY_M_ASSET_A1_BF, oCAsset.getA1_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if( oCAsset.getA2_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A2_AF, oCAsset.getA2_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try {
                if (oCAsset.getA2_bf().toString().contains("|") == false) {
                    values.put(KEY_M_ASSET_A2_BF, oCAsset.getA2_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA3_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A3_AF, oCAsset.getA3_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA3_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A3_BF, oCAsset.getA3_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA4_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A4_AF, oCAsset.getA4_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA4_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A4_BF, oCAsset.getA4_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA5_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A5_AF, oCAsset.getA5_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if( oCAsset.getA5_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A5_BF, oCAsset.getA5_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA6_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A6_AF, oCAsset.getA6_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA6_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A6_BF, oCAsset.getA6_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA7_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A7_AF, oCAsset.getA7_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA7_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A7_BF, oCAsset.getA7_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if( oCAsset.getA8_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A8_AF, oCAsset.getA8_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA8_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A8_BF, oCAsset.getA8_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try {
                if (oCAsset.getA9_af().toString().contains("|") == false) {
                    values.put(KEY_M_ASSET_A9_AF, oCAsset.getA9_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA9_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A9_BF, oCAsset.getA9_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA10_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A10_AF, oCAsset.getA10_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA10_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A10_BF, oCAsset.getA10_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA11_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A11_AF, oCAsset.getA11_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA11_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A11_BF, oCAsset.getA11_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA12_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A12_AF, oCAsset.getA12_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA12_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A12_BF, oCAsset.getA12_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA13_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A13_AF, oCAsset.getA13_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA13_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A13_BF, oCAsset.getA13_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA14_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A14_AF, oCAsset.getA14_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA14_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A14_BF, oCAsset.getA14_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA15_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A15_AF, oCAsset.getA15_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA15_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A15_BF, oCAsset.getA15_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA16_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A16_AF, oCAsset.getA16_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA16_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A16_BF, oCAsset.getA16_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA17_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A17_AF, oCAsset.getA17_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA17_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A17_BF, oCAsset.getA17_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA18_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A18_AF, oCAsset.getA18_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA18_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A18_BF, oCAsset.getA18_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA19_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A19_AF, oCAsset.getA19_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try {
                if(oCAsset.getA19_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A19_BF, oCAsset.getA19_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try {
                if(oCAsset.getA20_af().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A20_AF, oCAsset.getA20_af().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{
                if(oCAsset.getA20_bf().toString().contains("|") == false){
                    values.put(KEY_M_ASSET_A20_BF, oCAsset.getA20_bf().toString());
                }
            }
            catch(java.lang.NullPointerException exception){ }
            try{ values.put(KEY_M_ASSET_DOWNLOAD_DATE, sysdate); }
            catch(java.lang.NullPointerException exception){ }

            savestatus = db.insert(TABLE_M_ASSET, null, values); // key/value -> keys = column names/ values = column values
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "Add Exception :" + e.getMessage().toString());
            savestatus = 0;
        }

        return savestatus;
    }

    public List<StringWithTag> getPicList() {
        List<StringWithTag> lvi = new ArrayList<StringWithTag>();

        SQLiteDatabase db = this.getReadableDatabase();
        String kueri = " SELECT " + KEY_M_PERSON_NIK + "," +  KEY_M_PERSON_NAME +
                       " FROM " + VIEW_LOGIN +
                       " ORDER BY " + KEY_M_PERSON_NIK + ";";

        XMICursor = db.rawQuery(kueri, null);
        try {
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    String key = XMICursor.getString(1);
                    String value = XMICursor.getString(0);
                    Log.d("[GudangGaram]", "KVP Load : (Key,Value) -> " + key + "," + value);
                    lvi.add(new StringWithTag(value, key));
                    XMICursor.moveToNext();
                }
            }
        } catch (Exception e) {
        } finally {
            XMICursor.close();
            //db.close();
        }
        return lvi;
    }

    public List<String> LoadCboKota() {
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " select LValue " +
                       " from GGGG_SCANIT_M_LOOKUP " +
                       " where LContext = 'GGGG_FA_LOC_KOTA' " +
                       " order by LValue ";

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    lvi.add(XMICursor.getString(0));
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }

    public List<String> LoadCboLokasi() {
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " select LValue " +
                " from GGGG_SCANIT_M_LOOKUP " +
                " where LContext = 'GGGG_FA_LOC_LOKASI' " +
                " order by LValue ";

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    lvi.add(XMICursor.getString(0));
                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }

    public List<CLocation> LoadCboGLokasi() {
        List<CLocation> lvi = new ArrayList<CLocation>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " select Location_ID, Location_Code, Segment1, Segment2, Segment3, Segment4, Segment5 " +
                        "from GGGG_SCANIT_M_LOCATION " +
                        "order by segment2, segment3, segment4, segment5 ";

        XMICursor = db.rawQuery(kueri, null);
        try{
            if(XMICursor!=null){
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast())
                {
                    CLocation oc = new CLocation();
                    oc.setLocationID(XMICursor.getInt(0));
                    oc.setLocationName(XMICursor.getString(1));
                    oc.setSegment1(XMICursor.getString(2));
                    oc.setSegment2(XMICursor.getString(3));
                    oc.setSegment3(XMICursor.getString(4));
                    oc.setSegment4(XMICursor.getString(5));
                    oc.setSegment5(XMICursor.getString(6));

                    lvi.add(oc);

                    XMICursor.moveToNext();
                }
            }
        }
        catch(Exception e){
        }
        finally {
            XMICursor.close();
            //db.close();
        }

        return lvi;
    }



}
