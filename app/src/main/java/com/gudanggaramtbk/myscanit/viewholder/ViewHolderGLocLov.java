package com.gudanggaramtbk.myscanit.viewholder;

import android.widget.TextView;

/**
 * Created by LuckyM on 2/15/2019.
 */

public class ViewHolderGLocLov {
    public TextView XLocationID;
    public TextView XLocationName;
    public TextView XSegment1;
    public TextView XSegment2;
    public TextView XSegment3;
    public TextView XSegment4;
    public TextView XSegment5;

    // ----------- getter ---------
    public String getXLocationID()   { return XLocationID.getText().toString(); }
    public String getXLocationName() { return XLocationName.getText().toString(); }
    public String getXSegment1()     { return XSegment1.getText().toString(); }
    public String getXSegment2()     { return XSegment2.getText().toString(); }
    public String getXSegment3()     { return XSegment3.getText().toString(); }
    public String getXSegment4()     { return XSegment4.getText().toString(); }
    public String getXSegment5()     { return XSegment5.getText().toString(); }

    // ------------ setter ----------
    public void setXLocationID(TextView XLocationID) {
        this.XLocationID = XLocationID;
    }
    public void setXLocationName(TextView XLocationName) {
        this.XLocationName = XLocationName;
    }
    public void setXSegment1(String StrSegment1) { this.XSegment1.setText(StrSegment1); }
    public void setXSegment2(String StrSegment2) {
        this.XSegment2.setText(StrSegment2);
    }
    public void setXSegment3(String StrSegment3) {
        this.XSegment3.setText(StrSegment3);
    }
    public void setXSegment4(String StrSegment4) {
        this.XSegment4.setText(StrSegment4);
    }
    public void setXSegment5(String StrSegment5) {
        this.XSegment5.setText(StrSegment5);
    }


}
