package com.gudanggaramtbk.myscanit.viewholder;

import android.media.Image;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by LuckyM on 2/13/2019.
 */

public class ViewHolderMain {
    public CheckBox Chk;
    public TextView Txt_asset_book;
    public TextView Txt_asset_id;
    public TextView Txt_asset_category;
    public TextView Txt_asset_barcode;
    public TextView Txt_asset_desc;
    //public TextView Txt_asset_assigned;
    public TextView Txt_asset_down_date;
    public TextView Txt_asset_scan_date;
    public TextView Txt_qty;
    public TextView Txt_segment2;
    public TextView Txt_segment3;
    public TextView Txt_segment4;
    public TextView Txt_segment5;
    public ImageButton Cmd_action_edit;
    public ImageButton Cmd_action_delete;
}
