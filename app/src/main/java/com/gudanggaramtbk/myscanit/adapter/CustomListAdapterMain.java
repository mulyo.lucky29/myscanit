package com.gudanggaramtbk.myscanit.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.viewholder.ViewHolderMain;
import com.gudanggaramtbk.myscanit.activity.MainActivity;
import com.gudanggaramtbk.myscanit.model.CAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by LuckyM on 2/13/2019.
 */

public class CustomListAdapterMain extends ArrayAdapter<CAsset> {
    private Context                 mcontext;
    private List<CAsset>            originalData = null;
    private List<CAsset>            filteredData = null;
    private LayoutInflater          mInflater;
    private ListView                olistasset;
    private MainActivity cParent;
    private MySQLiteHelper dbHelper;
    private String                  pnik;


    public CustomListAdapterMain(Context        context,
                                 List<CAsset>   list,
                                 MySQLiteHelper dbHelper,
                                 MainActivity   oParent,
                                 ListView       listasset,
                                 String         oNik)
    {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        this.cParent  = oParent;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.filteredData  = list;
        this.originalData  = list;
        this.olistasset    = listasset;
        this.pnik          = oNik;

        // initialized list data to be uncheck
        for (int i = 0; i < list.size(); i++) {
            filteredData.get(i).setChecked(false);
            olistasset.setItemChecked(i, false);
        }
    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public CAsset getItemAtPosition(int position){
        return filteredData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderMain holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_main, parent, false);
            holder = new ViewHolderMain();
            holder.Txt_asset_id        = (TextView)    convertView.findViewById(R.id.txt_asset_id);
            holder.Txt_asset_category  = (TextView)    convertView.findViewById(R.id.txt_asset_category);
            holder.Txt_asset_barcode   = (TextView)    convertView.findViewById(R.id.txt_asset_barcode);
            holder.Txt_asset_desc      = (TextView)    convertView.findViewById(R.id.txt_asset_desc);
            holder.Txt_asset_scan_date = (TextView)    convertView.findViewById(R.id.txt_asset_scan_date);
            holder.Txt_qty             = (TextView)    convertView.findViewById(R.id.txt_qty);
            holder.Txt_segment2        = (TextView)    convertView.findViewById(R.id.txt_segment2);
            holder.Txt_segment3        = (TextView)    convertView.findViewById(R.id.txt_segment3);
            holder.Txt_segment4        = (TextView)    convertView.findViewById(R.id.txt_segment4);
            holder.Txt_segment5        = (TextView)    convertView.findViewById(R.id.txt_segment5);
            holder.Cmd_action_edit     = (ImageButton) convertView.findViewById(R.id.cmd_action_edit);
            holder.Cmd_action_delete   = (ImageButton) convertView.findViewById(R.id.cmd_action_delete);
            holder.Chk                 = (CheckBox)    convertView.findViewById(R.id.sec_asset_select);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderMain) convertView.getTag();
            holder.Chk.setOnCheckedChangeListener(null);
        }

        try{
            holder.Txt_asset_id.setText(filteredData.get(position).getAsset_ID());
            holder.Txt_asset_category.setText(filteredData.get(position).getAsset_Cat().toString());
            holder.Txt_asset_barcode.setText(filteredData.get(position).getAsset_BrCode().toString());
            holder.Txt_asset_desc.setText(filteredData.get(position).getAsset_Desc().toString());
            holder.Txt_asset_scan_date.setText(filteredData.get(position).getSCAN_DATE().toString());
            holder.Txt_qty.setText(filteredData.get(position).getAsset_Qty_af().toString());
            holder.Txt_segment2.setText(filteredData.get(position).getLoc_Seg2().toString());
            holder.Txt_segment3.setText(filteredData.get(position).getLoc_Seg3().toString());
            holder.Txt_segment4.setText(filteredData.get(position).getLoc_Seg4().toString());
            holder.Txt_segment5.setText(filteredData.get(position).getLoc_Seg5().toString());
            holder.Chk.setChecked(filteredData.get(position).getChecked());

            holder.Chk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    EH_CMD_CHECK(v,holder, position);
                }
            });
            holder.Cmd_action_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_DELETE : " + filteredData.get(position).getAsset_ID().toString());
                    EH_CMD_DELETE(filteredData.get(position).getAsset_ID().toString());
                }
            });

            holder.Cmd_action_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EH_CMD_EDIT(filteredData.get(position).getAsset_ID().toString());
                }
            });
        }
        catch(Exception e){
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                String FindWord = charText.toString().toLowerCase(Locale.getDefault());
                String filterableString;

                FilterResults filterResults = new FilterResults();
                final List<CAsset> list = originalData;
                int count = list.size();
                final ArrayList<CAsset> nlist = new ArrayList<CAsset>(count);

                for(int i = 0; i< count; i++){
                    filterableString = list.get(i).getAsset_BrCode() + " " + list.get(i).getAsset_Desc() + " " + list.get(i).getLoc_Seg2() + " " + list.get(i).getLoc_Seg3() + " " + list.get(i).getLoc_Seg4() + " " + list.get(i).getLoc_Seg5();
                    if(filterableString.toLowerCase().contains(FindWord)){
                        nlist.add(list.get(i));
                    }
                }
                filterResults.values = nlist;
                filterResults.count  = nlist.size();

                return filterResults;
            };

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charText, FilterResults results) {
                Log.d("[GudangGaram]", "CustomGLocLovAdapter :: publishResults charText :: " + charText.toString());
                filteredData = (ArrayList<CAsset>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    // =================================== event handler ===========================================
    public void EH_CMD_SELECT_ALL(){
        Integer idx = 0;
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_SELECT_ALL  Begin");
        try{
            for(idx=0; idx<filteredData.size(); idx++){
                filteredData.get(idx).setChecked(true);
                olistasset.setItemChecked(idx, true);
            }
        }
        catch(Exception e){
        }
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_SELECT_ALL  End");
    }

    public void EH_CMD_CLEAR_ALL(){
        Integer idx = 0;
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CLEAR_ALL Begin");
        try{
            for(idx=0; idx<filteredData.size(); idx++){
                filteredData.get(idx).setChecked(false);
                olistasset.setItemChecked(idx, false);
            }
        }
        catch(Exception e){
        }
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CLEAR_ALL End");
    }

    public void EH_CMD_CHECK(View v, ViewHolderMain hd, Integer position){
        Boolean selection;
        CheckBox cb = (CheckBox) v ;
        selection = ((CheckBox) v).isChecked();

        try{
            if(selection == true){
                Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK :: " +  position + " Checked");
                filteredData.get(position).setChecked(true);
                olistasset.setItemChecked(position, true);
            }
            else{
                Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK :: " + position + " UnChecked");
                filteredData.get(position).setChecked(false);
                olistasset.setItemChecked(position, false);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK Exception " + e.getMessage().toString());
        }
    }

    public void EH_CMD_DELETE(final String p_asset_id){
        Log.d("[GudangGaram]", "CustomListAdapterMain :: EH_CMD_DELETE");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mcontext);
        alertDialogBuilder
                .setTitle("Delete Confirm ")
                .setMessage("Confirm Delete Data ID = "  + p_asset_id + " ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // --- reset aseet ----
                                dbHelper.ResetAsset(p_asset_id);
                                // --- refresh list ---
                                cParent.EH_cmd_Refresh();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void EH_CMD_DELETE_ALL(final String p_nik){
        Log.d("[GudangGaram]", "MainActivity :: EH_CMD_DELETE_ALL");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mcontext);
        alertDialogBuilder
                .setTitle("Delete Confirm ")
                .setMessage("Confirm Delete Data ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // --- reset asset all ---
                                dbHelper.ResetAssetAll(p_nik);
                                // --- refresh list ---
                                cParent.EH_cmd_Refresh();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_EDIT(final String p_asset_id){
        Log.d("[GudangGaram]", "CustomListAdapterMain :: EH_CMD_EDIT");
        try{
          cParent.EH_cmd_asset_info(p_asset_id);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "CustomListAdapterMain :: EH_CMD_EDIT Exception :" + e.getMessage().toString());
        }
    }
}
