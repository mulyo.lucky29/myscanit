package com.gudanggaramtbk.myscanit.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.viewholder.ViewHolderMain;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;
import com.gudanggaramtbk.myscanit.model.CAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by LuckyM on 3/18/2019.
 */

public class CustomListAdapterDownloadAsset extends ArrayAdapter<CAsset> {
    private Context                   mcontext;
    private List<CAsset>              originalData = null;
    private List<CAsset>              filteredData = null;
    private LayoutInflater            mInflater;
    private Integer                   FilterResult;
    private DownloadAssetActivity pActivity;

    public CustomListAdapterDownloadAsset(Context context, List<CAsset> clist)
    {
        super(context,0,clist);
        this.mcontext = context;
        this.filteredData  = clist;
        this.originalData  = clist;
        this.FilterResult  = 0;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    // ----------------------------- getter ----------------------------------------
    public int getCount(){
        return filteredData.size();
    }
    public Integer getFilterResult() {  return FilterResult; }

    // ----------------------------- setter ----------------------------------------
    public void setParentActivity(DownloadAssetActivity oParent){
        this.pActivity = oParent;
    }
    //public void setFilterResult(Integer filterResult) {  FilterResult = filterResult;  }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderMain holder;
        Log.d("[GudangGaram]", "CustomListAdapterDownloadAsset :: getView");

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_download_asset, parent, false);
            holder = new ViewHolderMain();
            holder.Txt_asset_book      = (TextView) convertView.findViewById(R.id.txt_dl_asset_book);
            holder.Txt_asset_id        = (TextView) convertView.findViewById(R.id.txt_dl_asset_id);
            holder.Txt_asset_category  = (TextView) convertView.findViewById(R.id.txt_dl_asset_category);
            holder.Txt_asset_barcode   = (TextView) convertView.findViewById(R.id.txt_dl_asset_barcode);
            holder.Txt_asset_desc      = (TextView) convertView.findViewById(R.id.txt_dl_asset_desc);
            holder.Txt_asset_down_date = (TextView) convertView.findViewById(R.id.txt_dl_asset_down_date);
            holder.Txt_qty             = (TextView) convertView.findViewById(R.id.txt_dl_qty);
            holder.Txt_segment2        = (TextView) convertView.findViewById(R.id.txt_dl_segment2);
            holder.Txt_segment3        = (TextView) convertView.findViewById(R.id.txt_dl_segment3);
            holder.Txt_segment4        = (TextView) convertView.findViewById(R.id.txt_dl_segment4);
            holder.Txt_segment5        = (TextView) convertView.findViewById(R.id.txt_dl_segment5);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderMain ) convertView.getTag();
        }

        try{
            holder.Txt_asset_book.setText(filteredData.get(position).getAsset_Book());
            holder.Txt_asset_id.setText(filteredData.get(position).getAsset_ID());
            holder.Txt_asset_category.setText(filteredData.get(position).getAsset_Cat().toString());
            holder.Txt_asset_barcode.setText(filteredData.get(position).getAsset_BrCode().toString());
            holder.Txt_asset_desc.setText(filteredData.get(position).getAsset_Desc().toString());
            holder.Txt_asset_down_date.setText(filteredData.get(position).getDOWNLOAD_DATE().toString());
            holder.Txt_qty.setText(filteredData.get(position).getAsset_Qty_af().toString());
            holder.Txt_segment2.setText(filteredData.get(position).getLoc_Seg2().toString());
            holder.Txt_segment3.setText(filteredData.get(position).getLoc_Seg3().toString());
            holder.Txt_segment4.setText(filteredData.get(position).getLoc_Seg4().toString());
            holder.Txt_segment5.setText(filteredData.get(position).getLoc_Seg5().toString());
        }
        catch(Exception e){
            //Log.d("[GudangGaram]", "CustomListAdapterDownloadAsset :: getView Exception : " + e.getMessage().toString());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                String FindWord = charText.toString().toLowerCase(Locale.getDefault());
                String filterableString;

                FilterResults filterResults = new FilterResults();
                final List<CAsset> tmplist = originalData;
                int count = tmplist.size();
                final ArrayList<CAsset> nlist = new ArrayList<CAsset>(count);
                Log.d("[GudangGaram]", "CustomListAdapterDownloadAsset :: performFiltering : " + charText + " > " + count);

                for(int i = 0; i< count; i++){
                    filterableString = tmplist.get(i).getAsset_BrCode() + tmplist.get(i).getAsset_Desc() + tmplist.get(i).getLoc_Seg4() + tmplist.get(i).getLoc_Seg5();
                    if(filterableString.toLowerCase().contains(FindWord)){
                        nlist.add(tmplist.get(i));
                    }
                }
                Log.d("[GudangGaram]", "CustomListAdapterDownloadAsset :: performFiltering : filterResult : " + nlist.size());

                //setFilterResult(nlist.size());
                filterResults.values = nlist;
                filterResults.count  = nlist.size();

                // update record count search result
                try{
                    pActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pActivity.setLbl_down_record(Long.toString(nlist.size()) + " Records");
                        }
                    });
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "CustomListAdapterDownloadAsset :: performFiltering Exception " + e.getMessage());
                }


                return filterResults;
            };

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charText, FilterResults results) {
                Log.d("[GudangGaram]", "CustomGLocLovAdapter :: publishResults charText :: " + charText.toString());
                filteredData = (ArrayList<CAsset>) results.values;
                notifyDataSetChanged();
            }
        };
    }

}
