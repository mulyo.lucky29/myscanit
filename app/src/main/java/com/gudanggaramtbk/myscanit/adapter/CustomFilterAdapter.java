package com.gudanggaramtbk.myscanit.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;
import com.gudanggaramtbk.myscanit.model.CFilter;

/**
 * Created by luckym on 3/20/2019.
 */

public class CustomFilterAdapter {
    private SharedPreferences     config;
    private Context               context;
    private MySQLiteHelper dbHelper;
    private TextView              Txt_spf_asset_book;
    private EditText              Txt_spf_kota;
    private EditText              Txt_spf_location;
    private ImageButton           Cmd_spf_clear;
    private ImageButton           Cmd_spf_apply;
    private ImageButton           Cmd_spf_kota_lov;
    private ImageButton           Cmd_spf_location_lov;
    private CheckBox              Chk_spf_not_scan;
    private DownloadAssetActivity PActivity;
    private CFilter cfd;

    // override constructor
    public CustomFilterAdapter(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "CustomFilterAdapter :: Constructor");
        this.config = PConfig;
        cfd = new CFilter();
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "CustomFilterAdapter :: SetContext");
        this.context = context;
    }
    public void setParameterField(CFilter ocf){
        Txt_spf_asset_book.setText(ocf.getFAssetBook().toString());
        Txt_spf_kota.setText(ocf.getFKota().toString());
        Txt_spf_location.setText(ocf.getFLocation().toString());
        Chk_spf_not_scan.setChecked(ocf.getFData());
    }

    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "CustomFilterAdapter :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void setParentAtivity(DownloadAssetActivity oParent){
        Log.d("[GudangGaram]", "CustomFilterAdapter :: SetParentActivity");
        this.PActivity = oParent;
    }

    // --------------- setter --------------
    public void setTxt_spf_kota(String Ptxt_spf_kota) {
        Txt_spf_kota.setText(Ptxt_spf_kota);
    }
    public void setTxt_spf_location(String Ptxt_spf_location) {
        Txt_spf_location.setText(Ptxt_spf_location);
    }

    public View getView(CFilter oCf){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.asset_filter, null);
        Txt_spf_asset_book   = (TextView)    promptsView.findViewById(R.id.txt_spf_asset_book);
        Txt_spf_kota         = (EditText)    promptsView.findViewById(R.id.txt_spf_kota);
        Txt_spf_location     = (EditText)    promptsView.findViewById(R.id.txt_spf_location);
        Chk_spf_not_scan     = (CheckBox)    promptsView.findViewById(R.id.chk_spf_not_scan);
        Cmd_spf_clear        = (ImageButton) promptsView.findViewById(R.id.cmd_spf_clear);
        Cmd_spf_apply        = (ImageButton) promptsView.findViewById(R.id.cmd_spf_apply);
        Cmd_spf_kota_lov     = (ImageButton) promptsView.findViewById(R.id.cmd_spf_kota_lov);
        Cmd_spf_location_lov = (ImageButton) promptsView.findViewById(R.id.cmd_spf_location_lov);

        try{
            // -------- reset param data ------------------------------------------
            cfd.setAllFilter(oCf.getFAssetBook().toString(),oCf.getFKota().toString(),oCf.getFLocation().toString(),oCf.getFData(),oCf.getQueryCondition().toString());
            // -------- filled pre defined parameter value to param field ---------
            setParameterField(oCf);
        }
        catch(Exception e){
        }

        Cmd_spf_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_construct_filter_dialog :: Cmd_spf_clear");

                // ---------- clear all filter parameter -------------------------------
                cfd.setAllFilter("GGPWJ","","",false,"");
                // -------- filled pre defined parameter value to param field ---------
                setParameterField(cfd);
                // -------- sent back to sync with parent parameter and then apply -------------------
                PActivity.setFilterParameters(cfd);
                PActivity.ApplyFilter();
                PActivity.EH_cmd_close_filter();
            }
        });

        Cmd_spf_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String kueri = "";
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_construct_filter_dialog :: Cmd_spf_apply");
                if(Txt_spf_kota.getText().length() > 0){
                    kueri = kueri + " AND lo.Segment3  = '" + Txt_spf_kota.getText().toString() + "' ";
                }
                if(Txt_spf_location.getText().length() > 0){
                    kueri = kueri + " AND lo.Segment4 = '" + Txt_spf_location.getText().toString() + "' ";
                }
                if(Chk_spf_not_scan.isChecked() == true){
                    kueri = kueri + " AND ma.Scan_Date IS NULL ";
                }

                // ----------- set all filter parameter -----------
                cfd.setAllFilter(Txt_spf_asset_book.getText().toString(),Txt_spf_kota.getText().toString(),Txt_spf_location.getText().toString(), Chk_spf_not_scan.isChecked(),kueri);
                // -------- sent back to sync with parent parameter and then apply -------------------
                PActivity.setFilterParameters(cfd);
                PActivity.ApplyFilter();
                PActivity.EH_cmd_close_filter();
            }
        });

        // ------------ lov button kota --------------------
        Cmd_spf_kota_lov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_construct_filter_dialog :: Cmd_spf_kota_lov");
                // ------- move focus on textfield --------
                Txt_spf_kota.requestFocus();
                // ------- summon fragment lov ---------
                PActivity.ConstructLOVFragmentKota();
            }
        });
        // ------------ lov button location  --------------------
        Cmd_spf_location_lov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "DownloadAssetActivity :: EH_construct_filter_dialog :: Cmd_spf_location_lov");
                // ------- move focus on textfield --------
                Txt_spf_location.requestFocus();
                // ------- summon fragment lov ---------
                PActivity.ConstructLOVFragmentLokasi();
            }
        });

        return  promptsView;
    }

}
