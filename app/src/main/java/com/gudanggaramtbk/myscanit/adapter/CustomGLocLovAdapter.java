package com.gudanggaramtbk.myscanit.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.gudanggaramtbk.myscanit.R;
import com.gudanggaramtbk.myscanit.viewholder.ViewHolderGLocLov;
import com.gudanggaramtbk.myscanit.model.CLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by LuckyM on 2/15/2019.
 */

public class CustomGLocLovAdapter extends ArrayAdapter<CLocation> implements Filterable{
    private Context                   mcontext;
    private List<CLocation>           originalData = null;
    private List<CLocation>           filteredData = null;
    private LayoutInflater            mInflater;


    public CustomGLocLovAdapter(Context context,  List<CLocation> list) {
        super(context, 0, list);
        this.mcontext = context;
        this.filteredData  = list;
        this.originalData  = list;
        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public CLocation getItemAtPosition(int position){
        return filteredData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderGLocLov holder;
        Log.d("[GudangGaram]", "CustomGLocLovAdapter :: getView");

        // ------- initialized holder --------
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_lov_glokasi, parent, false);
            holder = new ViewHolderGLocLov();
            holder.XLocationID   = (TextView) convertView.findViewById(R.id.lbl_glov_id);
            holder.XLocationName = (TextView) convertView.findViewById(R.id.lbl_glov_code);
            holder.XSegment2     = (TextView) convertView.findViewById(R.id.lbl_glov_segment2);
            holder.XSegment3     = (TextView) convertView.findViewById(R.id.lbl_glov_segment3);
            holder.XSegment4     = (TextView) convertView.findViewById(R.id.lbl_glov_segment4);
            holder.XSegment5     = (TextView) convertView.findViewById(R.id.lbl_glov_segment5);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderGLocLov) convertView.getTag();
        }

        // ------- assign data to holder ---------
        try{
                holder.XLocationID.setText(filteredData.get(position).getLocationID().toString());
                holder.XLocationName.setText(filteredData.get(position).getLocationName().toString());
                holder.XSegment2.setText(filteredData.get(position).getSegment2().toString());
                holder.XSegment3.setText(filteredData.get(position).getSegment3().toString());
                holder.XSegment4.setText(filteredData.get(position).getSegment4().toString());
                holder.XSegment5.setText(filteredData.get(position).getSegment5().toString());
        }
        catch(Exception e){
            //Log.d("[GudangGaram]", "CustomGLocLovAdapter :: getView Exception : " + e.getMessage().toString());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                String FindWord = charText.toString().toLowerCase(Locale.getDefault());
                String filterableString;

                FilterResults filterResults = new FilterResults();
                final List<CLocation> list = originalData;
                int count = list.size();
                final ArrayList<CLocation> nlist = new ArrayList<CLocation>(count);

                for(int i = 0; i< count; i++){
                    filterableString = list.get(i).getSegment3().toString() + list.get(i).getSegment4().toString() + list.get(i).getSegment5().toString();
                    if(filterableString.toLowerCase().contains(FindWord)){
                        nlist.add(list.get(i));
                    }
                }
                filterResults.values = nlist;
                filterResults.count  = nlist.size();

                return filterResults;
            };

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charText, FilterResults results) {
                Log.d("[GudangGaram]", "CustomGLocLovAdapter :: publishResults charText :: " + charText.toString());
                filteredData = (ArrayList<CLocation>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
