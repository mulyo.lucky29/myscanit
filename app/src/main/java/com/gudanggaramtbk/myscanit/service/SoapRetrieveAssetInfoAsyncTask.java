package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;
import com.gudanggaramtbk.myscanit.model.CAsset;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyM on 2/12/2019.
 */

public class SoapRetrieveAssetInfoAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String               SentResponse;

    private String               p_asset_book;
    private Integer              p_page;
    private Integer              p_tpage;
    private String               p_segment3;
    private String               p_segment4;
    private String               barcode_asset_code;
    private String               asset_book;
    private String               asset_id;
    private String               asset_description;
    private Integer              asset_category_id;
    private String               asset_category;
    private Integer              location_id;
    private Integer              assigned_to_person_id;
    private String               assigned_to_nik;
    private String               assigned_to_name;
    private Integer              Qty;
    private String               Attribute1;
    private String               Attribute2;
    private String               Attribute3;
    private String               Attribute4;
    private String               Attribute5;
    private String               Attribute6;
    private String               Attribute7;
    private String               Attribute8;
    private String               Attribute9;
    private String               Attribute10;
    private String               Attribute11;
    private String               Attribute12;
    private String               Attribute13;
    private String               Attribute14;

    private ArrayAdapter<String>    adapter;
    private DownloadAssetActivity PActivity;

    public SoapRetrieveAssetInfoAsyncTask(){ }
    public interface SoapRetrieveAssetInfoAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveAssetInfoAsyncTask.SoapRetrieveAssetInfoAsyncTaskResponse delegate = null;
    public SoapRetrieveAssetInfoAsyncTask(SoapRetrieveAssetInfoAsyncTask.SoapRetrieveAssetInfoAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             Integer p_tpage,
                             Integer p_page,
                             String  p_asset_book,
                             String  p_segment3,
                             String  p_segment4){
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfoAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.p_asset_book      = p_asset_book;
        this.p_tpage           = p_tpage;
        this.p_page            = p_page;
        this.p_segment3        = p_segment3;
        this.p_segment4        = p_segment4;
        this.pd = new ProgressDialog(this.context);
    }
    public void setParentActivity(DownloadAssetActivity oParent){
        this.PActivity = oParent;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfoAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Asset Data Step " + p_page + " of " + p_tpage  + " ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "SoapRetrieveAssetInfoAsyncTask :: onPostExecute >> " + output);
            try{
                delegate.PostSentAction(output);
                // increment Current Page
                PActivity.setIncrementCurrentPage();
                // Request Next Page
                PActivity.RequestPageAssetDetail(PActivity.getTotalPage(),PActivity.getCurrentPage(),p_asset_book,p_segment3,p_segment4);
            }
            catch(Exception e){
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;
        Integer  nerr;
        Long     Result;
        String   row_num;
        String   row_total;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Info_Asset";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_asset_book ===================
        PropertyInfo prop_p_asset_book = new PropertyInfo();
        prop_p_asset_book.setName("p_asset_book");
        prop_p_asset_book.setValue(p_asset_book);
        prop_p_asset_book.setType(String.class);
        request.addProperty(prop_p_asset_book);

        // =============== p_page ===================
        PropertyInfo prop_p_page = new PropertyInfo();
        prop_p_page.setName("p_page");
        prop_p_page.setValue(p_page);
        prop_p_page.setType(Number.class);
        request.addProperty(prop_p_page);

        // =============== p_segment3 ===================
        PropertyInfo prop_p_segment3 = new PropertyInfo();
        prop_p_segment3.setName("p_segment3");
        prop_p_segment3.setValue(p_segment3);
        prop_p_segment3.setType(String.class);
        request.addProperty(prop_p_segment3);

        // =============== p_segment4 ===================
        PropertyInfo prop_p_segment4 = new PropertyInfo();
        prop_p_segment4.setName("p_segment4");
        prop_p_segment4.setValue(p_segment4);
        prop_p_segment4.setType(String.class);
        request.addProperty(prop_p_segment4);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                nerr   = 0;
                Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    List<CAsset> LCAsset = new ArrayList<>();

                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);

                        row_num               = obj3.getProperty(0).toString();
                        row_total             = obj3.getProperty(1).toString();
                        barcode_asset_code    = obj3.getProperty(2).toString();
                        asset_book            = obj3.getProperty(3).toString();
                        asset_id              = obj3.getProperty(4).toString();
                        asset_description     = obj3.getProperty(5).toString();
                        asset_category_id     = Integer.parseInt(obj3.getProperty(6).toString());
                        asset_category        = obj3.getProperty(7).toString();
                        location_id           = Integer.parseInt(obj3.getProperty(8).toString());
                        assigned_to_person_id = Integer.parseInt(obj3.getProperty(9).toString());
                        assigned_to_nik       = obj3.getProperty(10).toString();
                        assigned_to_name      = obj3.getProperty(11).toString();
                        Qty                   = Integer.parseInt(obj3.getProperty(12).toString());
                        Attribute1            = obj3.getProperty(13).toString();
                        Attribute2            = obj3.getProperty(14).toString();
                        Attribute3            = obj3.getProperty(15).toString();
                        Attribute4            = obj3.getProperty(16).toString();
                        Attribute5            = obj3.getProperty(17).toString();
                        Attribute6            = obj3.getProperty(18).toString();
                        Attribute7            = obj3.getProperty(19).toString();
                        Attribute8            = obj3.getProperty(20).toString();
                        Attribute9            = obj3.getProperty(21).toString();
                        Attribute10           = obj3.getProperty(22).toString();
                        Attribute11           = obj3.getProperty(23).toString();
                        Attribute12           = obj3.getProperty(24).toString();
                        Attribute13           = obj3.getProperty(25).toString();
                        Attribute14           = obj3.getProperty(26).toString();


                        Log.d("[GudangGaram]", "Record : " + row_num + " of " + row_total);
                        /*
                        Log.d("[GudangGaram]", "===================================================");
                        Log.d("[GudangGaram]", "Barcode Asset Code      :" + barcode_asset_code);
                        Log.d("[GudangGaram]", "Asset Book              :" + asset_book);
                        Log.d("[GudangGaram]", "Asset ID                :" + asset_id);
                        Log.d("[GudangGaram]", "Asset Description       :" + asset_description);
                        Log.d("[GudangGaram]", "Asset Category ID       :" + asset_category_id);
                        Log.d("[GudangGaram]", "Asset Category          :" + asset_category);
                        Log.d("[GudangGaram]", "Location ID             :" + location_id);
                        Log.d("[GudangGaram]", "Assigned To Person ID   :" + assigned_to_person_id);
                        Log.d("[GudangGaram]", "Assigned To NIK         :" + assigned_to_nik);
                        Log.d("[GudangGaram]", "Assigned To Name        :" + assigned_to_name);
                        Log.d("[GudangGaram]", "Qty                     :" + Qty);
                        Log.d("[GudangGaram]", "Attribute1              :" + Attribute1);
                        Log.d("[GudangGaram]", "Attribute2              :" + Attribute2);
                        Log.d("[GudangGaram]", "Attribute3              :" + Attribute3);
                        Log.d("[GudangGaram]", "Attribute4              :" + Attribute4);
                        Log.d("[GudangGaram]", "Attribute5              :" + Attribute5);
                        Log.d("[GudangGaram]", "Attribute6              :" + Attribute6);
                        Log.d("[GudangGaram]", "Attribute7              :" + Attribute7);
                        Log.d("[GudangGaram]", "Attribute8              :" + Attribute8);
                        Log.d("[GudangGaram]", "Attribute9              :" + Attribute9);
                        Log.d("[GudangGaram]", "Attribute10             :" + Attribute10);
                        Log.d("[GudangGaram]", "Attribute11             :" + Attribute11);
                        Log.d("[GudangGaram]", "Attribute12             :" + Attribute12);
                        Log.d("[GudangGaram]", "Attribute13             :" + Attribute13);
                        Log.d("[GudangGaram]", "Attribute14             :" + Attribute14);
                        */

                        CAsset oClass = new CAsset();
                        oClass.setAsset_BrCode(barcode_asset_code);
                        oClass.setAsset_Book(asset_book);
                        oClass.setAsset_ID(asset_id);
                        oClass.setAsset_Desc(asset_description);
                        oClass.setAsset_Cat_ID(asset_category_id);
                        oClass.setAsset_Cat(asset_category);
                        oClass.setAsset_Loc_ID_af(location_id);
                        oClass.setAsset_Loc_ID_bf(location_id);
                        oClass.setAsset_Assign_ID(assigned_to_person_id);
                        oClass.setAsset_Assign_NIK_af(assigned_to_nik);
                        oClass.setAsset_Assign_NIK_bf(assigned_to_nik);
                        oClass.setAsset_Assign_Name(assigned_to_name);
                        oClass.setAsset_Qty_af(Qty);
                        oClass.setAsset_Qty_bf(Qty);
                        oClass.setA1_bf(Attribute1);
                        oClass.setA2_bf(Attribute2);
                        oClass.setA3_bf(Attribute3);
                        oClass.setA4_bf(Attribute4);
                        oClass.setA5_bf(Attribute5);
                        oClass.setA6_bf(Attribute6);
                        oClass.setA7_bf(Attribute7);
                        oClass.setA8_bf(Attribute8);
                        oClass.setA9_bf(Attribute9);
                        oClass.setA10_bf(Attribute10);
                        oClass.setA11_bf(Attribute11);
                        oClass.setA12_bf(Attribute12);
                        oClass.setA13_bf(Attribute13);
                        oClass.setA14_bf(Attribute14);
                        oClass.setA1_af(Attribute1);
                        oClass.setA2_af(Attribute2);
                        oClass.setA3_af(Attribute3);
                        oClass.setA4_af(Attribute4);
                        oClass.setA5_af(Attribute5);
                        oClass.setA6_af(Attribute6);
                        oClass.setA7_af(Attribute7);
                        oClass.setA8_af(Attribute8);
                        oClass.setA9_af(Attribute9);
                        oClass.setA10_af(Attribute10);
                        oClass.setA11_af(Attribute11);
                        oClass.setA12_af(Attribute12);
                        oClass.setA13_af(Attribute13);
                        oClass.setA14_af(Attribute14);

                        // add class asset to List
                        LCAsset.add(oClass);


                        /*
                        Result = dbHelper.AddAsset(oClass);
                        if(Result > 0){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            publishProgress("Retrieve Asset [ " + row_num + " of " + row_total + " ] >> [ " + barcode_asset_code  + " ] | " + asset_description);
                        }
                        else{
                            nerr +=1;
                        }
                        Thread.sleep(200);
                        */
                    }

                    // optimize insert time using batch insertion
                    dbHelper.AddBulkAsset(LCAsset);
                }
            }
            catch(NullPointerException e){
                //Toast.makeText(context, "Connection Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: end doInBackground");

        return "";
    }
}
