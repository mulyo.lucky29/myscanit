package com.gudanggaramtbk.myscanit.service;

/**
 * Created by LuckyM on 2/12/2019.
 */

public interface SoapRetrieveAssetInfoAsyncTaskResponse {
    void PostSentAction(String output);
}
