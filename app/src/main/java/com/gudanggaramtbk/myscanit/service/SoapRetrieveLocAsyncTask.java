package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.SyncAction;
import com.gudanggaramtbk.myscanit.model.CLocation;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 2/8/2019.
 */

public class SoapRetrieveLocAsyncTask extends AsyncTask<String, String, String>  {
    private SharedPreferences    config;
    private Context              context;
    private ProgressDialog       pd;
    private MySQLiteHelper dbHelper;
    private String               SentResponse;
    private String               location_id;
    private String               location_name;
    private String               location_segment1;
    private String               location_segment2;
    private String               location_segment3;
    private String               location_segment4;
    private String               location_segment5;
    private SyncAction PActivity;

    private ArrayAdapter<String> adapter;

    public SoapRetrieveLocAsyncTask(){ }
    public interface SoapRetrieveLocAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveLocAsyncTask.SoapRetrieveLocAsyncTaskResponse delegate = null;
    public SoapRetrieveLocAsyncTask(SoapRetrieveLocAsyncTask.SoapRetrieveLocAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config){
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;

        this.pd = new ProgressDialog(this.context);
    }
    public void setParentActivity(SyncAction oParent){
        this.PActivity  = oParent;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Master Location From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask :: onPostExecute >> " + output);
        try{
            delegate.PostSentAction(output);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve Loc Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve Loc" + output + " Done", Toast.LENGTH_LONG).show();
            }

            PActivity.EH_Reload_Record_Count("loc");
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;
        Integer  nerr;
        Long     Result;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Loc";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                nerr   = 0;
                Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);

                        location_id       = obj3.getProperty(0).toString();
                        location_name     = obj3.getProperty(1).toString();
                        location_segment2 = obj3.getProperty(2).toString();
                        location_segment3 = obj3.getProperty(3).toString();
                        location_segment4 = obj3.getProperty(4).toString();
                        location_segment5 = obj3.getProperty(5).toString();

                        //ResultGudang.add(new StringWithTag(gudang_name,gudang_id));

                        Log.d("[GudangGaram]", "Location ID        :" + location_id);
                        Log.d("[GudangGaram]", "Location Name      :" + location_name);
                        Log.d("[GudangGaram]", "Location Segment2  :" + location_segment2);
                        Log.d("[GudangGaram]", "Location Segment3  :" + location_segment3);
                        Log.d("[GudangGaram]", "Location Segment4  :" + location_segment4);
                        Log.d("[GudangGaram]", "Location Segment5  :" + location_segment5);

                        CLocation oClass = new CLocation();
                        oClass.setLocationID(Integer.parseInt(location_id));
                        oClass.setLocationName(location_name);
                        oClass.setSegment2(location_segment2);
                        oClass.setSegment3(location_segment3);
                        oClass.setSegment4(location_segment4);
                        oClass.setSegment5(location_segment5);

                        Result = dbHelper.AddLocation(oClass);
                        if(Result > 0){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            publishProgress("Add Location Info [ " + Integer.toString(i+1) + " of " + ncount + " ] : " + location_name);
                        }
                        else{
                            nerr +=1;
                        }

                        Thread.sleep(200);
                    }
                }
            }
            catch(NullPointerException e){
                //Toast.makeText(context, "Connection Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: end doInBackground");

        return "";
    }
}
