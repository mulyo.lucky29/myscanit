package com.gudanggaramtbk.myscanit.service;

/**
 * Created by LuckyM on 3/13/2019.
 */

public interface SoapSendReqBatchIDAsyncTaskResponse {
    void PostSentAction(String output);
}
