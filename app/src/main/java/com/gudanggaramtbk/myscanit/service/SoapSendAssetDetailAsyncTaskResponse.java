package com.gudanggaramtbk.myscanit.service;

/**
 * Created by LuckyM on 3/13/2019.
 */

public interface SoapSendAssetDetailAsyncTaskResponse {
    void PostSentAction(String output);
}
