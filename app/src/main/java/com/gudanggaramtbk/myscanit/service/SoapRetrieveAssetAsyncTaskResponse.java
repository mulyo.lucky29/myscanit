package com.gudanggaramtbk.myscanit.service;

/**
 * Created by LuckyM on 2/8/2019.
 */

public interface SoapRetrieveAssetAsyncTaskResponse {
    void PostSentAction(String output);
}
