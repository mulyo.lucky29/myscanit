package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.SyncAction;
import com.gudanggaramtbk.myscanit.model.StringWithTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckym on 2/8/2019.
 */

public class SoapRetrievePIC {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    SyncAction PActivity;
    // override constructor
    public SoapRetrievePIC(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrievePIC :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrievePIC :: SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrievePIC :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(SyncAction oParent){
        this.PActivity   = oParent;
    }

    public String Retrieve() {
        Log.d("[GudangGaram]", "SoapRetrievePIC :: Retrieve Begin");

        //final List<String> Result = new ArrayList<String>();
        final List<StringWithTag> Result = new ArrayList<StringWithTag>();
        try {
            SoapRetrievePICAsyncTask SoapRequest = new SoapRetrievePICAsyncTask(new SoapRetrievePICAsyncTask.SoapRetrievePICAsyncTaskResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            SoapRequest.setParentActivity(PActivity);

            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveLoc :: Retrieve End");
        }
        return "";
    }
}
