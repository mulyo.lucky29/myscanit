package com.gudanggaramtbk.myscanit.service;

/**
 * Created by luckym on 1/31/2019.
 */

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
