package com.gudanggaramtbk.myscanit.service;

/**
 * Created by LuckyM on 3/19/2019.
 */

public interface SoapRetrieveCountAssetAsyncTaskResponse {
    void PostSentAction(String output);
}
