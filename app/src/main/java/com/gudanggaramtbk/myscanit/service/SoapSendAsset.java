package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.MainActivity;
import com.gudanggaramtbk.myscanit.model.CAsset;

import java.util.List;

/**
 * Created by LuckyM on 3/13/2019.
 */

public class SoapSendAsset {
    private SharedPreferences       config;
    private Context                 context;
    private MySQLiteHelper dbHelper;
    private MainActivity pActivity;

    // override constructor
    public SoapSendAsset(SharedPreferences PConfig,Context ctx, MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapSendAsset :: Constructor");
        this.context          = ctx;
        this.config           = PConfig;
        this.dbHelper         = dbHelper;
    }
    public void setParentActivity(MainActivity pActivity){
        this.pActivity = pActivity;
    }

    public void Send(final List<CAsset> listselected, final String pNiK, final  String pDeviceID) {
        Log.d("[GudangGaram]", "SoapSendAsset :: Send > Begin");

        try {
            // call WS To Retrieve BatchID
            SoapSendReqBatchIDAsyncTask SoapRequest = new SoapSendReqBatchIDAsyncTask(new SoapSendReqBatchIDAsyncTask.SoapSendReqBatchIDAsyncTaskResponse() {
                @Override
                public void PostSentAction(String BatchID) {
                    Log.d("[GudangGaram]", "SoapSendAsset :: Send :: PostSentAction :: BatchID = " + BatchID);
                    if(listselected.size() > 0){
                        Log.d("[GudangGaram]", "SoapSendAsset :: Send :: PostSentAction :: Selected Item Count = " + listselected.size());

                        try {
                            // call ws to send all data selected with batch id
                            SoapSendAssetDetailAsyncTask SoapRequest = new SoapSendAssetDetailAsyncTask(new SoapSendAssetDetailAsyncTask.SoapSendAssetDetailAsyncTaskResponse() {
                                @Override
                                public void PostSentAction(String TrxID) { }
                            });
                            SoapRequest.setAttribute(context, pActivity, dbHelper, config, BatchID, listselected, pDeviceID);
                            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                //work on sgs3 android 4.0.4
                                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                            }
                            else {
                                SoapRequest.execute(); // work on sgs2 android 2.3
                            }
                        }
                        catch (Exception e) {
                            Log.d("[GudangGaram]:", "SoapSendAsset :: Send :: PostSentAction :: Exception " + e.getMessage().toString());
                        }
                        finally {
                            Log.d("[GudangGaram]:", "SoapSendAsset Send :: PostSentAction :: Finally");
                        }

                    }
                    Log.d("[GudangGaram]", "SoapSendAsset :: Send :: PostSentAction :: End");
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SOAP REQUEST Exception :" + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapSendAsset :: Send :: End");
        }
    }
}
