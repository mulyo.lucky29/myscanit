package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

/**
 * Created by LuckyM on 2/8/2019.
 */

public class SoapRetrieveAsset {
    private SharedPreferences       config;
    private Context                 context;
    private MySQLiteHelper dbHelper;
    private DownloadAssetActivity PActivity;
    // override constructor
    public SoapRetrieveAsset(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveAsset :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveAsset :: SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveAsset :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(DownloadAssetActivity oParent){
        this.PActivity = oParent;
    }

    public String Retrieve(final String p_asset_book, final String p_segment3, final String p_segment4) {
        Log.d("[GudangGaram]", "SoapRetrieveAsset :: Retrieve Begin");
        try {
            SoapRetrieveAssetAsyncTask SoapRequest = new SoapRetrieveAssetAsyncTask(new SoapRetrieveAssetAsyncTask.SoapRetrieveAssetAsyncTaskResponse() {
                @Override
                public void PostSentAction(String totalPage) {
                    // total page
                    Log.d("[GudangGaram]", "SoapRetrieveAsset :: PostSentAction :: totalPage : " + totalPage);

                    /*
                    try{
                        if(Integer.parseInt(totalPage) > 0){
                            Log.d("[GudangGaram]", "SoapRetrieveAssetInfo Count > 0");
                            for(int p_curr_page =1; p_curr_page <= Integer.parseInt(totalPage); p_curr_page ++){

                                SoapRetrieveAssetInfo ai = new SoapRetrieveAssetInfo(config);
                                ai.setContext(context);
                                ai.setParentActivity(PActivity);
                                ai.setDBHelper(dbHelper);
                                ai.Retrieve(p_asset_book, p_curr_page, p_segment3, p_segment4);
                            }
                        }
                        else{
                            Log.d("[GudangGaram]", "SoapRetrieveAssetInfo Count = 0");
                        }
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "SoapRetrieveAssetInfo Exception : " + e.getMessage().toString());
                    }


                   //             SoapRetrieveAssetInfoAsyncTask SoapRequest = new SoapRetrieveAssetInfoAsyncTask(new SoapRetrieveAssetInfoAsyncTask.SoapRetrieveAssetInfoAsyncTaskResponse() {
                   //                 @Override
                   //                 public void PostSentAction(String output) {
                   //                 }
                   //             });
                   //             SoapRequest.setAttribute(context, dbHelper, config, p_asset_book, p_curr_page, p_segment3, p_segment4);
                   //             if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                   //                 //work on sgs3 android 4.0.4
                   //                 //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                   //                 SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                   //             }
                   //             else {
                   //                 Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: Execute Begin");
                   //                 SoapRequest.execute(); // work on sgs2 android 2.3
                   //             }
                    */
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, p_asset_book, p_segment3, p_segment4);
            SoapRequest.setParentActivity(PActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "SoapRetrieveAsset :: Execute Begin");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveAsset :: Retrieve End");
        }
        return "";
    }

}
