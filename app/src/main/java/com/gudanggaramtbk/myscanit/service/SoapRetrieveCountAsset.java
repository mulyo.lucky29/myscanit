package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

/**
 * Created by LuckyM on 3/19/2019.
 */

public class SoapRetrieveCountAsset {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private DownloadAssetActivity PActivity;

    // override constructor
    public SoapRetrieveCountAsset(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public void setParetActivity(DownloadAssetActivity oParent){
        this.PActivity = oParent;
    }
    public String Count(String p_asset_book, String p_segment3, String p_segment4) {
        Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: Retrieve Begin");
        try {
            SoapRetrieveCountAssetAsyncTask SoapRequest = new SoapRetrieveCountAssetAsyncTask(new SoapRetrieveCountAssetAsyncTask.SoapRetrieveCountAssetAsyncTaskResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, p_asset_book, p_segment3, p_segment4);
            SoapRequest.setParentActivity(PActivity);

            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: Execute Begin");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveCountAsset :: Retrieve End");
        }
        return "";
    }
}
