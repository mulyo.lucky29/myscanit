package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.SyncAction;

/**
 * Created by luckym on 2/8/2019.
 */

public class SoapRetrieveLoc {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper dbHelper;
    private SyncAction PActivity;

    // override constructor
    public SoapRetrieveLoc(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveLoc :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveLoc :: SetContext");
        this.context = context;
    }
    public void setParentActivity(SyncAction oParent){
        this.PActivity = oParent;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveLoc :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public String Retrieve() {
        Log.d("[GudangGaram]", "SoapRetrieveLoc :: Retrieve Begin");
        try {
            SoapRetrieveLocAsyncTask SoapRequest = new SoapRetrieveLocAsyncTask(new SoapRetrieveLocAsyncTask.SoapRetrieveLocAsyncTaskResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            SoapRequest.setParentActivity(PActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "SoapRetrieveLoc :: Execute Begin");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveLoc :: Retrieve End");
        }
        return "";
    }
}
