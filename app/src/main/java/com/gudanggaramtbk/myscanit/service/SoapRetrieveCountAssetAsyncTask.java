package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 3/19/2019.
 */

public class SoapRetrieveCountAssetAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String                SentResponse;
    private String                asset_book;
    private String                location_segment3;
    private String                location_segment4;
    private DownloadAssetActivity oParent;

    private ArrayAdapter<String> adapter;

    public SoapRetrieveCountAssetAsyncTask(){ }
    public interface SoapRetrieveCountAssetAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveCountAssetAsyncTask.SoapRetrieveCountAssetAsyncTaskResponse delegate = null;
    public SoapRetrieveCountAssetAsyncTask(SoapRetrieveCountAssetAsyncTask.SoapRetrieveCountAssetAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_asset_book,
                             String p_segment3,
                             String p_segment4){
        Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.asset_book        = p_asset_book;
        this.location_segment3 = p_segment3;
        this.location_segment4 = p_segment4;

        this.pd = new ProgressDialog(this.context);
    }
    public void setParentActivity(DownloadAssetActivity pObj){
        this.oParent           = pObj;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Get Info Record Count ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask :: onPostExecute >> " + output);
        try{
            delegate.PostSentAction(output);
        }
        catch(Exception e){
        }
        finally {
            // sent back record count
            oParent.set_Record_Count(output + " Records");
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount = 0;
        String   Result = "";

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_Count_List_Asset";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_asset_book ===================
        PropertyInfo prop_p_asset_book = new PropertyInfo();
        prop_p_asset_book.setName("p_asset_book");
        prop_p_asset_book.setValue(asset_book);
        prop_p_asset_book.setType(String.class);
        request.addProperty(prop_p_asset_book);

        // =============== p_segment3 ===================
        PropertyInfo prop_p_segment3 = new PropertyInfo();
        prop_p_segment3.setName("p_segment3");
        prop_p_segment3.setValue(location_segment3);
        prop_p_segment3.setType(String.class);
        request.addProperty(prop_p_segment3);

        // =============== p_segment4 ===================
        PropertyInfo prop_p_segment4 = new PropertyInfo();
        prop_p_segment4.setName("p_segment4");
        prop_p_segment4.setValue(location_segment4);
        prop_p_segment4.setType(String.class);
        request.addProperty(prop_p_segment4);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            Result = resultsRequestSOAP.getProperty(0).toString();
            Log.d("[GudangGaram]", "ResultResponse : " + Result);
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask :: Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask :: end doInBackground");
        }

        Log.d("[GudangGaram]", "SoapRetrieveCountAssetAsyncTask  :: end doInBackground");

        return Result;
    }
}
