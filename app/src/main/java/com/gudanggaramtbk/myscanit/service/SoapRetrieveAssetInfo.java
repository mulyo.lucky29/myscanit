package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

/**
 * Created by LuckyM on 2/12/2019.
 */

public class SoapRetrieveAssetInfo {
    private SharedPreferences       config;
    private Context                 context;
    private MySQLiteHelper dbHelper;
    private DownloadAssetActivity PActivity;
    // override constructor
    public SoapRetrieveAssetInfo(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(DownloadAssetActivity oParent){
        this.PActivity  = oParent;
    }

    public String Retrieve(final Integer p_total_page, final Integer p_curr_page, final String p_asset_book, final String p_segment3, final String p_segment4) {
        Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: Retrieve Begin");
        try {
            SoapRetrieveAssetInfoAsyncTask SoapRequest = new SoapRetrieveAssetInfoAsyncTask(new SoapRetrieveAssetInfoAsyncTask.SoapRetrieveAssetInfoAsyncTaskResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, p_total_page, p_curr_page, p_asset_book, p_segment3, p_segment4);
            SoapRequest.setParentActivity(PActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: Execute Begin");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveAssetInfo :: Retrieve End");
        }
        return "";
    }
}
