package com.gudanggaramtbk.myscanit.service;

/**
 * Created by luckym on 2/8/2019.
 */

public interface SoapRetrievePICAsyncTaskResponse {
    void PostSentAction(String output);
}
