package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.MainActivity;
import com.gudanggaramtbk.myscanit.model.CAsset;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by LuckyM on 3/13/2019.
 */

public class SoapSendAssetDetailAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context                   context;
    private MainActivity              pActivity;
    private ProgressDialog            pd;
    private String                    ResultResponse;
    private MySQLiteHelper            dbHelper;
    private String                    SentResponse;
    private String                    batchID;
    private String                    device_id;
    private List<CAsset>              objLCAsset;
    private Integer                   seq_no;
    private Integer                   seq_total;
    private Integer                   sent;


    public SoapSendAssetDetailAsyncTask(){}
    public interface SoapSendAssetDetailAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapSendAssetDetailAsyncTask.SoapSendAssetDetailAsyncTaskResponse delegate = null;
    public SoapSendAssetDetailAsyncTask(SoapSendAssetDetailAsyncTask.SoapSendAssetDetailAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context,
                             MainActivity      pActivity,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config,
                             String            p_batch_id,
                             List<CAsset>      p_listselected,
                             String            p_device_id)
    {
        Log.d("[GudangGaram]", "SoapSendAssetDetailAsyncTask :: setAttribute");
        this.context           = context;
        this.pActivity         = pActivity;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.batchID           = p_batch_id;
        this.objLCAsset        = p_listselected;
        this.device_id         = p_device_id;
        this.sent              = 0;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendAssetDetailAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Sending Asset Data To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        Log.d("[GudangGaram]", "SoapSendAssetDetailAsyncTask :: onProgressUpdate");
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendAssetDetailAsyncTask :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
            // if last reach then do flush asset table and refresh
            if(seq_total > 0){
                Log.d("[GudangGaram]", "count sent      = " + sent.toString());
                Log.d("[GudangGaram]", "count seq_total = " + seq_total.toString());
                if(sent.compareTo(seq_total) == 0){
                    // clear asset table
                    Log.d("[GudangGaram]", "clear asset table");
                    dbHelper.flushTable(dbHelper.getTableMAsset());
                    pActivity.EH_cmd_refresh_MainList(context, pActivity);
                }
                else{
                    Log.d("[GudangGaram]", "sent <> seq_total");
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "onPostExecute Execption : " + e.getMessage());
        }
        finally {
        }
    }

    private Integer WS_SEND(CAsset oca){
        Integer result = 0;
    /*

        String  v_oracle_id = "-1";
        String  NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String  SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Insert_Asset";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        // ===============1. p_stg_id ===================
        PropertyInfo prop_p_stg_id = new PropertyInfo();
        prop_p_stg_id.setName("p_stg_id");
        prop_p_stg_id.setValue(batchID);
        prop_p_stg_id.setType(Integer.class);

        // ===============2. p_asset_id ===================
        PropertyInfo prop_p_asset_id = new PropertyInfo();
        prop_p_asset_id.setName("p_asset_id");
        prop_p_asset_id.setValue(oca.getAsset_ID().toString());
        prop_p_asset_id.setType(Integer.class);

        // ===============3. p_asset_book: ===================
        PropertyInfo prop_p_asset_book = new PropertyInfo();
        prop_p_asset_book.setName("p_asset_book");
        prop_p_asset_book.setValue(oca.getAsset_Book().toString());
        prop_p_asset_book.setType(String.class);

        // ===============4. p_tag_number ===================
        PropertyInfo prop_p_tag_number = new PropertyInfo();
        prop_p_tag_number.setName("p_tag_number");
        prop_p_tag_number.setValue(oca.getAsset_BrCode().toString());
        prop_p_tag_number.setType(String.class);

        // ===============5. p_asset_desc ===================
        PropertyInfo prop_p_asset_desc = new PropertyInfo();
        prop_p_asset_desc.setName("p_asset_desc");
        prop_p_asset_desc.setValue(oca.getAsset_Desc().toString());
        prop_p_asset_desc.setType(String.class);

        // ===============6. p_asset_cat_id ===================
        PropertyInfo prop_p_asset_cat_id = new PropertyInfo();
        prop_p_asset_cat_id.setName("p_asset_cat_id");
        prop_p_asset_cat_id.setValue(oca.getAsset_Cat_ID());
        prop_p_asset_cat_id.setType(Integer.class);

        // ===============7. p_asset_cat  ===================
        PropertyInfo prop_p_asset_cat = new PropertyInfo();
        prop_p_asset_cat.setName("p_asset_cat");
        prop_p_asset_cat.setValue(oca.getAsset_Cat().toString());
        prop_p_asset_cat.setType(String.class);

        // ===============8. p_loc_id_bf  ===================
        PropertyInfo prop_p_loc_id_bf = new PropertyInfo();
        prop_p_loc_id_bf.setName("p_loc_id_bf");
        prop_p_loc_id_bf.setValue(oca.getAsset_Loc_ID_bf());
        prop_p_loc_id_bf.setType(Integer.class);

        // ===============9. p_loc_id_af  ===================
        PropertyInfo prop_p_loc_id_af = new PropertyInfo();
        prop_p_loc_id_af.setName("p_loc_id_af");
        prop_p_loc_id_af.setValue(oca.getAsset_Loc_ID_af());
        prop_p_loc_id_af.setType(Integer.class);

        // ===============10. p_assign_bf  ===================
        PropertyInfo prop_p_assign_bf = new PropertyInfo();
        prop_p_assign_bf.setName("p_assign_bf");
        prop_p_assign_bf.setValue(oca.getAsset_Assign_NIK_bf().toString());
        prop_p_assign_bf.setType(String.class);

        // ===============11. p_assign_af  ===================
        PropertyInfo prop_p_assign_af = new PropertyInfo();
        prop_p_assign_af.setName("p_assign_af");
        prop_p_assign_af.setValue(oca.getAsset_Assign_NIK_af().toString());
        prop_p_assign_af.setType(String.class);

        // ===============12. p_qty_bf  ===================
        PropertyInfo prop_p_qty_bf = new PropertyInfo();
        prop_p_qty_bf.setName("p_qty_bf");
        prop_p_qty_bf.setValue(oca.getAsset_Qty_bf());
        prop_p_qty_bf.setType(Integer.class);

        // ===============13. p_qty_af  ===================
        PropertyInfo prop_p_qty_af = new PropertyInfo();
        prop_p_qty_af.setName("p_qty_af");
        prop_p_qty_af.setValue(oca.getAsset_Qty_af());
        prop_p_qty_af.setType(Integer.class);

        // ===============14. p_attribute1_bf  ===================
        PropertyInfo prop_p_attribute1_bf = new PropertyInfo();
        prop_p_attribute1_bf.setName("p_attribute1_bf");
        prop_p_attribute1_bf.setValue(oca.getA1_bf().toString());
        prop_p_attribute1_bf.setType(String.class);

        // ===============15. p_attribute1_af  ===================
        PropertyInfo prop_p_attribute1_af = new PropertyInfo();
        prop_p_attribute1_af.setName("p_attribute1_af");
        prop_p_attribute1_af.setValue(oca.getA1_af().toString());
        prop_p_attribute1_af.setType(String.class);

        // ===============16. p_attribute2_bf  ===================
        PropertyInfo prop_p_attribute2_bf = new PropertyInfo();
        prop_p_attribute2_bf.setName("p_attribute2_bf");
        prop_p_attribute2_bf.setValue(oca.getA2_bf().toString());
        prop_p_attribute2_bf.setType(String.class);

        // ===============17. p_attribute2_af  ===================
        PropertyInfo prop_p_attribute2_af = new PropertyInfo();
        prop_p_attribute2_af.setName("p_attribute2_af");
        prop_p_attribute2_af.setValue(oca.getA2_af().toString());
        prop_p_attribute2_af.setType(String.class);

        // ===============18. p_attribute3_bf  ===================
        PropertyInfo prop_p_attribute3_bf = new PropertyInfo();
        prop_p_attribute3_bf.setName("p_attribute3_bf");
        prop_p_attribute3_bf.setValue(oca.getA3_bf().toString());
        prop_p_attribute3_bf.setType(String.class);

        // ===============19. p_attribute3_af  ===================
        PropertyInfo prop_p_attribute3_af = new PropertyInfo();
        prop_p_attribute3_af.setName("p_attribute3_af");
        prop_p_attribute3_af.setValue(oca.getA3_af().toString());
        prop_p_attribute3_af.setType(String.class);

        // ===============20. p_attribute4_bf  ===================
        PropertyInfo prop_p_attribute4_bf = new PropertyInfo();
        prop_p_attribute4_bf.setName("p_attribute4_bf");
        prop_p_attribute4_bf.setValue(oca.getA4_bf().toString());
        prop_p_attribute4_bf.setType(String.class);

        // ===============21. p_attribute4_af  ===================
        PropertyInfo prop_p_attribute4_af = new PropertyInfo();
        prop_p_attribute4_af.setName("p_attribute4_af");
        prop_p_attribute4_af.setValue(oca.getA4_af().toString());
        prop_p_attribute4_af.setType(String.class);

        // ===============22. p_attribute5_bf  ===================
        PropertyInfo prop_p_attribute5_bf = new PropertyInfo();
        prop_p_attribute5_bf.setName("p_attribute5_bf");
        prop_p_attribute5_bf.setValue(oca.getA5_bf().toString());
        prop_p_attribute5_bf.setType(String.class);

        // ===============23. p_attribute5_af  ===================
        PropertyInfo prop_p_attribute5_af = new PropertyInfo();
        prop_p_attribute5_af.setName("p_attribute5_af");
        prop_p_attribute5_af.setValue(oca.getA5_af().toString());
        prop_p_attribute5_af.setType(String.class);

        // ===============24. p_attribute6_bf  ===================
        PropertyInfo prop_p_attribute6_bf = new PropertyInfo();
        prop_p_attribute6_bf.setName("p_attribute6_bf");
        prop_p_attribute6_bf.setValue(oca.getA6_bf().toString());
        prop_p_attribute6_bf.setType(String.class);

        // ===============25. p_attribute6_af  ===================
        PropertyInfo prop_p_attribute6_af = new PropertyInfo();
        prop_p_attribute6_af.setName("p_attribute6_af");
        prop_p_attribute6_af.setValue(oca.getA6_af().toString());
        prop_p_attribute6_af.setType(String.class);

        // ===============26. p_attribute7_bf  ===================
        PropertyInfo prop_p_attribute7_bf = new PropertyInfo();
        prop_p_attribute7_bf.setName("p_attribute7_bf");
        prop_p_attribute7_bf.setValue(oca.getA7_bf().toString());
        prop_p_attribute7_bf.setType(String.class);

        // ===============27. p_attribute7_af  ===================
        PropertyInfo prop_p_attribute7_af = new PropertyInfo();
        prop_p_attribute7_af.setName("p_attribute7_af");
        prop_p_attribute7_af.setValue(oca.getA7_af().toString());
        prop_p_attribute7_af.setType(String.class);

        // ===============28. p_attribute8_bf  ===================
        PropertyInfo prop_p_attribute8_bf = new PropertyInfo();
        prop_p_attribute8_bf.setName("p_attribute8_bf");
        prop_p_attribute8_bf.setValue(oca.getA8_bf().toString());
        prop_p_attribute8_bf.setType(String.class);

        // ===============29. p_attribute8_af  ===================
        PropertyInfo prop_p_attribute8_af = new PropertyInfo();
        prop_p_attribute8_af.setName("p_attribute8_af");
        prop_p_attribute8_af.setValue(oca.getA8_af().toString());
        prop_p_attribute8_af.setType(String.class);

        // ===============30. p_attribute9_bf  ===================
        PropertyInfo prop_p_attribute9_bf = new PropertyInfo();
        prop_p_attribute9_bf.setName("p_attribute9_bf");
        prop_p_attribute9_bf.setValue(oca.getA9_bf().toString());
        prop_p_attribute9_bf.setType(String.class);

        // ===============31. p_attribute9_af  ===================
        PropertyInfo prop_p_attribute9_af = new PropertyInfo();
        prop_p_attribute9_af.setName("p_attribute9_af");
        prop_p_attribute9_af.setValue(oca.getA9_af().toString());
        prop_p_attribute9_af.setType(String.class);

        // ===============32. p_attribute10_bf  ===================
        PropertyInfo prop_p_attribute10_bf = new PropertyInfo();
        prop_p_attribute10_bf.setName("p_attribute10_bf");
        prop_p_attribute10_bf.setValue(oca.getA10_bf().toString());
        prop_p_attribute10_bf.setType(String.class);

        // ===============33. p_attribute10_af  ===================
        PropertyInfo prop_p_attribute10_af = new PropertyInfo();
        prop_p_attribute10_af.setName("p_attribute10_af");
        prop_p_attribute10_af.setValue(oca.getA10_af().toString());
        prop_p_attribute10_af.setType(String.class);

        // ===============34. p_attribute11_bf  ===================
        PropertyInfo prop_p_attribute11_bf = new PropertyInfo();
        prop_p_attribute11_bf.setName("p_attribute11_bf");
        prop_p_attribute11_bf.setValue(oca.getA11_bf().toString());
        prop_p_attribute11_bf.setType(String.class);

        // ===============35. p_attribute11_af  ===================
        PropertyInfo prop_p_attribute11_af = new PropertyInfo();
        prop_p_attribute11_af.setName("p_attribute11_af");
        prop_p_attribute11_af.setValue(oca.getA11_af().toString());
        prop_p_attribute11_af.setType(String.class);

        // ===============36. p_attribute12_bf  ===================
        PropertyInfo prop_p_attribute12_bf = new PropertyInfo();
        prop_p_attribute12_bf.setName("p_attribute12_bf");
        prop_p_attribute12_bf.setValue(oca.getA12_bf().toString());
        prop_p_attribute12_bf.setType(String.class);

        // ===============37. p_attribute12_af  ===================
        PropertyInfo prop_p_attribute12_af = new PropertyInfo();
        prop_p_attribute12_af.setName("p_attribute12_af");
        prop_p_attribute12_af.setValue(oca.getA12_af().toString());
        prop_p_attribute12_af.setType(String.class);

        // ===============38. p_attribute13_bf  ===================
        PropertyInfo prop_p_attribute13_bf = new PropertyInfo();
        prop_p_attribute13_bf.setName("p_attribute13_bf");
        prop_p_attribute13_bf.setValue(oca.getA13_bf().toString());
        prop_p_attribute13_bf.setType(String.class);

        // ===============39. p_attribute13_af  ===================
        PropertyInfo prop_p_attribute13_af = new PropertyInfo();
        prop_p_attribute13_af.setName("p_attribute13_af");
        prop_p_attribute13_af.setValue(oca.getA13_af().toString());
        prop_p_attribute13_af.setType(String.class);

        // ===============40. p_attribute14_bf  ===================
        PropertyInfo prop_p_attribute14_bf = new PropertyInfo();
        prop_p_attribute14_bf.setName("p_attribute14_bf");
        prop_p_attribute14_bf.setValue(oca.getA14_bf().toString());
        prop_p_attribute14_bf.setType(String.class);

        // ===============41. p_attribute14_af  ===================
        PropertyInfo prop_p_attribute14_af = new PropertyInfo();
        prop_p_attribute14_af.setName("p_attribute14_af");
        prop_p_attribute14_af.setValue(oca.getA14_af().toString());
        prop_p_attribute14_af.setType(String.class);

        // ===============42. p_attribute15_bf  ===================
        PropertyInfo prop_p_attribute15_bf = new PropertyInfo();
        prop_p_attribute15_bf.setName("p_attribute15_bf");
        prop_p_attribute15_bf.setValue(oca.getA15_bf().toString());
        prop_p_attribute15_bf.setType(String.class);

        // ===============43. p_attribute15_af  ===================
        PropertyInfo prop_p_attribute15_af = new PropertyInfo();
        prop_p_attribute15_af.setName("p_attribute15_af");
        prop_p_attribute15_af.setValue(oca.getA15_af().toString());
        prop_p_attribute15_af.setType(String.class);

        // ===============44. p_attribute16_bf  ===================
        PropertyInfo prop_p_attribute16_bf = new PropertyInfo();
        prop_p_attribute16_bf.setName("p_attribute16_bf");
        prop_p_attribute16_bf.setValue(oca.getA16_bf().toString());
        prop_p_attribute16_bf.setType(String.class);

        // ===============45. p_attribute16_af  ===================
        PropertyInfo prop_p_attribute16_af = new PropertyInfo();
        prop_p_attribute16_af.setName("p_attribute16_af");
        prop_p_attribute16_af.setValue(oca.getA16_af().toString());
        prop_p_attribute16_af.setType(String.class);

        // ===============46. p_attribute17_bf  ===================
        PropertyInfo prop_p_attribute17_bf = new PropertyInfo();
        prop_p_attribute17_bf.setName("p_attribute17_bf");
        prop_p_attribute17_bf.setValue(oca.getA17_bf().toString());
        prop_p_attribute17_bf.setType(String.class);

        // ===============47. p_attribute17_af  ===================
        PropertyInfo prop_p_attribute17_af = new PropertyInfo();
        prop_p_attribute17_af.setName("p_attribute17_af");
        prop_p_attribute17_af.setValue(oca.getA17_af().toString());
        prop_p_attribute17_af.setType(String.class);

        // ===============48. p_attribute18_bf  ===================
        PropertyInfo prop_p_attribute18_bf = new PropertyInfo();
        prop_p_attribute18_bf.setName("p_attribute18_bf");
        prop_p_attribute18_bf.setValue(oca.getA18_bf().toString());
        prop_p_attribute18_bf.setType(String.class);

        // ===============49. p_attribute18_af  ===================
        PropertyInfo prop_p_attribute18_af = new PropertyInfo();
        prop_p_attribute18_af.setName("p_attribute18_af");
        prop_p_attribute18_af.setValue(oca.getA18_af().toString());
        prop_p_attribute18_af.setType(String.class);

        // ===============50. p_attribute19_bf  ===================
        PropertyInfo prop_p_attribute19_bf = new PropertyInfo();
        prop_p_attribute19_bf.setName("p_attribute19_bf");
        prop_p_attribute19_bf.setValue(oca.getA19_bf().toString());
        prop_p_attribute19_bf.setType(String.class);

        // ===============51. p_attribute19_af  ===================
        PropertyInfo prop_p_attribute19_af = new PropertyInfo();
        prop_p_attribute19_af.setName("p_attribute19_af");
        prop_p_attribute19_af.setValue(oca.getA19_af().toString());
        prop_p_attribute19_af.setType(String.class);

        // ===============52. p_attribute20_bf  ===================
        PropertyInfo prop_p_attribute20_bf = new PropertyInfo();
        prop_p_attribute20_bf.setName("p_attribute20_bf");
        prop_p_attribute20_bf.setValue(oca.getA20_bf().toString());
        prop_p_attribute20_bf.setType(String.class);

        // ===============53. p_attribute10_af  ===================
        PropertyInfo prop_p_attribute20_af = new PropertyInfo();
        prop_p_attribute20_af.setName("p_attribute20_af");
        prop_p_attribute20_af.setValue(oca.getA20_bf().toString());
        prop_p_attribute20_af.setType(String.class);

        // ===============54. p_download_date  ===================
        PropertyInfo prop_p_download_date = new PropertyInfo();
        prop_p_download_date.setName("p_download_date");
        prop_p_download_date.setValue(oca.getDOWNLOAD_DATE().toString());
        prop_p_download_date.setType(String.class);

        // ===============55. p_scan_date:  ===================
        PropertyInfo prop_p_scan_date = new PropertyInfo();
        prop_p_scan_date.setName("p_scan_date");
        prop_p_scan_date.setValue(oca.getSCAN_DATE().toString());
        prop_p_scan_date.setType(String.class);

        // ===============56. p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(device_id);
        prop_p_device_id.setType(String.class);

        // ===============57.  p_scan_by ===================
        PropertyInfo prop_p_scan_by = new PropertyInfo();
        prop_p_scan_by.setName("p_scan_by");
        prop_p_scan_by.setValue(oca.getSCAN_BY());
        prop_p_scan_by.setType(String.class);

        request.addProperty(prop_p_stg_id);
        request.addProperty(prop_p_asset_id);
        request.addProperty(prop_p_asset_book);
        request.addProperty(prop_p_tag_number);
        request.addProperty(prop_p_asset_desc);
        request.addProperty(prop_p_asset_cat_id);
        request.addProperty(prop_p_asset_cat);
        request.addProperty(prop_p_loc_id_bf);
        request.addProperty(prop_p_loc_id_af);
        request.addProperty(prop_p_assign_bf);
        request.addProperty(prop_p_assign_af);
        request.addProperty(prop_p_qty_bf);
        request.addProperty(prop_p_qty_af);
        request.addProperty(prop_p_attribute1_bf);
        request.addProperty(prop_p_attribute1_af);
        request.addProperty(prop_p_attribute2_bf);
        request.addProperty(prop_p_attribute2_af);
        request.addProperty(prop_p_attribute3_bf);
        request.addProperty(prop_p_attribute3_af);
        request.addProperty(prop_p_attribute4_bf);
        request.addProperty(prop_p_attribute4_af);
        request.addProperty(prop_p_attribute5_bf);
        request.addProperty(prop_p_attribute5_af);
        request.addProperty(prop_p_attribute6_bf);
        request.addProperty(prop_p_attribute6_af);
        request.addProperty(prop_p_attribute7_bf);
        request.addProperty(prop_p_attribute7_af);
        request.addProperty(prop_p_attribute8_bf);
        request.addProperty(prop_p_attribute8_af);
        request.addProperty(prop_p_attribute9_bf);
        request.addProperty(prop_p_attribute9_af);
        request.addProperty(prop_p_attribute10_bf);
        request.addProperty(prop_p_attribute10_af);
        request.addProperty(prop_p_attribute11_bf);
        request.addProperty(prop_p_attribute11_af);
        request.addProperty(prop_p_attribute12_bf);
        request.addProperty(prop_p_attribute12_af);
        request.addProperty(prop_p_attribute13_bf);
        request.addProperty(prop_p_attribute13_af);
        request.addProperty(prop_p_attribute14_bf);
        request.addProperty(prop_p_attribute14_af);
        request.addProperty(prop_p_attribute15_bf);
        request.addProperty(prop_p_attribute15_af);
        request.addProperty(prop_p_attribute16_bf);
        request.addProperty(prop_p_attribute16_af);
        request.addProperty(prop_p_attribute17_bf);
        request.addProperty(prop_p_attribute17_af);
        request.addProperty(prop_p_attribute18_bf);
        request.addProperty(prop_p_attribute18_af);
        request.addProperty(prop_p_attribute19_bf);
        request.addProperty(prop_p_attribute19_af);
        request.addProperty(prop_p_attribute20_bf);
        request.addProperty(prop_p_attribute20_af);
        request.addProperty(prop_p_download_date);
        request.addProperty(prop_p_scan_date);
        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_scan_by);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            v_oracle_id = resultsRequestSOAP.getProperty(0).toString();
            ResultResponse  = v_oracle_id;

            try{
                if(Integer.parseInt(v_oracle_id) > 0) {
                    result = Integer.parseInt(v_oracle_id);
                }
                else{
                    result = 0;
                }
            }
            catch(Exception e){
                result = 0;
            }

            Log.d("[GudangGaram]", "ResultResponse : " + ResultResponse);

            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            Log.d("[GudangGaram]", "Sending Opname Data " + seq_no  + " of " + seq_total + " Records");

            // publish progress
            publishProgress("Sending Opname Data " + seq_no  + " of " + seq_total + " Records");
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
        }
        */

        return  result;
    }

    @Override
    protected String doInBackground(String... params) {
        Integer result = 0;
        sent           = 0;
        seq_total = objLCAsset.size();

        for(Integer iter=0; iter<seq_total; iter ++){
            seq_no = iter + 1;
            String curAssetID = objLCAsset.get(iter).getAsset_ID().toString();
            Log.d("[GudangGaram]", "SoapSendAssetDetailAsyncTask :: doInBackground :: Process(" + Integer.toString(seq_no) + ") : " + curAssetID);

            for (CAsset oca : dbHelper.loadScanAsset(curAssetID)) {
                // ==========================================

                String  v_oracle_id = "-1";
                String  NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String  SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                String OPERATION_NAME = "Insert_Asset";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                // ===============1. p_stg_id ===================
                PropertyInfo prop_p_stg_id = new PropertyInfo();
                prop_p_stg_id.setName("p_stg_id");
                prop_p_stg_id.setValue(batchID);
                prop_p_stg_id.setType(Integer.class);

                // ===============2. p_asset_id ===================
                PropertyInfo prop_p_asset_id = new PropertyInfo();
                prop_p_asset_id.setName("p_asset_id");
                prop_p_asset_id.setValue(oca.getAsset_ID().toString());
                prop_p_asset_id.setType(Integer.class);

                // ===============3. p_asset_book: ===================
                PropertyInfo prop_p_asset_book = new PropertyInfo();
                prop_p_asset_book.setName("p_asset_book");
                prop_p_asset_book.setValue(oca.getAsset_Book().toString());
                prop_p_asset_book.setType(String.class);

                // ===============4. p_tag_number ===================
                PropertyInfo prop_p_tag_number = new PropertyInfo();
                prop_p_tag_number.setName("p_tag_number");
                prop_p_tag_number.setValue(oca.getAsset_BrCode().toString());
                prop_p_tag_number.setType(String.class);

                // ===============5. p_asset_desc ===================
                PropertyInfo prop_p_asset_desc = new PropertyInfo();
                prop_p_asset_desc.setName("p_asset_desc");
                prop_p_asset_desc.setValue(oca.getAsset_Desc().toString());
                prop_p_asset_desc.setType(String.class);

                // ===============6. p_asset_cat_id ===================
                PropertyInfo prop_p_asset_cat_id = new PropertyInfo();
                prop_p_asset_cat_id.setName("p_asset_cat_id");
                prop_p_asset_cat_id.setValue(oca.getAsset_Cat_ID());
                prop_p_asset_cat_id.setType(Integer.class);

                // ===============7. p_asset_cat  ===================
                PropertyInfo prop_p_asset_cat = new PropertyInfo();
                prop_p_asset_cat.setName("p_asset_cat");
                prop_p_asset_cat.setValue(oca.getAsset_Cat().toString());
                prop_p_asset_cat.setType(String.class);

                // ===============8. p_loc_id_bf  ===================
                PropertyInfo prop_p_loc_id_bf = new PropertyInfo();
                prop_p_loc_id_bf.setName("p_loc_id_bf");
                prop_p_loc_id_bf.setValue(oca.getAsset_Loc_ID_bf());
                prop_p_loc_id_bf.setType(Integer.class);

                // ===============9. p_loc_id_af  ===================
                PropertyInfo prop_p_loc_id_af = new PropertyInfo();
                prop_p_loc_id_af.setName("p_loc_id_af");
                prop_p_loc_id_af.setValue(oca.getAsset_Loc_ID_af());
                prop_p_loc_id_af.setType(Integer.class);

                // ===============10. p_assign_bf  ===================
                PropertyInfo prop_p_assign_bf = new PropertyInfo();
                prop_p_assign_bf.setName("p_assign_bf");
                prop_p_assign_bf.setValue(oca.getAsset_Assign_NIK_bf().toString());
                prop_p_assign_bf.setType(String.class);

                // ===============11. p_assign_af  ===================
                PropertyInfo prop_p_assign_af = new PropertyInfo();
                prop_p_assign_af.setName("p_assign_af");
                prop_p_assign_af.setValue(oca.getAsset_Assign_NIK_af().toString());
                prop_p_assign_af.setType(String.class);

                // ===============12. p_qty_bf  ===================
                PropertyInfo prop_p_qty_bf = new PropertyInfo();
                prop_p_qty_bf.setName("p_qty_bf");
                prop_p_qty_bf.setValue(oca.getAsset_Qty_bf());
                prop_p_qty_bf.setType(Integer.class);

                // ===============13. p_qty_af  ===================
                PropertyInfo prop_p_qty_af = new PropertyInfo();
                prop_p_qty_af.setName("p_qty_af");
                prop_p_qty_af.setValue(oca.getAsset_Qty_af());
                prop_p_qty_af.setType(Integer.class);

                // ===============14. p_attribute1_bf  ===================
                PropertyInfo prop_p_attribute1_bf = new PropertyInfo();
                prop_p_attribute1_bf.setName("p_attribute1_bf");
                prop_p_attribute1_bf.setValue(oca.getA1_bf().toString());
                prop_p_attribute1_bf.setType(String.class);

                // ===============15. p_attribute1_af  ===================
                PropertyInfo prop_p_attribute1_af = new PropertyInfo();
                prop_p_attribute1_af.setName("p_attribute1_af");
                prop_p_attribute1_af.setValue(oca.getA1_af().toString());
                prop_p_attribute1_af.setType(String.class);

                // ===============16. p_attribute2_bf  ===================
                PropertyInfo prop_p_attribute2_bf = new PropertyInfo();
                prop_p_attribute2_bf.setName("p_attribute2_bf");
                prop_p_attribute2_bf.setValue(oca.getA2_bf().toString());
                prop_p_attribute2_bf.setType(String.class);

                // ===============17. p_attribute2_af  ===================
                PropertyInfo prop_p_attribute2_af = new PropertyInfo();
                prop_p_attribute2_af.setName("p_attribute2_af");
                prop_p_attribute2_af.setValue(oca.getA2_af().toString());
                prop_p_attribute2_af.setType(String.class);

                // ===============18. p_attribute3_bf  ===================
                PropertyInfo prop_p_attribute3_bf = new PropertyInfo();
                prop_p_attribute3_bf.setName("p_attribute3_bf");
                prop_p_attribute3_bf.setValue(oca.getA3_bf().toString());
                prop_p_attribute3_bf.setType(String.class);

                // ===============19. p_attribute3_af  ===================
                PropertyInfo prop_p_attribute3_af = new PropertyInfo();
                prop_p_attribute3_af.setName("p_attribute3_af");
                prop_p_attribute3_af.setValue(oca.getA3_af().toString());
                prop_p_attribute3_af.setType(String.class);

                // ===============20. p_attribute4_bf  ===================
                PropertyInfo prop_p_attribute4_bf = new PropertyInfo();
                prop_p_attribute4_bf.setName("p_attribute4_bf");
                prop_p_attribute4_bf.setValue(oca.getA4_bf().toString());
                prop_p_attribute4_bf.setType(String.class);

                // ===============21. p_attribute4_af  ===================
                PropertyInfo prop_p_attribute4_af = new PropertyInfo();
                prop_p_attribute4_af.setName("p_attribute4_af");
                prop_p_attribute4_af.setValue(oca.getA4_af().toString());
                prop_p_attribute4_af.setType(String.class);

                // ===============22. p_attribute5_bf  ===================
                PropertyInfo prop_p_attribute5_bf = new PropertyInfo();
                prop_p_attribute5_bf.setName("p_attribute5_bf");
                prop_p_attribute5_bf.setValue(oca.getA5_bf().toString());
                prop_p_attribute5_bf.setType(String.class);

                // ===============23. p_attribute5_af  ===================
                PropertyInfo prop_p_attribute5_af = new PropertyInfo();
                prop_p_attribute5_af.setName("p_attribute5_af");
                prop_p_attribute5_af.setValue(oca.getA5_af().toString());
                prop_p_attribute5_af.setType(String.class);

                // ===============24. p_attribute6_bf  ===================
                PropertyInfo prop_p_attribute6_bf = new PropertyInfo();
                prop_p_attribute6_bf.setName("p_attribute6_bf");
                prop_p_attribute6_bf.setValue(oca.getA6_bf().toString());
                prop_p_attribute6_bf.setType(String.class);

                // ===============25. p_attribute6_af  ===================
                PropertyInfo prop_p_attribute6_af = new PropertyInfo();
                prop_p_attribute6_af.setName("p_attribute6_af");
                prop_p_attribute6_af.setValue(oca.getA6_af().toString());
                prop_p_attribute6_af.setType(String.class);

                // ===============26. p_attribute7_bf  ===================
                PropertyInfo prop_p_attribute7_bf = new PropertyInfo();
                prop_p_attribute7_bf.setName("p_attribute7_bf");
                prop_p_attribute7_bf.setValue(oca.getA7_bf().toString());
                prop_p_attribute7_bf.setType(String.class);

                // ===============27. p_attribute7_af  ===================
                PropertyInfo prop_p_attribute7_af = new PropertyInfo();
                prop_p_attribute7_af.setName("p_attribute7_af");
                prop_p_attribute7_af.setValue(oca.getA7_af().toString());
                prop_p_attribute7_af.setType(String.class);

                // ===============28. p_attribute8_bf  ===================
                PropertyInfo prop_p_attribute8_bf = new PropertyInfo();
                prop_p_attribute8_bf.setName("p_attribute8_bf");
                prop_p_attribute8_bf.setValue(oca.getA8_bf().toString());
                prop_p_attribute8_bf.setType(String.class);

                // ===============29. p_attribute8_af  ===================
                PropertyInfo prop_p_attribute8_af = new PropertyInfo();
                prop_p_attribute8_af.setName("p_attribute8_af");
                prop_p_attribute8_af.setValue(oca.getA8_af().toString());
                prop_p_attribute8_af.setType(String.class);

                // ===============30. p_attribute9_bf  ===================
                PropertyInfo prop_p_attribute9_bf = new PropertyInfo();
                prop_p_attribute9_bf.setName("p_attribute9_bf");
                prop_p_attribute9_bf.setValue(oca.getA9_bf().toString());
                prop_p_attribute9_bf.setType(String.class);

                // ===============31. p_attribute9_af  ===================
                PropertyInfo prop_p_attribute9_af = new PropertyInfo();
                prop_p_attribute9_af.setName("p_attribute9_af");
                prop_p_attribute9_af.setValue(oca.getA9_af().toString());
                prop_p_attribute9_af.setType(String.class);

                // ===============32. p_attribute10_bf  ===================
                PropertyInfo prop_p_attribute10_bf = new PropertyInfo();
                prop_p_attribute10_bf.setName("p_attribute10_bf");
                prop_p_attribute10_bf.setValue(oca.getA10_bf().toString());
                prop_p_attribute10_bf.setType(String.class);

                // ===============33. p_attribute10_af  ===================
                PropertyInfo prop_p_attribute10_af = new PropertyInfo();
                prop_p_attribute10_af.setName("p_attribute10_af");
                prop_p_attribute10_af.setValue(oca.getA10_af().toString());
                prop_p_attribute10_af.setType(String.class);

                // ===============34. p_attribute11_bf  ===================
                PropertyInfo prop_p_attribute11_bf = new PropertyInfo();
                prop_p_attribute11_bf.setName("p_attribute11_bf");
                prop_p_attribute11_bf.setValue(oca.getA11_bf().toString());
                prop_p_attribute11_bf.setType(String.class);

                // ===============35. p_attribute11_af  ===================
                PropertyInfo prop_p_attribute11_af = new PropertyInfo();
                prop_p_attribute11_af.setName("p_attribute11_af");
                prop_p_attribute11_af.setValue(oca.getA11_af().toString());
                prop_p_attribute11_af.setType(String.class);

                // ===============36. p_attribute12_bf  ===================
                PropertyInfo prop_p_attribute12_bf = new PropertyInfo();
                prop_p_attribute12_bf.setName("p_attribute12_bf");
                prop_p_attribute12_bf.setValue(oca.getA12_bf().toString());
                prop_p_attribute12_bf.setType(String.class);

                // ===============37. p_attribute12_af  ===================
                PropertyInfo prop_p_attribute12_af = new PropertyInfo();
                prop_p_attribute12_af.setName("p_attribute12_af");
                prop_p_attribute12_af.setValue(oca.getA12_af().toString());
                prop_p_attribute12_af.setType(String.class);

                // ===============38. p_attribute13_bf  ===================
                PropertyInfo prop_p_attribute13_bf = new PropertyInfo();
                prop_p_attribute13_bf.setName("p_attribute13_bf");
                prop_p_attribute13_bf.setValue(oca.getA13_bf().toString());
                prop_p_attribute13_bf.setType(String.class);

                // ===============39. p_attribute13_af  ===================
                PropertyInfo prop_p_attribute13_af = new PropertyInfo();
                prop_p_attribute13_af.setName("p_attribute13_af");
                prop_p_attribute13_af.setValue(oca.getA13_af().toString());
                prop_p_attribute13_af.setType(String.class);

                // ===============40. p_attribute14_bf  ===================
                PropertyInfo prop_p_attribute14_bf = new PropertyInfo();
                prop_p_attribute14_bf.setName("p_attribute14_bf");
                prop_p_attribute14_bf.setValue(oca.getA14_bf().toString());
                prop_p_attribute14_bf.setType(String.class);

                // ===============41. p_attribute14_af  ===================
                PropertyInfo prop_p_attribute14_af = new PropertyInfo();
                prop_p_attribute14_af.setName("p_attribute14_af");
                prop_p_attribute14_af.setValue(oca.getA14_af().toString());
                prop_p_attribute14_af.setType(String.class);

                // ===============42. p_attribute15_bf  ===================
                PropertyInfo prop_p_attribute15_bf = new PropertyInfo();
                prop_p_attribute15_bf.setName("p_attribute15_bf");
                prop_p_attribute15_bf.setValue(oca.getA15_bf().toString());
                prop_p_attribute15_bf.setType(String.class);

                // ===============43. p_attribute15_af  ===================
                PropertyInfo prop_p_attribute15_af = new PropertyInfo();
                prop_p_attribute15_af.setName("p_attribute15_af");
                prop_p_attribute15_af.setValue(oca.getA15_af().toString());
                prop_p_attribute15_af.setType(String.class);

                // ===============44. p_attribute16_bf  ===================
                PropertyInfo prop_p_attribute16_bf = new PropertyInfo();
                prop_p_attribute16_bf.setName("p_attribute16_bf");
                prop_p_attribute16_bf.setValue(oca.getA16_bf().toString());
                prop_p_attribute16_bf.setType(String.class);

                // ===============45. p_attribute16_af  ===================
                PropertyInfo prop_p_attribute16_af = new PropertyInfo();
                prop_p_attribute16_af.setName("p_attribute16_af");
                prop_p_attribute16_af.setValue(oca.getA16_af().toString());
                prop_p_attribute16_af.setType(String.class);

                // ===============46. p_attribute17_bf  ===================
                PropertyInfo prop_p_attribute17_bf = new PropertyInfo();
                prop_p_attribute17_bf.setName("p_attribute17_bf");
                prop_p_attribute17_bf.setValue(oca.getA17_bf().toString());
                prop_p_attribute17_bf.setType(String.class);

                // ===============47. p_attribute17_af  ===================
                PropertyInfo prop_p_attribute17_af = new PropertyInfo();
                prop_p_attribute17_af.setName("p_attribute17_af");
                prop_p_attribute17_af.setValue(oca.getA17_af().toString());
                prop_p_attribute17_af.setType(String.class);

                // ===============48. p_attribute18_bf  ===================
                PropertyInfo prop_p_attribute18_bf = new PropertyInfo();
                prop_p_attribute18_bf.setName("p_attribute18_bf");
                prop_p_attribute18_bf.setValue(oca.getA18_bf().toString());
                prop_p_attribute18_bf.setType(String.class);

                // ===============49. p_attribute18_af  ===================
                PropertyInfo prop_p_attribute18_af = new PropertyInfo();
                prop_p_attribute18_af.setName("p_attribute18_af");
                prop_p_attribute18_af.setValue(oca.getA18_af().toString());
                prop_p_attribute18_af.setType(String.class);

                // ===============50. p_attribute19_bf  ===================
                PropertyInfo prop_p_attribute19_bf = new PropertyInfo();
                prop_p_attribute19_bf.setName("p_attribute19_bf");
                prop_p_attribute19_bf.setValue(oca.getA19_bf().toString());
                prop_p_attribute19_bf.setType(String.class);

                // ===============51. p_attribute19_af  ===================
                PropertyInfo prop_p_attribute19_af = new PropertyInfo();
                prop_p_attribute19_af.setName("p_attribute19_af");
                prop_p_attribute19_af.setValue(oca.getA19_af().toString());
                prop_p_attribute19_af.setType(String.class);

                // ===============52. p_attribute20_bf  ===================
                PropertyInfo prop_p_attribute20_bf = new PropertyInfo();
                prop_p_attribute20_bf.setName("p_attribute20_bf");
                prop_p_attribute20_bf.setValue(oca.getA20_bf().toString());
                prop_p_attribute20_bf.setType(String.class);

                // ===============53. p_attribute10_af  ===================
                PropertyInfo prop_p_attribute20_af = new PropertyInfo();
                prop_p_attribute20_af.setName("p_attribute20_af");
                prop_p_attribute20_af.setValue(oca.getA20_bf().toString());
                prop_p_attribute20_af.setType(String.class);

                // ===============54. p_download_date  ===================
                PropertyInfo prop_p_download_date = new PropertyInfo();
                prop_p_download_date.setName("p_download_date");
                prop_p_download_date.setValue(oca.getDOWNLOAD_DATE().toString());
                prop_p_download_date.setType(String.class);

                // ===============55. p_scan_date:  ===================
                PropertyInfo prop_p_scan_date = new PropertyInfo();
                prop_p_scan_date.setName("p_scan_date");
                prop_p_scan_date.setValue(oca.getSCAN_DATE().toString());
                prop_p_scan_date.setType(String.class);

                // ===============56. p_device_id ===================
                PropertyInfo prop_p_device_id = new PropertyInfo();
                prop_p_device_id.setName("p_device_id");
                prop_p_device_id.setValue(device_id);
                prop_p_device_id.setType(String.class);

                // ===============57.  p_scan_by ===================
                PropertyInfo prop_p_scan_by = new PropertyInfo();
                prop_p_scan_by.setName("p_scan_by");
                prop_p_scan_by.setValue(oca.getSCAN_BY());
                prop_p_scan_by.setType(String.class);

                request.addProperty(prop_p_stg_id);
                request.addProperty(prop_p_asset_id);
                request.addProperty(prop_p_asset_book);
                request.addProperty(prop_p_tag_number);
                request.addProperty(prop_p_asset_desc);
                request.addProperty(prop_p_asset_cat_id);
                request.addProperty(prop_p_asset_cat);
                request.addProperty(prop_p_loc_id_bf);
                request.addProperty(prop_p_loc_id_af);
                request.addProperty(prop_p_assign_bf);
                request.addProperty(prop_p_assign_af);
                request.addProperty(prop_p_qty_bf);
                request.addProperty(prop_p_qty_af);
                request.addProperty(prop_p_attribute1_bf);
                request.addProperty(prop_p_attribute1_af);
                request.addProperty(prop_p_attribute2_bf);
                request.addProperty(prop_p_attribute2_af);
                request.addProperty(prop_p_attribute3_bf);
                request.addProperty(prop_p_attribute3_af);
                request.addProperty(prop_p_attribute4_bf);
                request.addProperty(prop_p_attribute4_af);
                request.addProperty(prop_p_attribute5_bf);
                request.addProperty(prop_p_attribute5_af);
                request.addProperty(prop_p_attribute6_bf);
                request.addProperty(prop_p_attribute6_af);
                request.addProperty(prop_p_attribute7_bf);
                request.addProperty(prop_p_attribute7_af);
                request.addProperty(prop_p_attribute8_bf);
                request.addProperty(prop_p_attribute8_af);
                request.addProperty(prop_p_attribute9_bf);
                request.addProperty(prop_p_attribute9_af);
                request.addProperty(prop_p_attribute10_bf);
                request.addProperty(prop_p_attribute10_af);
                request.addProperty(prop_p_attribute11_bf);
                request.addProperty(prop_p_attribute11_af);
                request.addProperty(prop_p_attribute12_bf);
                request.addProperty(prop_p_attribute12_af);
                request.addProperty(prop_p_attribute13_bf);
                request.addProperty(prop_p_attribute13_af);
                request.addProperty(prop_p_attribute14_bf);
                request.addProperty(prop_p_attribute14_af);
                request.addProperty(prop_p_attribute15_bf);
                request.addProperty(prop_p_attribute15_af);
                request.addProperty(prop_p_attribute16_bf);
                request.addProperty(prop_p_attribute16_af);
                request.addProperty(prop_p_attribute17_bf);
                request.addProperty(prop_p_attribute17_af);
                request.addProperty(prop_p_attribute18_bf);
                request.addProperty(prop_p_attribute18_af);
                request.addProperty(prop_p_attribute19_bf);
                request.addProperty(prop_p_attribute19_af);
                request.addProperty(prop_p_attribute20_bf);
                request.addProperty(prop_p_attribute20_af);
                request.addProperty(prop_p_download_date);
                request.addProperty(prop_p_scan_date);
                request.addProperty(prop_p_device_id);
                request.addProperty(prop_p_scan_by);

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);

                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    v_oracle_id = resultsRequestSOAP.getProperty(0).toString();
                    ResultResponse  = v_oracle_id;

                    try{
                        if(Integer.parseInt(v_oracle_id) > 0) {
                            result = Integer.parseInt(v_oracle_id);
                            sent = sent + 1;
                        }
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "Exception : " + e.getMessage().toString());
                    }

                    Log.d("[GudangGaram]", "ResultResponse : " + ResultResponse);
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d("[GudangGaram]", "Sending Opname Data " + seq_no  + " of " + seq_total + " Records");
                    // publish progress
                    publishProgress("Sending Opname Data " + seq_no  + " of " + seq_total + " Records");
                }
                catch (Exception ex) {
                    Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
                }
                finally {
                    Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
                }


                // ===========================================

                //result = WS_SEND(oca);
                //if(result > 0){
                //    Log.d("[GudangGaram]", "Sent > " + sent);
                //    sent = sent + 1;
                //}

            }
        } // end for

        return result.toString();
    }
}
