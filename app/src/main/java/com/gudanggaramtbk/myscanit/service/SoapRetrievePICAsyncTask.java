package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.SyncAction;
import com.gudanggaramtbk.myscanit.model.CPic;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by luckym on 2/8/2019.
 */

public class SoapRetrievePICAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences    config;
    private Context              context;
    private ProgressDialog       pd;
    private MySQLiteHelper dbHelper;
    private String               SentResponse;
    private String               person_id;
    private String               person_nik;
    private String               person_name;
    private SyncAction PActivity;

    private ArrayAdapter<String> adapter;

    public SoapRetrievePICAsyncTask(){ }
    public interface SoapRetrievePICAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapRetrievePICAsyncTask.SoapRetrievePICAsyncTaskResponse delegate = null;
    public SoapRetrievePICAsyncTask(SoapRetrievePICAsyncTask.SoapRetrievePICAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config){
        Log.d("[GudangGaram]", "SoapRetrievePICAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd = new ProgressDialog(this.context);
    }
    public void setParentActivity(SyncAction oParent){
        this.PActivity           = oParent;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrievePICAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Master PIC From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrievePICAsyncTask :: onPostExecute >> " + output);
        try{
            delegate.PostSentAction(output);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                Toast.makeText(context,"Retrieve PIC Done", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(context,"Retrieve PIC" + output + " Done", Toast.LENGTH_LONG).show();
            }
            PActivity.EH_Reload_Record_Count("pic");
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;
        Integer  nerr;
        Long     Result;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Pic";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                nerr   = 0;
                Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());
                if(ncount > 0) {
                    for (int i = 0; i < ncount; i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);

                        person_id = obj3.getProperty(0).toString();
                        person_nik = obj3.getProperty(1).toString();
                        person_name = obj3.getProperty(2).toString();

                        Log.d("[GudangGaram]", "person id        :" + person_id);
                        Log.d("[GudangGaram]", "person nik       :" + person_nik);
                        Log.d("[GudangGaram]", "person name      :" + person_name);

                        CPic oClass = new CPic();
                        oClass.setPersonID(Integer.parseInt(person_id));
                        oClass.setPersonNIK(person_nik);
                        oClass.setPersonName(person_name);

                        Result = dbHelper.AddPIC(oClass);
                        if(Result > 0){
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        publishProgress("Add PIC Info [ " + Integer.toString(i+1) + " of " + ncount + " ] : [" + person_nik + "] >> " + person_name);
                        }
                        else{
                            nerr +=1;
                        }
                        Thread.sleep(200);
                    }
                }
            }
            catch(NullPointerException e){
                Toast.makeText(context, "Connection Exception " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
        }
        Log.d("[GudangGaram]", "SoapRetrievePICAsyncTask  :: end doInBackground");

        return "";
    }
}
