package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 3/13/2019.
 */

public class SoapSendReqBatchIDAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private MySQLiteHelper dbHelper;
    private String                    SentResponse;

    public SoapSendReqBatchIDAsyncTask(){}
    public interface SoapSendReqBatchIDAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapSendReqBatchIDAsyncTask.SoapSendReqBatchIDAsyncTaskResponse delegate = null;
    public SoapSendReqBatchIDAsyncTask(SoapSendReqBatchIDAsyncTask.SoapSendReqBatchIDAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper    dbHelper,
                             SharedPreferences config){
        Log.d("[GudangGaram]", "SoapSendReqBatchIDAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapSendReqBatchIDAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Get BatchID From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SoapSendReqBatchIDAsyncTask :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapSendReqBatchIDAsyncTask :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String   v_oracle_id = "-1";

        String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

        String OPERATION_NAME = "Get_Stg_ID";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            v_oracle_id = resultsRequestSOAP.getProperty(0).toString();
            ResultResponse  = v_oracle_id;
            Log.d("[GudangGaram]", "ResultResponse : " + ResultResponse);
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        finally {
            Log.d("[GudangGaram]", "SoapSendReqOLTaskAsync :: end doInBackground");
        }

        return ResultResponse;
    }
}
