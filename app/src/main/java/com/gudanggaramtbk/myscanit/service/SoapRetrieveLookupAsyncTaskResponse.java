package com.gudanggaramtbk.myscanit.service;

/**
 * Created by luckym on 2/8/2019.
 */

public interface SoapRetrieveLookupAsyncTaskResponse {
    void PostSentAction(String output);
}
