package com.gudanggaramtbk.myscanit.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.SyncAction;
import com.gudanggaramtbk.myscanit.model.StringWithTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckym on 2/8/2019.
 */

public class SoapRetrieveLookup {
    private SharedPreferences config;
    private Context           context;
    private MySQLiteHelper dbHelper;
    private SyncAction PActivity;
    // override constructor
    public SoapRetrieveLookup(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapRetrieveLookup :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapRetrieveLookup :: SetContext");
        this.context = context;
    }
    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SoapRetrieveLookup :: SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void setParentActivity(SyncAction oParent){
        this.PActivity = oParent;
    }

    public String Retrieve() {
        Log.d("[GudangGaram]", "SoapRetrieveLookup :: Retrieve Begin");

        //final List<String> Result = new ArrayList<String>();
        final List<StringWithTag> Result = new ArrayList<StringWithTag>();
        try {
            SoapRetrieveLookupAsyncTask SoapRequest = new SoapRetrieveLookupAsyncTask(new SoapRetrieveLookupAsyncTask.SoapRetrieveLookupAsyncTaskResponse() {
                @Override
                public void PostSentAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config);
            SoapRequest.setParentActivity(PActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveLookup :: Retrieve End");
        }
        return "";
    }
}
