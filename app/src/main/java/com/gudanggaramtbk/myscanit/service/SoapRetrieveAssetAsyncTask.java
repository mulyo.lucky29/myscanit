package com.gudanggaramtbk.myscanit.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.gudanggaramtbk.myscanit.util.MySQLiteHelper;
import com.gudanggaramtbk.myscanit.activity.DownloadAssetActivity;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 2/8/2019.
 */

public class SoapRetrieveAssetAsyncTask extends AsyncTask<String, String, String> {
    private SharedPreferences     config;
    private Context               context;
    private ProgressDialog        pd;
    private MySQLiteHelper dbHelper;
    private String                SentResponse;
    private String                p_asset_book;
    private String                p_segment3;
    private String                p_segment4;
    private String                TResult;
    private DownloadAssetActivity PActivity;

    private ArrayAdapter<String> adapter;

    public SoapRetrieveAssetAsyncTask(){ }
    public interface SoapRetrieveAssetAsyncTaskResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveAssetAsyncTask.SoapRetrieveAssetAsyncTaskResponse delegate = null;
    public SoapRetrieveAssetAsyncTask(SoapRetrieveAssetAsyncTask.SoapRetrieveAssetAsyncTaskResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_asset_book,
                             String p_segment3,
                             String p_segment4){
        Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.p_asset_book      = p_asset_book;
        this.p_segment3        = p_segment3;
        this.p_segment4        = p_segment4;
        this.pd = new ProgressDialog(this.context);
    }
    public void setParentActivity(DownloadAssetActivity oParent){
        this.PActivity         = oParent;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Please Wait Retrieving Asset Data From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask :: onPostExecute >> " + TResult);
        try{
            delegate.PostSentAction(TResult);
            try{
                PActivity.setTotalPage(Integer.parseInt(TResult));
                if(Integer.parseInt(TResult) > 0){
                    PActivity.RequestPageAssetDetail(PActivity.getTotalPage(),PActivity.getCurrentPage(),p_asset_book,p_segment3,p_segment4);
                }
            }
            catch(Exception e){
            }
        }
        catch(Exception e){
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;
        Integer  nerr;
        Long     Result;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_Page_list_Asset";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_asset_book ===================
        PropertyInfo prop_p_asset_book = new PropertyInfo();
        prop_p_asset_book.setName("p_asset_book");
        prop_p_asset_book.setValue(p_asset_book);
        prop_p_asset_book.setType(String.class);
        request.addProperty(prop_p_asset_book);

        // =============== p_segment3 ===================
        PropertyInfo prop_p_segment3 = new PropertyInfo();
        prop_p_segment3.setName("p_segment3");
        prop_p_segment3.setValue(p_segment3);
        prop_p_segment3.setType(String.class);
        request.addProperty(prop_p_segment3);

        // =============== p_segment4 ===================
        PropertyInfo prop_p_segment4 = new PropertyInfo();
        prop_p_segment4.setName("p_segment4");
        prop_p_segment4.setValue(p_segment4);
        prop_p_segment4.setType(String.class);
        request.addProperty(prop_p_segment4);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject result = (SoapObject) envelope.bodyIn;
                TResult = result.getProperty(0).toString();
                Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask  TResult :" + TResult);
            }
            catch(NullPointerException e){
                Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask  :: Exception " + e.getMessage().toString());
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "SoapRetrieveAssetAsyncTask  :: end doInBackground");

        return TResult;
    }
}
